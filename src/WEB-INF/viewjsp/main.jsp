<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="mainApp" >
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>title</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/style.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/ngToast.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/home.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/customerSearch.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/designerList.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/customerPage.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/designerPage.css" />

		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/jquery.min.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/angular.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/ui-bootstrap-tpls-0.12.0.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/angular-animate.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/angular-sanitize.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/angular-ui-router.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/ng-infinite-scroll.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/ngToast.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/nsPopover.js" charset="UTF-8" ></script>



		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/app.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/filter/commonFilter.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/service/httpRequester.js" charset="UTF-8" ></script>

		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/directive/commonDirective.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/directive/customerMenu.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/directive/designerMenu.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/directive/numbersOnly.js" charset="UTF-8" ></script>

		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/filter/searchFor.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/mainCtrl.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/homeCtrl.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/customerSearchCtrl.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/customerInfoCtrl.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/designerInfoCtrl.js" charset="UTF-8" ></script>
		
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/modal/designerListModalCtrl.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/modal/delDesignerModalCtrl.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/modal/selCustomerModalCtrl.js" charset="UTF-8" ></script>
		
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/init.js" charset="UTF-8" ></script>


	</head>
	<body ng-controller="mainCtrl" >
		<toast></toast>
		<div class="navbar navbar-default navbar-static-top" >
			<div class="container">
				<div class="navbar-header" >
					<a ui-sref="/" class="navbar-brand">JUNO</a>
				</div>
				
				<ul class="nav navbar-nav navbar-right">
					<li><a ui-sref="viewCustomer" >고객</a></li>
					<li><a ui-sref="viewDesigner" >디자이너</a></li>
					<li class="dropdown" dropdown >
						<a type="button" class="dropdown-toggle" dropdown-toggle data-toggle="dropdown" role="button" aria-expanded="false" >관리<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu" >
							<li><a ng-click="statisticInfo()" >통계보기</a></li>
							<li><a ng-click="changeInfo()" >정보변경</a></li>
							<li class="divider" ></li>
							<li><a ui-sref="contact" >기술지원</a></li>
							<li class="divider" ></li>
							<li><a ng-click="signOut()" >로그아웃</a></li>
						</ul>
					</li>
				</ul>
			</div>

		</div>
		<div class="container" >
			<div ui-view></div>
		</div>
	</body>
</html>
