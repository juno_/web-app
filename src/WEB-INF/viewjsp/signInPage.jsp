<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html ng-app="mainApp" >
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>Error</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/style.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/signIn.css" />

		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/jquery.min.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/angular.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/ui-bootstrap-tpls-0.12.0.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/angular-ui-router.js" charset="UTF-8" ></script>



		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/app.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/directive/commonDirective.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/service/httpRequester.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/mainCtrl.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/signInCtrl.js" charset="UTF-8" ></script>


	</head>
	<body ng-controller="mainCtrl" >
		<div class="navbar navbar-default navbar-static-top" >
			<div class="container">
				<div class="navbar-header" >
					<a ng-click="home()" href="javascript:void(0);" class="navbar-brand">JUNO</a>
				</div>
			</div>
		</div>
		<div class="container" >
			<div class="resign-in-container" ng-controller="signInCtrl" >
				<div class="center-block text-center resign-in-comment has-error" >
					<span class="glyphicon glyphicon-warning-sign help-block" ></span>
					<p class="help-block" >세션이 유효하지 않습니다. <br/>다시 로그인해주세요.</p>
				</div>
                                <form novalidate name="signInForm" >                
                                        <div class="form-group" ng-class="{'has-error' : signInForm.adminId.$invalid && !signInForm.adminId.$pristine}" >
                                                <label>아이디</label>               
                                                <input type="text" class="form-control input-lg" name="adminId"  ng-model="adminId"  required />
                                                <p class="help-block" ng-show="signInForm.adminId.$invalid && !signInForm.adminId.$pristine" >아이디를 입력하세요.</p>
                                        </div>  
                                        <div class="form-group" ng-class="{'has-error' : signInForm.adminPw.$invalid && !signInForm.adminPw.$pristine}" >
                                                <label>비밀번호</label>             
                                                <input type="password" class="form-control input-lg" name="adminPw" ng-model="adminPw" required />
                                                <p class="help-block" ng-show="signInForm.adminPw.$invalid && !signInForm.adminPw.$pristine" >비밀번호를 입력하세요.</p>
                                        </div>                                      
                                        <input type="submit" value="로그인" class="btn btn-primary" ng-click="signIn(signInForm)" />
                                </form>         
			</div>
		</div>
	</body>
</html>
