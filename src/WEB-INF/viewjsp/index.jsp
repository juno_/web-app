<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html ng-app="mainApp" >
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>title</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/style.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/web/css/signIn.css" />

		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/jquery.min.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/angular.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/ui-bootstrap-tpls-0.12.0.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/angular-animate.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/angular-sanitize.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/ngToast.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/angular-ui-router.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/ng-infinite-scroll.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/externals/nsPopover.js" charset="UTF-8" ></script>



		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/app.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/directive/commonDirective.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/service/httpRequester.js" charset="UTF-8" ></script>
		<script language="javascript" src="${pageContext.request.contextPath}/web/js/mainApp/controller/signInCtrl.js" charset="UTF-8" ></script>


	</head>
	<body ng-controller="signInCtrl" class="full-bg" >
		<div class="sign-in-container" >
			<form novalidate name="signInForm" >
				<div class="form-group" ng-class="{'has-error' : signInForm.adminId.$invalid && !signInForm.adminId.$pristine}" >
					<label ><h3>아이디</h3></label>
					<input type="text" class="form-control input-lg" name="adminId"  ng-model="adminId"  required  focus/>
					<p class="help-block" ng-show="signInForm.adminId.$invalid && !signInForm.adminId.$pristine" >아이디를 입력하세요.</p>
				</div>
				<div class="form-group" ng-class="{'has-error' : signInForm.adminPw.$invalid && !signInForm.adminPw.$pristine}" >
					<label ><h3>비밀번호</h3></label>
					<input type="password" class="form-control input-lg" name="adminPw" ng-model="adminPw" required />
					<p class="help-block" ng-show="signInForm.adminPw.$invalid && !signInForm.adminPw.$pristine" >비밀번호를 입력하세요.</p>
				</div>
				<input type="submit" value="로그인" class="btn btn-primary btn-lg btn-block" ng-click="signIn(signInForm)" />

			</form>
		</div>
	</body>
</html>
