package model.memo;
public class MemoDTO {

	private int num;

	private String cId;
	private String cName;

	private String content;
	private String date;
	private String timestamp;
	
	private String dId;
	private String dName;
	private boolean isHiMemo;

	private String procedureType;
	private int price;

	
	/**
	 * Get num.
	 *
	 * @return num as int.
	 */
	public int getNum()
	{
	    return num;
	}
	
	/**
	 * Set num.
	 *
	 * @param num the value to set.
	 */
	public void setNum(int num)
	{
	    this.num = num;
	}
	
	/**
	 * Get cId.
	 *
	 * @return cId as String.
	 */
	public String getCId()
	{
	    return cId;
	}
	
	/**
	 * Set cId.
	 *
	 * @param cId the value to set.
	 */
	public void setCId(String cId)
	{
	    this.cId = cId;
	}
	
	/**
	 * Get cName.
	 *
	 * @return cName as String.
	 */
	public String getCName()
	{
	    return cName;
	}
	
	/**
	 * Set cName.
	 *
	 * @param cName the value to set.
	 */
	public void setCName(String cName)
	{
	    this.cName = cName;
	}
	
	/**
	 * Get content.
	 *
	 * @return content as String.
	 */
	public String getContent()
	{
	    return content;
	}
	
	/**
	 * Set content.
	 *
	 * @param content the value to set.
	 */
	public void setContent(String content)
	{
	    this.content = content;
	}
	
	/**
	 * Get date.
	 *
	 * @return date as String.
	 */
	public String getDate()
	{
	    return date;
	}
	
	/**
	 * Set date.
	 *
	 * @param date the value to set.
	 */
	public void setDate(String date)
	{
	    this.date = date;
	}
	
	/**
	 * Get timestamp.
	 *
	 * @return timestamp as String.
	 */
	public String getTimestamp()
	{
	    return timestamp;
	}
	
	/**
	 * Set timestamp.
	 *
	 * @param timestamp the value to set.
	 */
	public void setTimestamp(String timestamp)
	{
	    this.timestamp = timestamp;
	}
	
	/**
	 * Get dId.
	 *
	 * @return dId as String.
	 */
	public String getDId()
	{
	    return dId;
	}
	
	/**
	 * Set dId.
	 *
	 * @param dId the value to set.
	 */
	public void setDId(String dId)
	{
	    this.dId = dId;
	}
	
	/**
	 * Get dName.
	 *
	 * @return dName as String.
	 */
	public String getDName()
	{
	    return dName;
	}
	
	/**
	 * Set dName.
	 *
	 * @param dName the value to set.
	 */
	public void setDName(String dName)
	{
	    this.dName = dName;
	}
	
	/**
	 * Get isHiMemo.
	 *
	 * @return isHiMemo as boolean.
	 */
	public boolean getIsHiMemo()
	{
	    return isHiMemo;
	}
	
	/**
	 * Set isHiMemo.
	 *
	 * @param isHiMemo the value to set.
	 */
	public void setIsHiMemo(boolean isHiMemo)
	{
	    this.isHiMemo = isHiMemo;
	}
	
	
	/**
	 * Get procedureType.
	 *
	 * @return procedureType as String.
	 */
	public String getProcedureType()
	{
	    return procedureType;
	}
	
	/**
	 * Set procedureType.
	 *
	 * @param procedureType the value to set.
	 */
	public void setProcedureType(String procedureType)
	{
	    this.procedureType = procedureType;
	}
	
	/**
	 * Get price.
	 *
	 * @return price as int.
	 */
	public int getPrice()
	{
	    return price;
	}
	
	/**
	 * Set price.
	 *
	 * @param price the value to set.
	 */
	public void setPrice(int price)
	{
	    this.price = price;
	}
}
