package model.memo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

public class MemoDAO{
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}


	public boolean insert(final MemoDTO dto){
		boolean succ = true;
		String sql = "insert into memo(c_id, content, write_date, timestamp, d_id, is_hi_memo, procedure_type, price) values(?, ?, ?, ?, ?, ?, ?, ?)";
		Object args[] = new Object[]{
			dto.getCId(),
			dto.getContent(),
			dto.getDate(),
			dto.getTimestamp(),
			dto.getDId(),
			dto.getIsHiMemo(),
			dto.getProcedureType(),
			dto.getPrice()
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}

		return succ;

	}
	public boolean update(MemoDTO memo){
		boolean succ = true;
		String sql = "update memo set content = ?, price = ?, procedure_type = ? where d_id = ? and c_id=? and timestamp =?";
		Object args[] = new Object[]{
			memo.getContent(),
			memo.getPrice(),
			memo.getProcedureType(),
			memo.getDId(),
			memo.getCId(),
			memo.getTimestamp()
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}

		return succ;

	}
	public boolean delete(String designerId, String customerId, String timestamp){
		boolean succ = true;
		String sql = "delete from memo where d_id = ? and c_id=? and timestamp =?";
		Object args[] = new Object[]{
			designerId,
			customerId,
			timestamp
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}

		return succ;

	}

	public List<MemoDTO> select(String customerId, String designerId, int count){
		String sql = "select memo.num, c_id, customer.name, content, write_date, timestamp, d_id, designer.name, is_hi_memo, procedure_type, price from memo, customer, designer where customer.id=memo.c_id and memo.d_id=designer.id and designer.id=? and memo.is_show=true and customer.id=? order by timestamp desc limit 0, ?";
		List<MemoDTO> result = new ArrayList<MemoDTO>();

		Object args[] = new Object[]{
			designerId,
			customerId,
			count
		};

		try{
			result = this.jdbcTemplate.query(sql, args, new RowMapper<MemoDTO>(){

				public MemoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					MemoDTO dto = new MemoDTO();
					dto.setNum(rs.getInt("memo.num"));
					dto.setCId(rs.getString("c_id"));
					dto.setCName(rs.getString("customer.name"));
					dto.setContent(rs.getString("content"));
					dto.setDate(rs.getString("write_date"));
					dto.setTimestamp(rs.getString("timestamp"));
					dto.setDId(rs.getString("d_id"));
					dto.setDName(rs.getString("designer.name"));
					dto.setIsHiMemo(rs.getBoolean("is_hi_memo"));
					dto.setProcedureType(rs.getString("procedure_type"));
					dto.setPrice(rs.getInt("price"));
					return dto;
				}
			});


		}catch(EmptyResultDataAccessException e){
			result = new ArrayList<MemoDTO>();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;

	}
	public List<MemoDTO> select(String customerId, String designerId, String timestamp, int count){
		String sql = "select memo.num as num, c_id, customer.name c_name, content, write_date, timestamp, d_id, designer.name d_name, is_hi_memo, price, procedure_type from memo, customer, designer where customer.id=memo.c_id and memo.d_id=designer.id and designer.id=? and customer.id=? and memo.is_show=true and timestamp < ? order by timestamp desc limit 0, ?";
		List<MemoDTO> result = new ArrayList<MemoDTO>();

		Object args[] = new Object[]{
			designerId,
			customerId,
			timestamp,
			count
		};

		try{
			result = this.jdbcTemplate.query(sql, args, new RowMapper<MemoDTO>(){

				public MemoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					MemoDTO dto = new MemoDTO();
					dto.setNum(rs.getInt("num"));
					dto.setCId(rs.getString("c_id"));
					dto.setCName(rs.getString("c_name"));
					dto.setContent(rs.getString("content"));
					dto.setDate(rs.getString("write_date"));
					dto.setTimestamp(rs.getString("timestamp"));
					dto.setDId(rs.getString("d_id"));
					dto.setDName(rs.getString("d_name"));
					dto.setIsHiMemo(rs.getBoolean("is_hi_memo"));
					dto.setProcedureType(rs.getString("procedure_type"));
					dto.setPrice(rs.getInt("price"));
					return dto;
				}
			});


		}catch(EmptyResultDataAccessException e){
			result = new ArrayList<MemoDTO>();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
	//select * from memo inner join(select max(timestamp) as max from memo group by c_id) a2 on memo.timestamp = a2.max;
	public List<MemoDTO> selectAll(String designerId, int count){
		//String sql = "select @prev:=c_id as c_id, timestamp, content, name, write_date, num, d_id from (select * from memo, customer where customer.id=memo.c_id order by memo.c_id, timestamp desc) as tb, (select @rank:=0, @prev:='' ) r where if(c_id=@prev, @rank:=@rank+1, @rank:=1) <= ? and d_id=?";
		String sql = "select c_id, d_id, timestamp, content, c_name, write_date, m_num, is_hi_memo, procedure_type, price from (select c_id, d_id, timestamp, content, customer.name as c_name, write_date, memo.num as m_num, procedure_type, price, is_hi_memo, (case @vjob when c_id then @rownum:=@rownum+1 else @rownum:=1 end) rnum, (@vjob:=c_id) vjob from memo, customer, (select @vjob:='', @rownum:=0 from dual) b where memo.c_id = customer.id and memo.is_show=true order by c_id, timestamp desc) c where rnum <= ? and d_id=?";
		List<MemoDTO> result = new ArrayList<MemoDTO>();

		Object args[] = new Object[]{
			count,
			designerId
		};

		try{
			result = this.jdbcTemplate.query(sql, args, new RowMapper<MemoDTO>(){

				public MemoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					MemoDTO dto = new MemoDTO();
					dto.setNum(rs.getInt("m_num"));
					dto.setCId(rs.getString("c_id"));
					dto.setCName(rs.getString("c_name"));
					dto.setContent(rs.getString("content"));
					dto.setDate(rs.getString("write_date"));
					dto.setTimestamp(rs.getString("timestamp"));
					dto.setDId(rs.getString("d_id"));
					dto.setIsHiMemo(rs.getBoolean("is_hi_memo"));
					dto.setProcedureType(rs.getString("procedure_type"));
					dto.setPrice(rs.getInt("price"));
					return dto;
				}
			});


		}catch(EmptyResultDataAccessException e){
			result = new ArrayList<MemoDTO>();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
}
