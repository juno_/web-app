package model.procedure;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

public class ProcDAO{
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<ProcDTO> selectAll(String branch){
		String sql = "select * from procedures order by num";
		List<ProcDTO> result = new ArrayList<ProcDTO>();

		Object args[] = new Object[]{
		};

		try{
			result = this.jdbcTemplate.query(sql, args, new RowMapper<ProcDTO>(){

				public ProcDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					ProcDTO dto = new ProcDTO();
					dto.setNum(rs.getInt("num"));
					dto.setName(rs.getString("name"));
					return dto;
				}
			});


		}catch(EmptyResultDataAccessException e){
			result = new ArrayList<ProcDTO>();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
}
