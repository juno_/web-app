package model.procedure;
public class ProcDTO {
	private int num;
	private String name;

	
	/**
	 * Get num.
	 *
	 * @return num as int.
	 */
	public int getNum()
	{
	    return num;
	}
	
	/**
	 * Set num.
	 *
	 * @param num the value to set.
	 */
	public void setNum(int num)
	{
	    this.num = num;
	}
	
	/**
	 * Get name.
	 *
	 * @return name as String.
	 */
	public String getName()
	{
	    return name;
	}
	
	/**
	 * Set name.
	 *
	 * @param name the value to set.
	 */
	public void setName(String name)
	{
	    this.name = name;
	}
}
