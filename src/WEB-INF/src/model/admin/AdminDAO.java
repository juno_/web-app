package model.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import model.customer.CustomerDTO;

public class AdminDAO{
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}


	public boolean insert(final AdminDTO dto){
		boolean succ = true;
		/*
		String sql = "insert into memo(c_id, content, write_date, timestamp, d_id) values(?, ?, ?, ?, ?)";
		Object args[] = new Object[]{
			dto.getCId(),
			dto.getContent(),
			dto.getDate(),
			dto.getTimestamp(),
			dto.getDId()
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}
		*/

		return succ;

	}
	public AdminDTO signIn(String id, String password){
		String sql = "select id, admin.name, admin.branch, branch_name.name from admin, branch_name where admin.branch = branch_name.branch and id = ? and password = password(?)";

		AdminDTO admin = null;
		Object args[] = new Object[]{
			id,
			password
		};

		try{
			admin = this.jdbcTemplate.queryForObject(sql, args, new RowMapper<AdminDTO>(){
					public AdminDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
						AdminDTO dto = new AdminDTO();
						dto.setId(rs.getString("id"));
						dto.setName(rs.getString("admin.name"));
						dto.setBranch(rs.getString("admin.branch"));
						dto.setBranchName(rs.getString("branch_name.name"));

						return dto;
					}
				});


		}catch(EmptyResultDataAccessException e){
			admin = null;	
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return admin; 
	}

}
