package model.admin;
public class AdminDTO {
	private String id;
	private String name;
	private String password;
	private String branch;
	private String branchName;
	
	/**
	 * Get id.
	 *
	 * @return id as String.
	 */
	public String getId()
	{
	    return id;
	}
	
	/**
	 * Set id.
	 *
	 * @param id the value to set.
	 */
	public void setId(String id)
	{
	    this.id = id;
	}
	
	/**
	 * Get name.
	 *
	 * @return name as String.
	 */
	public String getName()
	{
	    return name;
	}
	
	/**
	 * Set name.
	 *
	 * @param name the value to set.
	 */
	public void setName(String name)
	{
	    this.name = name;
	}
	
	/**
	 * Get password.
	 *
	 * @return password as String.
	 */
	public String getPassword()
	{
	    return password;
	}
	
	/**
	 * Set password.
	 *
	 * @param password the value to set.
	 */
	public void setPassword(String password)
	{
	    this.password = password;
	}
	
	/**
	 * Get branch.
	 *
	 * @return branch as int.
	 */
	public String getBranch()
	{
	    return branch;
	}
	
	/**
	 * Set branch.
	 *
	 * @param branch the value to set.
	 */
	public void setBranch(String branch)
	{
	    this.branch = branch;
	}
	
	/**
	 * Get branchName.
	 *
	 * @return branchName as String.
	 */
	public String getBranchName()
	{
	    return branchName;
	}
	
	/**
	 * Set branchName.
	 *
	 * @param branchName the value to set.
	 */
	public void setBranchName(String branchName)
	{
	    this.branchName = branchName;
	}
}

