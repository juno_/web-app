package model;
import model.customer.CustomerDTO;
import model.push.SendedPushDAO;
import model.push.SendedPushDTO;
import org.json.JSONObject;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import org.springframework.context.ApplicationContext;


public class PushData{
	
	public static String API_KEY="AIzaSyDQwb7--MhbB6g0zMlST4SALie0-QpfvBc";
	public static String IOS_KEY_PATH_DEV="/var/www/mijang/src/keystore/apns_keystore_dev.p12";
	public static String IOS_KEY_PATH_AD="/var/www/mijang/src/keystore/apns_keystore_adhoc.p12";


	public static JSONObject visitAlarm(JSONObject customer, String designerId, JSONObject lastMemoList, ApplicationContext context) throws Exception{
		JSONObject pushData = new JSONObject();	//push for save in server
		JSONObject sendData = new JSONObject();	//push for native 
		String pushId = "";
		int TYPE=1;

		JSONObject nativeData = new JSONObject();
		JSONObject data = new JSONObject();
		JSONObject designerData = new JSONObject();
		Calendar now = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String current = formatter.format(now.getTime());


		nativeData.put("title", "고객방문");
		nativeData.put("subTitle", customer.getString("name")+" 고객님이 입점하셨습니다.");

		pushData.put("native", nativeData);
		pushData.put("type", TYPE);


		sendData.put("native", nativeData);
		sendData.put("type", TYPE);

		designerData.put("id", designerId);
		pushData.put("designer", designerData);


		data.put("time", current);
		data.put("customer", customer);
		if(lastMemoList != null){
			data.put("memo", lastMemoList);
		}

		pushData.put("data", data);

		SendedPushDAO dao = context.getBean(SendedPushDAO.class);
		SendedPushDTO dto = new SendedPushDTO();
		dto.setDId(designerId);
		dto.setData(pushData.toString());
		dto.setType(TYPE);
		dto.setCId(customer.getString("id"));
		dao.completeDup(dto);
		pushId = dao.insert(dto);
		sendData.put("id", pushId);
		sendData.put("badge", dao.getUnfetchedCount(designerId));

		System.out.println(sendData.toString());

		return sendData;
	}
	public static JSONObject changeCustomerInfo(JSONObject customer, String dId, ApplicationContext context) throws Exception{
		JSONObject pushData = new JSONObject();
		JSONObject nativeData = new JSONObject();
		JSONObject data= new JSONObject();
		JSONObject sendData = new JSONObject();	//push for native 
		String pushId = "";
		int TYPE=3;

		nativeData.put("title", "고객정보 변경");
		nativeData.put("subTitle", "고객정보가 업데이트 되었습니다.");
		data.put("customer", customer);


		pushData.put("native", nativeData);
		pushData.put("data", data);
		pushData.put("type", TYPE);

		sendData.put("native", nativeData);
		sendData.put("type", TYPE);

		SendedPushDAO dao = context.getBean(SendedPushDAO.class);
		SendedPushDTO dto = new SendedPushDTO();
		dto.setDId(dId);
		dto.setData(pushData.toString());
		dto.setIsComplete(true);
		dto.setType(TYPE);
		dto.setCId(customer.getString("id"));
		pushId = dao.insert(dto);
		sendData.put("id", pushId);

		return sendData;
	}
}
