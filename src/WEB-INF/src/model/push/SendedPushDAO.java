package model.push;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import org.springframework.dao.DuplicateKeyException;


public class SendedPushDAO{
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}
	private String makePushId(){
		UUID uid = UUID.randomUUID();
		return uid.toString();

	}
	public boolean setComplete(String pushId){
		boolean succ = true;
		String sql = "update sended_push set is_complete = true where id = ?";
		Object[] args = new Object[]{
			pushId
		};
		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			succ=false;
			ex.printStackTrace();
		}   
		return succ;
	}   
	public boolean setFetch(int[] num){
		boolean succ = true;
		if(num.length == 0){
			return true;
		}
		String sql = "update sended_push set is_fetch = true where num in (";
		Object args[] = new Object[num.length];
		try{
			for(int i=0;i<num.length;i++){
				sql+="?";
				args[i] = num[i];
				if(i < num.length-1){
					sql+=", ";
				}
			}
			sql+=")";
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			succ=false;
			ex.printStackTrace();
		}   
		return succ;
	}   
	public String insert(final SendedPushDTO dto){
		boolean succ = true;
		String sql = "insert into sended_push(d_id, data, insert_date, id, is_fetch, is_complete, type, c_id) values(?, ?, now(), ?, ?, ?, ?, ?)";
		String id = makePushId();
		Object args[] = new Object[]{
			dto.getDId(),
			dto.getData(),
			id,
			dto.getIsFetch(),
			dto.getIsComplete(),
			dto.getType(),
			dto.getCId()

		};  
		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			id = null;
		}   

		return id;
	}   


	public SendedPushDTO fetchPush(String dId, String pushId){
		String sql = "select data, num, d_id, is_complete, insert_date, id, is_fetch, is_complete from sended_push where d_id=?  and  id = ? and is_fetch = false order by insert_date asc";
		SendedPushDTO push = new SendedPushDTO();
		Object[] args = new Object[]{
			dId,
			pushId
		};


		try{
			push = this.jdbcTemplate.queryForObject(sql, args, new RowMapper<SendedPushDTO>(){
				public SendedPushDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					SendedPushDTO dto = new SendedPushDTO();		
					dto.setNum(rs.getInt("num"));
					dto.setData(rs.getString("data"));
					dto.setDId(rs.getString("d_id"));
					dto.setIsComplete(rs.getBoolean("is_complete"));
					dto.setInsertDate(rs.getString("insert_date"));
					dto.setIsFetch(rs.getBoolean("is_fetch"));
					dto.setId(rs.getString("id"));
					return dto;
				}   
			}); 


		}catch(EmptyResultDataAccessException e){ 
			push = null;
		}catch(Exception ex){
			ex.printStackTrace();
		}   
		return push;

	}
	public List<SendedPushDTO> fetchAllPush(String dId){
		String sql = "select data, num, d_id, is_complete, insert_date, id, is_complete, is_fetch from sended_push where d_id=?  and (is_fetch=false or is_complete = false ) order by insert_date asc";
		List<SendedPushDTO> list =new ArrayList<SendedPushDTO>();
		Object[] args = new Object[]{
			dId
		};
		try{
			list= this.jdbcTemplate.query(sql, args, new RowMapper<SendedPushDTO>(){
				public SendedPushDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					SendedPushDTO dto = new SendedPushDTO();		
					dto.setNum(rs.getInt("num"));
					dto.setData(rs.getString("data"));
					dto.setDId(rs.getString("d_id"));
					dto.setIsComplete(rs.getBoolean("is_complete"));
					dto.setInsertDate(rs.getString("insert_date"));
					dto.setIsFetch(rs.getBoolean("is_fetch"));
					dto.setId(rs.getString("id"));
					return dto;
				}   
			}); 
		}catch(EmptyResultDataAccessException e){ 
			list = new ArrayList<SendedPushDTO>();
		}catch(Exception ex){
			ex.printStackTrace();
		}   
		return list;
	}
	public List<SendedPushDTO> getNotCompletePush(String dId){
		String sql = "select data, num, d_id, is_complete, insert_date, id, is_fetch, is_complete from sended_push where d_id=?  and is_fetch=false or is_complete=false order by insert_date asc";
		List<SendedPushDTO> list =new ArrayList<SendedPushDTO>();
		Object[] args = new Object[]{
			dId
		};
		try{
			list= this.jdbcTemplate.query(sql, args, new RowMapper<SendedPushDTO>(){
				public SendedPushDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					SendedPushDTO dto = new SendedPushDTO();		
					dto.setNum(rs.getInt("num"));
					dto.setData(rs.getString("data"));
					dto.setDId(rs.getString("d_id"));
					dto.setIsComplete(rs.getBoolean("is_complete"));
					dto.setIsFetch(rs.getBoolean("is_fetch"));
					dto.setInsertDate(rs.getString("insert_date"));
					dto.setId(rs.getString("id"));
					return dto;
				}   
			}); 
		}catch(EmptyResultDataAccessException e){ 
			list = new ArrayList<SendedPushDTO>();
		}catch(Exception ex){
			ex.printStackTrace();
		}   
		return list;
	}
	public int getUnfetchedCount(String dId){
		String sql = "select count(*) as count from sended_push where d_id=?  and is_complete=false";
		int count =0;
		Object[] args = new Object[]{
			dId
		};
		try{
			count = this.jdbcTemplate.queryForObject(sql, args, Integer.class); 
		}catch(Exception ex){
			ex.printStackTrace();
		}   
		return count;
	}
	public boolean completeDup(SendedPushDTO dto){
		boolean succ = true;
		String sql = "update sended_push set is_complete = true, is_fetch=true where d_id = ? and type = ? and c_id = ?";
		Object[] args = new Object[]{
			dto.getDId(),
			dto.getType(),
			dto.getCId()
		};
		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			succ=false;
			ex.printStackTrace();
		}   
		return succ;
	}   

}
