package model.push;
public class SendedPushDTO{
	private int num;
	private String dId;
	private String data;
	private String insertDate;
	private String id;
	private boolean isComplete;
	private boolean isFetch;
	private int type;
	private String cId;
	
	public SendedPushDTO(){
		isComplete=false;
		isFetch=false;
		cId = null;
	};
	/**
	 * Get num.
	 *
	 * @return num as int.
	 */
	public int getNum()
	{
	    return num;
	}
	
	/**
	 * Set num.
	 *
	 * @param num the value to set.
	 */
	public void setNum(int num)
	{
	    this.num = num;
	}
	
	/**
	 * Get dId.
	 *
	 * @return dId as String.
	 */
	public String getDId()
	{
	    return dId;
	}
	
	/**
	 * Set dId.
	 *
	 * @param dId the value to set.
	 */
	public void setDId(String dId)
	{
	    this.dId = dId;
	}
	
	/**
	 * Get data.
	 *
	 * @return data as String.
	 */
	public String getData()
	{
	    return data;
	}
	
	/**
	 * Set data.
	 *
	 * @param data the value to set.
	 */
	public void setData(String data)
	{
	    this.data = data;
	}
	
	/**
	 * Get insertDate.
	 *
	 * @return insertDate as String.
	 */
	public String getInsertDate()
	{
	    return insertDate;
	}
	
	/**
	 * Set insertDate.
	 *
	 * @param insertDate the value to set.
	 */
	public void setInsertDate(String insertDate)
	{
	    this.insertDate = insertDate;
	}
	
	/**
	 * Get isComplete.
	 *
	 * @return isComplete as boolean.
	 */
	public boolean getIsComplete()
	{
	    return isComplete;
	}
	
	/**
	 * Set isComplete.
	 *
	 * @param isComplete the value to set.
	 */
	public void setIsComplete(boolean isComplete)
	{
	    this.isComplete = isComplete;
	}
	
	/**
	 * Get id.
	 *
	 * @return id as String.
	 */
	public String getId()
	{
	    return id;
	}
	
	/**
	 * Set id.
	 *
	 * @param id the value to set.
	 */
	public void setId(String id)
	{
	    this.id = id;
	}
	
	/**
	 * Get isFetch.
	 *
	 * @return isFetch as boolean.
	 */
	public boolean getIsFetch()
	{
	    return isFetch;
	}
	
	/**
	 * Set isFetch.
	 *
	 * @param isFetch the value to set.
	 */
	public void setIsFetch(boolean isFetch)
	{
	    this.isFetch = isFetch;
	}
	
	/**
	 * Get type.
	 *
	 * @return type as int.
	 */
	public int getType()
	{
	    return type;
	}
	
	/**
	 * Set type.
	 *
	 * @param type the value to set.
	 */
	public void setType(int type)
	{
	    this.type = type;
	}
	
	/**
	 * Get cId.
	 *
	 * @return cId as String.
	 */
	public String getCId()
	{
	    return cId;
	}
	
	/**
	 * Set cId.
	 *
	 * @param cId the value to set.
	 */
	public void setCId(String cId)
	{
	    this.cId = cId;
	}
}
