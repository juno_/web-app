package model.customer;
public class CDMapDTO {
	private String cId;
	private String cName;

	private String branch;

	private String dId;
	private String dName;

	private String lastVisitDate;

	
	/**
	 * Get cId.
	 *
	 * @return cId as String.
	 */
	public String getCId()
	{
	    return cId;
	}
	
	/**
	 * Set cId.
	 *
	 * @param cId the value to set.
	 */
	public void setCId(String cId)
	{
	    this.cId = cId;
	}
	
	/**
	 * Get cName.
	 *
	 * @return cName as String.
	 */
	public String getCName()
	{
	    return cName;
	}
	
	/**
	 * Set cName.
	 *
	 * @param cName the value to set.
	 */
	public void setCName(String cName)
	{
	    this.cName = cName;
	}
	
	/**
	 * Get branch.
	 *
	 * @return branch as int.
	 */
	public String getBranch()
	{
	    return branch;
	}
	
	/**
	 * Set branch.
	 *
	 * @param branch the value to set.
	 */
	public void setBranch(String branch)
	{
	    this.branch = branch;
	}
	
	/**
	 * Get dId.
	 *
	 * @return dId as String.
	 */
	public String getDId()
	{
	    return dId;
	}
	
	/**
	 * Set dId.
	 *
	 * @param dId the value to set.
	 */
	public void setDId(String dId)
	{
	    this.dId = dId;
	}
	
	/**
	 * Get dName.
	 *
	 * @return dName as String.
	 */
	public String getDName()
	{
	    return dName;
	}
	
	/**
	 * Set dName.
	 *
	 * @param dName the value to set.
	 */
	public void setDName(String dName)
	{
	    this.dName = dName;
	}
	
	/**
	 * Get lastVisitDate.
	 *
	 * @return lastVisitDate as String.
	 */
	public String getLastVisitDate()
	{
	    return lastVisitDate;
	}
	
	/**
	 * Set lastVisitDate.
	 *
	 * @param lastVisitDate the value to set.
	 */
	public void setLastVisitDate(String lastVisitDate)
	{
	    this.lastVisitDate = lastVisitDate;
	}
}
	

