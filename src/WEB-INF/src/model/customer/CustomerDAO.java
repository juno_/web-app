package model.customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import org.springframework.dao.DuplicateKeyException;

import model.visithistory.VisitHistoryDTO;
import model.designer.DesignerDTO;

public class CustomerDAO{
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}


	public boolean insert(final CustomerDTO dto) throws DuplicateKeyException{
		boolean succ = true;
		String sql = "insert into customer(id, name, branch, last_phone, gender, birth) values(?, ?, ?, ?, ?, ?)";
		Object args[] = new Object[]{
			dto.getId(),
			dto.getName(),
			dto.getBranch(),
			dto.getLastPhone(),
			dto.getGender(),
			dto.getBirth(),
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(DuplicateKeyException ex){
			throw ex;
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}

		return succ;

	}
	public List<DesignerDTO> selectMyDesigner(String customerId){               
                String sql = "select designer.id, designer.name, push_key, phone_dist from designer, c_d_map where designer.id = c_d_map.d_id and c_d_map.c_id = ? and is_valid=true"; 
                List<DesignerDTO> result = new ArrayList<DesignerDTO>();            

                Object args[] = new Object[]{                                       
                        customerId
                };          

                try{        
                        result = this.jdbcTemplate.query(sql, args, new RowMapper<DesignerDTO>(){    

                                public DesignerDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                                        DesignerDTO dto = new DesignerDTO();        
					dto.setId(rs.getString("designer.id"));
					dto.setName(rs.getString("designer.name"));
					dto.setPushKey(rs.getString("push_key"));
					dto.setPhoneDist(rs.getString("phone_dist"));
                                        return dto;                                 
                                }                                                   
                        });


                }catch(EmptyResultDataAccessException e){ 
                        result = new ArrayList<DesignerDTO>();                      
                }catch(Exception ex){
                        ex.printStackTrace();                                       
                }   
                return result;

        }
	public boolean updateCDMapShow(String designerId, String customerId, boolean show) {
		boolean succ = true;
		String sql = "update c_d_map set is_show = ? where c_id =? and d_id = ?";
		Object args[] = new Object[]{
			show,
			customerId,
			designerId
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}

		return succ;
	}
	public boolean insertCDMap(String designerId, String customerId) {
		boolean succ = true;
		String sql = "insert into c_d_map(c_id, d_id, fixed_memo) values(?, ?, ?)";
		Object args[] = new Object[]{
			customerId,
			designerId,
			""
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}

		return succ;

	}
	public boolean isMapped(String designerId, String customerId){

		String sql = "select count(*) from c_d_map where d_id=? and c_id=?";
		Object args[] = new Object[]{
			designerId,
			customerId
		};
		int count=0;
		boolean re=true;
		try{
			count = this.jdbcTemplate.queryForObject(sql, args, Integer.class);
			if(count >= 1){
				re=true;
			}else{
				re=false;
			}


		}catch(Exception ex){
			ex.printStackTrace();
			re=true;
		}
		return re;
	}
	public boolean isHideCustomer(String designerId, String customerId){

		String sql = "select count(*) from c_d_map where d_id=? and c_id=? and is_show = false";
		Object args[] = new Object[]{
			designerId,
			customerId
		};
		int count=0;
		boolean re=true;
		try{
			count = this.jdbcTemplate.queryForObject(sql, args, Integer.class);
			if(count >= 1){
				re=true;
			}else{
				re=false;
			}


		}catch(Exception ex){
			ex.printStackTrace();
			re=true;
		}
		return re;
	}


	public boolean updateFixedMemo(String designerId, String customerId, String fixedMemo){
		String sql = "update c_d_map set fixed_memo = ? where c_id = ? and d_id=?";
		boolean result=true;
		int updateCount=0;

		Object args[] = new Object[]{
			fixedMemo,
			customerId,
			designerId
		};

		try{
			updateCount = this.jdbcTemplate.update(sql, args);
			result = (updateCount == 1);
		}catch(Exception ex){
			result=false;
			ex.printStackTrace();
		}

		return result;
	}
	public boolean isExist(String id){
		//String sql = "select count(*) from customer";
		String sql = "select count(*) from customer where id=?";
		int total =0; 
		boolean re = false;
		Object[] args = new Object[]{
			id
		};
		try{
			total= this.jdbcTemplate.queryForObject(sql, args, Integer.class);
			if(total >= 1){
				re=true;
			}else{
				re=false;
			}

		}catch(Exception ex){
			ex.printStackTrace();
			re = true;
		}
		return re;
	}
	public int getTotal(String branch){
		//String sql = "select count(*) from customer";
		String sql = "select count(distinct c_id) from c_d_map left join designer on c_d_map.d_id = designer.id where branch=?";
		int total =0; 
		Object[] args = new Object[]{
			branch
		};
		try{
			total= this.jdbcTemplate.queryForObject(sql, args, Integer.class);

		}catch(Exception ex){
			ex.printStackTrace();
			total = 0;
		}
		return total;
	}
	public List<CustomerDTO> selectByKeyword(String keyword){
		String sql = "select id, name, branch, last_phone, gender, birth from customer where id=? or name like ? ";
		Object args[] = new Object[]{
			keyword,
			"%"+keyword+"%"
		};
		List<CustomerDTO> customer = new ArrayList<CustomerDTO>();
		try{
			customer = this.jdbcTemplate.query(sql, args, new RowMapper<CustomerDTO>(){
					public CustomerDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						CustomerDTO dto = new CustomerDTO();
						dto.setId(rs.getString("id"));
						dto.setName(rs.getString("name"));
						dto.setBranch(rs.getString("branch"));
						dto.setLastPhone(rs.getString("last_phone"));
						dto.setGender(rs.getString("gender"));
						dto.setBirth(rs.getString("birth"));
						return dto;
					}
			});

		}catch(EmptyResultDataAccessException ex){
			System.out.println(ex);
			customer = new ArrayList<CustomerDTO>();

		}catch(Exception ex){
			ex.printStackTrace();
			customer = new ArrayList<CustomerDTO>();
		}
		return customer;
	}
	public CustomerDTO select(String customerId){
		String sql = "select id, name, branch, last_phone, gender, birth from customer where id=?";
		Object args[] = new Object[]{
			customerId
		};
		CustomerDTO customer = new CustomerDTO();
		try{
			customer = this.jdbcTemplate.queryForObject(sql, args, new RowMapper<CustomerDTO>(){
					public CustomerDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						CustomerDTO dto = new CustomerDTO();
						dto.setId(rs.getString("id"));
						dto.setName(rs.getString("name"));
						dto.setBranch(rs.getString("branch"));
						dto.setLastPhone(rs.getString("last_phone"));
						dto.setGender(rs.getString("gender"));
						dto.setBirth(rs.getString("birth"));
						return dto;
					}
			});

		}catch(EmptyResultDataAccessException ex){
			customer = null;

		}catch(Exception ex){
			ex.printStackTrace();
			customer = null;
		}
		return customer;
	}
	public VisitHistoryDTO selectFirstVisit(String customerId, String branch){
		String sql = "select customer.id, customer.name, designer.id, designer.name from customer left join (select * from c_d_map, designer where c_d_map.d_id=designer.id and branch=?) c_d_map on customer.id=c_d_map.c_id left join designer on c_d_map.d_id=designer.id where customer.id=? ";


		Object args[] = new Object[]{
			branch,
			customerId
		};
		VisitHistoryDTO vDto= new VisitHistoryDTO();
		try{
			vDto = this.jdbcTemplate.queryForObject(sql, args, new RowMapper<VisitHistoryDTO>(){
					public VisitHistoryDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						VisitHistoryDTO dto = new VisitHistoryDTO();
						dto.setCId(rs.getString("customer.id"));
						dto.setCName(rs.getString("customer.name"));
						dto.setDId(rs.getString("designer.id"));
						dto.setDName(rs.getString("designer.name"));
						dto.setVisitDate("");
						dto.setBranch(branch);

						return dto;
					}
			});

		}catch(EmptyResultDataAccessException ex){
			vDto = null;

		}catch(Exception ex){
			ex.printStackTrace();
			vDto = null;
		}
		return vDto;
	}
	public boolean update(CustomerDTO dto){
		String sql = "update customer set name= ?, gender=?, last_phone=?, birth=? where id = ?";
		boolean result=true;
		int updateCount=0;

		Object args[] = new Object[]{
			dto.getName(),
			dto.getGender(),
			dto.getLastPhone(),
			dto.getBirth(),
			dto.getId()

		};

		try{
			updateCount = this.jdbcTemplate.update(sql, args);
			result = (updateCount == 1);
		}catch(Exception ex){
			result=false;
			ex.printStackTrace();
		}

		return result;
	}
}
