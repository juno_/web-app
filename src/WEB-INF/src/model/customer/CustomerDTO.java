package model.customer;
public class CustomerDTO {
	private String id;
	private String name;
	private String fixedMemo;
	private String branch;
	private String lastPhone;
	private String gender;
	private String birth;
	
	/**
	 * Get id.
	 *
	 * @return id as String.
	 */
	public String getId()
	{
	    return id;
	}
	
	/**
	 * Set id.
	 *
	 * @param id the value to set.
	 */
	public void setId(String id)
	{
	    this.id = id;
	}
	
	/**
	 * Get name.
	 *
	 * @return name as String.
	 */
	public String getName()
	{
	    return name;
	}
	
	/**
	 * Set name.
	 *
	 * @param name the value to set.
	 */
	public void setName(String name)
	{
	    this.name = name;
	}
	
	/**
	 * Get fixedMemo.
	 *
	 * @return fixedMemo as String.
	 */
	public String getFixedMemo()
	{
	    return fixedMemo;
	}
	
	/**
	 * Set fixedMemo.
	 *
	 * @param fixedMemo the value to set.
	 */
	public void setFixedMemo(String fixedMemo)
	{
	    this.fixedMemo = fixedMemo;
	}
	
	/**
	 * Get branch.
	 *
	 * @return branch as String.
	 */
	public String getBranch()
	{
	    return branch;
	}
	
	/**
	 * Set branch.
	 *
	 * @param branch the value to set.
	 */
	public void setBranch(String branch)
	{
	    this.branch = branch;
	}
	
	/**
	 * Get lastPhone.
	 *
	 * @return lastPhone as String.
	 */
	public String getLastPhone()
	{
	    return lastPhone;
	}
	
	/**
	 * Set lastPhone.
	 *
	 * @param lastPhone the value to set.
	 */
	public void setLastPhone(String lastPhone)
	{
	    this.lastPhone = lastPhone;
	}
	
	/**
	 * Get gender.
	 *
	 * @return gender as String.
	 */
	public String getGender()
	{
	    return gender;
	}
	
	/**
	 * Set gender.
	 *
	 * @param gender the value to set.
	 */
	public void setGender(String gender)
	{
	    this.gender = gender;
	}
	
	/**
	 * Get birth.
	 *
	 * @return birth as String.
	 */
	public String getBirth()
	{
	    return birth;
	}
	
	/**
	 * Set birth.
	 *
	 * @param birth the value to set.
	 */
	public void setBirth(String birth)
	{
	    this.birth = birth;
	}
	
}

