package model.stats;
public class StatsDetailDTO extends StatsDTO{
	private String dist;

	
	public StatsDetailDTO(String x, String y, String dist){
		super(x, y);
		this.dist = dist;
	}
	public StatsDetailDTO(String x, String y){
		this.dist = dist;
	}
	public StatsDetailDTO(){ }

	
	
	/**
	 * Get dist.
	 *
	 * @return dist as String.
	 */
	public String getDist()
	{
	    return dist;
	}
	
	/**
	 * Set dist.
	 *
	 * @param dist the value to set.
	 */
	public void setDist(String dist)
	{
	    this.dist = dist;
	}
}
