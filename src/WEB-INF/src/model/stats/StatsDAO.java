package model.stats;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import org.springframework.dao.DuplicateKeyException;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class StatsDAO{
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}

	private List<StatsDetailDTO> fillDateHour(List<StatsDetailDTO> list, String sDate, String eDate){
		if(sDate.compareTo(eDate) > 0){ 
                        return list;
                }   
                Calendar startCal = Calendar.getInstance();
                SimpleDateFormat formatter=new SimpleDateFormat();
                int inc = 0;
                String [] dateSplit = sDate.split("-");
                startCal.set(Integer.parseInt(dateSplit[0]), Integer.parseInt(dateSplit[1])-1, Integer.parseInt(dateSplit[2]));
                inc = Calendar.DATE;
                formatter = new SimpleDateFormat("yyyy-MM-dd");

                List<StatsDetailDTO> filledList = new ArrayList<StatsDetailDTO>();
                int listIdx=0;

                String nowDate = formatter.format(startCal.getTime());

                int hourIdx=0;
                while(true){
                        if(listIdx < list.size() && list.get(listIdx).getDist().equals(nowDate)){
                                int i=0;
                                for(i=0;i<24;i++){
					if(listIdx >= list.size()){
						break;
					}
                                        if(list.get(listIdx).getDist().equals(nowDate) && Integer.parseInt(list.get(listIdx).getX()) == i){ 
                                                filledList.add(list.get(listIdx));
                                                listIdx++;
                                        }else if(list.get(listIdx).getDist().equals(nowDate)){
                                                StatsDetailDTO dto = new StatsDetailDTO( String.format("%02d", i), "0",  nowDate);
                                                filledList.add(dto);
                                        }else if(!list.get(listIdx).getDist().equals(nowDate)){
                                                break;
                                        }   
                                }   
                                for(;i<24;i++){
                                        StatsDetailDTO dto = new StatsDetailDTO( String.format("%02d", i), "0",  nowDate);
                                        filledList.add(dto);
                                }   
                        }else{
                                hourIdx=0;
                                for(int i=0;i<24;i++){
                                        StatsDetailDTO dto = new StatsDetailDTO( String.format("%02d", i), "0",  nowDate);
                                        filledList.add(dto);
                                }   
                        }   

                        startCal.add(inc, 1); 
			nowDate = formatter.format(startCal.getTime());
                        if(nowDate.substring(0, sDate.length()).compareTo(eDate) > 0){
                                break;

                        }

                }
                return filledList;


	}


	public int getTodayVisit(String designerId){
		String sql = "select count(*) from visit_history where d_id=? and visit_date like ?";
		String today = "";
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar todayCal = Calendar.getInstance();
		today = formatter.format(todayCal.getTime());

		Object args[] = new Object[]{
			designerId,
			today+"%"
		};
		int result = 0;
		try{
			result = this.jdbcTemplate.queryForObject(sql,args,Integer.class);
		

		}catch(EmptyResultDataAccessException ex){
			System.out.println(ex);

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
	public List<StatsDetailDTO> visitWithTime(String designerId, String sDate, String eDate){
		String sql = "select date_format(visit_date, '%H') as x, left(visit_date, 10) as dist, count(*) as y from visit_history where d_id=? and left(visit_date, 10) >= ? and left(visit_date, 10) <= ? group by x, dist order by dist, x";
		Object args[] = new Object[]{
			designerId,
			sDate, 
			eDate
		};
		List<StatsDetailDTO> statistic = new ArrayList<StatsDetailDTO>();
		try{
			statistic = this.jdbcTemplate.query(sql, args, new RowMapper<StatsDetailDTO>(){
					public StatsDetailDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						StatsDetailDTO dto = new StatsDetailDTO(
							rs.getString("x"),
							rs.getString("y"),
							rs.getString("dist")
						);
						return dto;
					}
			});
			statistic = fillDateHour(statistic, sDate, eDate);
		

		}catch(EmptyResultDataAccessException ex){
			System.out.println(ex);
			statistic = new ArrayList<StatsDetailDTO>();

		}catch(Exception ex){
			ex.printStackTrace();
			statistic = new ArrayList<StatsDetailDTO>();
		}
		return statistic;
	}
	public List<StatsDetailDTO> visitWithMonth(String designerId, String yearMonth){

		String sql = "select left(visit_date, 10) as x, week(visit_date, 4) - week(date_sub(visit_date, interval dayofmonth(visit_date)-1 day), 4) +1 as dist, count(*) as y from visit_history where d_id=? and left(visit_date, 7) >= ? group by x, dist order by dist, x";
		Object args[] = new Object[]{
			designerId,
			yearMonth
		};
		List<StatsDetailDTO> statistic = new ArrayList<StatsDetailDTO>();
		try{
			statistic = this.jdbcTemplate.query(sql, args, new RowMapper<StatsDetailDTO>(){
					public StatsDetailDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						StatsDetailDTO dto = new StatsDetailDTO(
							rs.getString("x"),
							rs.getString("y"),
							rs.getString("dist")
						);
						return dto;
					}
			});

		}catch(EmptyResultDataAccessException ex){
			System.out.println(ex);
			statistic = new ArrayList<StatsDetailDTO>();

		}catch(Exception ex){
			ex.printStackTrace();
			statistic = new ArrayList<StatsDetailDTO>();
		}
		return statistic;
	}
	
	public List<StatsDetailDTO> visitWithYear(String designerId, String year){

		String sql = "select substr(visit_date, 6, 2) as dist, week(visit_date, 4) - week(date_sub(visit_date, interval dayofmonth(visit_date)-1 day), 4) +1 as x, count(*) as y from visit_history where d_id=? and left(visit_date, 4) = ? group by x, dist order by dist, x";
		Object args[] = new Object[]{
			designerId,
			year
		};
		List<StatsDetailDTO> statistic = new ArrayList<StatsDetailDTO>();
		try{
			statistic = this.jdbcTemplate.query(sql, args, new RowMapper<StatsDetailDTO>(){
					public StatsDetailDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						StatsDetailDTO dto = new StatsDetailDTO(
							rs.getString("x"),
							rs.getString("y"),
							rs.getString("dist")
						);
						return dto;
					}
			});

		}catch(EmptyResultDataAccessException ex){
			System.out.println(ex);
			statistic = new ArrayList<StatsDetailDTO>();

		}catch(Exception ex){
			ex.printStackTrace();
			statistic = new ArrayList<StatsDetailDTO>();
		}
		return statistic;
	}
	public List<StatsDTO> getGender(String designerId){

		String sql = "select count(*) as y, gender as x from customer, c_d_map where c_d_map.c_id = customer.id and d_id = ? and is_show=true group by x";
		Object args[] = new Object[]{
			designerId
		};
		List<StatsDTO> statistic = new ArrayList<StatsDTO>();
		try{
			statistic = this.jdbcTemplate.query(sql, args, new RowMapper<StatsDTO>(){
					public StatsDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						StatsDTO dto = new StatsDTO(
							rs.getString("x"),
							rs.getString("y")
						);
						return dto;
					}
			});

		}catch(EmptyResultDataAccessException ex){
			System.out.println(ex);
			statistic = new ArrayList<StatsDTO>();

		}catch(Exception ex){
			ex.printStackTrace();
			statistic = new ArrayList<StatsDTO>();
		}
		return statistic;
	}
	public List<StatsDetailDTO> getAge(String designerId){
		String sql = "select sum(tot) as y, gender, case when b between 1 and 9 then b else -100  end as x from (select count(*) as tot, gender, floor((2015 + 1 - case length(birth) when 8 then left(birth, 4) when 6 then concat('19', left(birth, 2)) else 4000 end)/10)  as b from customer, c_d_map where customer.id = c_d_map.c_id and d_id =? and c_d_map.is_show=true group by b, gender order by gender, b) as t group by gender, x";			//4000 is dummy value for unknown birthday value
		Object args[] = new Object[]{
			designerId
		};
		List<StatsDetailDTO> statistic = new ArrayList<StatsDetailDTO>();
		try{
			statistic = this.jdbcTemplate.query(sql, args, new RowMapper<StatsDetailDTO>(){
					public StatsDetailDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						StatsDetailDTO dto = new StatsDetailDTO(
							rs.getString("x"),
							rs.getString("y"),
							rs.getString("gender")
						);
						return dto;
					}
			});
		}catch(EmptyResultDataAccessException ex){
			System.out.println(ex);
			statistic = new ArrayList<StatsDetailDTO>();

		}catch(Exception ex){
			ex.printStackTrace();
			statistic = new ArrayList<StatsDetailDTO>();
		}
		return statistic;
	}
	public List<StatsDTO> getBusyDay(String designerId, String sDate, String eDate){
		String sql = "select count(*) as y, dayofweek(visit_date) - 1 as x from visit_history where d_id = ? and left(visit_date, 10) <= ? and left(visit_date, 10) >= ? group by x";
		Object args[] = new Object[]{
			designerId,
			eDate, 
			sDate
		};
		List<StatsDTO> statistic = new ArrayList<StatsDTO>();
		try{
			statistic = this.jdbcTemplate.query(sql, args, new RowMapper<StatsDTO>(){
					public StatsDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						StatsDTO dto = new StatsDTO(
							rs.getString("x"),
							rs.getString("y")
						);
						return dto;
					}
			});
		}catch(EmptyResultDataAccessException ex){
			System.out.println(ex);
			statistic = new ArrayList<StatsDTO>();

		}catch(Exception ex){
			ex.printStackTrace();
			statistic = new ArrayList<StatsDTO>();
		}
		return statistic;
	}
	public List<StatsDetailDTO> getProcedures(String designerId, String sDate, String eDate){
		String sql = "select count(pt) as y, b.name as x, b.txt as dist from (select substring_index(substring_index(procedure_type, '|', procedures.num), '|', -1) pt, gender  from customer, procedures inner join memo on char_length(procedure_type)-char_length(replace(procedure_type, '|', '')) >= procedures.num -1  where procedure_type <> '' and customer.id=memo.c_id and d_id = ? and left(write_date, 10) <= ? and left(write_date, 10) >= ? ) a right outer join (select num, name, txt from procedures, gender) b on a.pt=b.num and a.gender=b.txt group by b.name, b.txt";
		Object args[] = new Object[]{
			designerId,
			eDate, 
			sDate
		};
		List<StatsDetailDTO> statistic = new ArrayList<StatsDetailDTO>();
		try{
			statistic = this.jdbcTemplate.query(sql, args, new RowMapper<StatsDetailDTO>(){
					public StatsDetailDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						StatsDetailDTO dto = new StatsDetailDTO(
							rs.getString("x"),
							rs.getString("y"),
							rs.getString("dist")
						);
						return dto;
					}
			});


		}catch(EmptyResultDataAccessException ex){
			System.out.println(ex);
			statistic = new ArrayList<StatsDetailDTO>();

		}catch(Exception ex){
			ex.printStackTrace();
			statistic = new ArrayList<StatsDetailDTO>();
		}
		return statistic;
	}
	public List<StatsDTO> getProceduresNoGender(String designerId, String sDate, String eDate){
		String sql = "select max(y) as y, x from (select count(pt) as y, b.name as x from (select substring_index(substring_index(procedure_type, '|', procedures.num), '|', -1) pt  from customer, procedures inner join memo on char_length(procedure_type)-char_length(replace(procedure_type, '|', '')) >= procedures.num -1  where procedure_type <> '' and customer.id=memo.c_id and d_id = ? and left(write_date, 10) <= ? and left(write_date, 10) >= ? ) a right outer join (select num, name from procedures) b on a.pt=b.num group by b.name) t";
		Object args[] = new Object[]{
			designerId,
			eDate, 
			sDate
		};
		List<StatsDTO> statistic = new ArrayList<StatsDTO>();
		try{
			statistic = this.jdbcTemplate.query(sql, args, new RowMapper<StatsDTO>(){
					public StatsDTO mapRow (ResultSet rs, int rowNum) throws SQLException {
						StatsDTO dto = new StatsDTO(
							rs.getString("x"),
							rs.getString("y")
						);
						return dto;
					}
			});


		}catch(EmptyResultDataAccessException ex){
			System.out.println(ex);
			statistic = new ArrayList<StatsDTO>();

		}catch(Exception ex){
			ex.printStackTrace();
			statistic = new ArrayList<StatsDTO>();
		}
		return statistic;
	}

}
