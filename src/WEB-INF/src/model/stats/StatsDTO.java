package model.stats;
public class StatsDTO {
	private String x;
	private String y;

	
	public StatsDTO(String x, String y){
		this.x = x;
		this.y = y;
	}
	public StatsDTO(){ }

	
	/**
	 * Get x.
	 *
	 * @return x as String.
	 */
	public String getX()
	{
	    return x;
	}
	
	/**
	 * Set x.
	 *
	 * @param x the value to set.
	 */
	public void setX(String x)
	{
	    this.x = x;
	}
	
	/**
	 * Get y.
	 *
	 * @return y as String.
	 */
	public String getY()
	{
	    return y;
	}
	
	/**
	 * Set y.
	 *
	 * @param y the value to set.
	 */
	public void setY(String y)
	{
	    this.y = y;
	}
}
