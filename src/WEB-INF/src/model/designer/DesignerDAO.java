package model.designer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import model.customer.CustomerDTO;

public class DesignerDAO{
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}

        public boolean isExist(String id){
                //String sql = "select count(*) from customer";
                String sql = "select count(*) from designer where id=?";
                int total =0; 
                boolean re = false;
                Object[] args = new Object[]{
                        id  
                };  
                try{
                        total= this.jdbcTemplate.queryForObject(sql, args, Integer.class);
                        if(total >= 1){ 
                                re=true;
                        }else{
                                re=false;
                        }   

                }catch(Exception ex){
                        ex.printStackTrace();
                        re = true;
                }   
                return re; 
        }
	public boolean changePassword(String designerId, String newPw){
		boolean succ = true;
		String sql = "update designer set password = password(?) where id= ? ";
		Object args[] = new Object[]{
			newPw,
			designerId
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}

		return succ;

	}

	public boolean insert(final DesignerDTO dto){
		boolean succ = true;
		String sql = "insert into designer (id, name, password, branch) values(?, ?, password(?), ?)";
		Object args[] = new Object[]{
			dto.getId(),
			dto.getName(),
			dto.getPassword(),
			dto.getBranch()
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}

		return succ;

	}
	public boolean savePushKey(String designerId, String regId, String phoneDist){
		boolean succ = false;
		String sql = "update designer set push_key = ?, phone_dist=? where id=?";
		Object args[] = new Object[]{
			regId,
			phoneDist,
			designerId
		};
		
		try{
			int reCount=0;
			reCount = this.jdbcTemplate.update(sql, args);
			if(reCount == 1){
				succ=true;
			}
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}
		/*
		String sql = "insert into memo(c_id, content, write_date, timestamp, d_id) values(?, ?, ?, ?, ?)";
		Object args[] = new Object[]{
			dto.getCId(),
			dto.getContent(),
			dto.getDate(),
			dto.getTimestamp(),
			dto.getDId()
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}
		*/

		return succ;

	}
	public DesignerDTO signIn(String id, String password){
		String sql = "select id, name, branch from designer where id = ? and password = password(?) and is_valid = true";

		DesignerDTO designer = null;
		Object args[] = new Object[]{
			id,
			password
		};

		try{
			designer = this.jdbcTemplate.queryForObject(sql, args, new RowMapper<DesignerDTO>(){
					public DesignerDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
						DesignerDTO dto = new DesignerDTO();
						dto.setId(rs.getString("id"));
						dto.setName(rs.getString("name"));
						dto.setBranch(rs.getString("branch"));

						return dto;
					}
				});


		}catch(EmptyResultDataAccessException e){
			designer = null;	
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return designer; 
	}

	public List<CustomerDTO> selectMyCustomer(String designerId){
		String sql = "select customer.name, customer.id, fixed_memo, gender, last_phone from customer, c_d_map where customer.id = c_d_map.c_id and c_d_map.d_id = ? and c_d_map.is_show=true order by customer.name asc";
		List<CustomerDTO> result = new ArrayList<CustomerDTO>();

		Object args[] = new Object[]{
			designerId
		};

		try{
			result = this.jdbcTemplate.query(sql, args, new RowMapper<CustomerDTO>(){

				public CustomerDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					CustomerDTO dto = new CustomerDTO();
					dto.setName(rs.getString("customer.name"));
					dto.setId(rs.getString("customer.id"));
					dto.setFixedMemo(rs.getString("fixed_memo"));
					dto.setGender(rs.getString("gender"));
					dto.setLastPhone(rs.getString("last_phone"));
					return dto;
				}
			});


		}catch(EmptyResultDataAccessException e){
			result = new ArrayList<CustomerDTO>();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;

	}

	public List<DesignerDTO> selectList(String branch){
		String sql = "select id, name, branch from designer where branch = ? and is_valid=true order by name asc";
		List<DesignerDTO> result = new ArrayList<DesignerDTO>();

		Object args[] = new Object[]{
			branch	
		};

		try{
			result = this.jdbcTemplate.query(sql, args, new RowMapper<DesignerDTO>(){

				public DesignerDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DesignerDTO dto = new DesignerDTO();
					dto.setName(rs.getString("name"));
					dto.setId(rs.getString("id"));
					dto.setBranch(rs.getString("branch"));
					return dto;
				}
			});


		}catch(EmptyResultDataAccessException e){
			result = new ArrayList<DesignerDTO>();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;

	}
	public DesignerDTO select(String designerId){
		String sql = "select id, name, branch, push_key, phone_dist from designer where id = ?";

		Object args[] = new Object[]{
			designerId
		};
		DesignerDTO result=null;

		try{
			result = this.jdbcTemplate.queryForObject(sql, args, new RowMapper<DesignerDTO>(){

				public DesignerDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					DesignerDTO dto = new DesignerDTO();
					dto.setName(rs.getString("name"));
					dto.setId(rs.getString("id"));
					dto.setBranch(rs.getString("branch"));
					dto.setPushKey(rs.getString("push_key"));
					dto.setPhoneDist(rs.getString("phone_dist"));
					return dto;
				}
			});


		}catch(EmptyResultDataAccessException e){
			result = null ;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return result;

	}
	public boolean hideCustomer(String designerId, String customerId){
		boolean succ = false;
		String sql = "update c_d_map set is_show=false where d_id=? and c_id=?";
		String sqlMemo = "update memo set is_show=false where d_id=? and c_id=?";
		Object args[] = new Object[]{
			designerId,
			customerId
		};
		
		try{
			int reCount=0;
			succ = this.jdbcTemplate.update(sql, args)==1 ? true : false;
			this.jdbcTemplate.update(sqlMemo, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}
		/*
		String sql = "insert into memo(c_id, content, write_date, timestamp, d_id) values(?, ?, ?, ?, ?)";
		Object args[] = new Object[]{
			dto.getCId(),
			dto.getContent(),
			dto.getDate(),
			dto.getTimestamp(),
			dto.getDId()
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}
		*/

		return succ;

	}
}
