package model.visithistory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;


public class VisitHistoryDAO{
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}


	public boolean insert(final VisitHistoryDTO dto){
		boolean succ = true;
		String sql = "insert into visit_history(c_id, d_id, visit_date, branch ) values(?, ?, now(), ?)";
		Object args[] = new Object[]{
			dto.getCId(),
			dto.getDId(),
			dto.getBranch()
		};

		try{
			this.jdbcTemplate.update(sql, args);
		}catch(Exception ex){
			ex.printStackTrace();
			succ =false;
		}

		return succ;

	}
	public VisitHistoryDTO getLastVisit(String customerId, String branch){
		String sql = "select c_id, customer.name, d_id, designer.name, visit_date, visit_history.branch from visit_history, customer, designer where visit_history.c_id = customer.id and visit_history.d_id = designer.id and c_id= ? and visit_history.branch =? order by visit_date desc limit 0, 1";

		VisitHistoryDTO visitHistory = new VisitHistoryDTO();
		Object args[] = new Object[]{
			customerId, 
			branch
		};

		try{
			visitHistory = this.jdbcTemplate.queryForObject(sql, args, new RowMapper<VisitHistoryDTO>(){
					public VisitHistoryDTO mapRow(ResultSet rs, int rowNum) throws SQLException{
						VisitHistoryDTO dto = new VisitHistoryDTO();
						dto.setCId(rs.getString("c_id"));
						dto.setCName(rs.getString("customer.name"));
						dto.setDId(rs.getString("d_id"));
						dto.setDName(rs.getString("designer.name"));
						dto.setVisitDate(rs.getString("visit_date"));
						dto.setBranch(rs.getString("visit_history.branch"));

						return dto;
					}
				});


		}catch(EmptyResultDataAccessException e){
			visitHistory = null;
		}catch(Exception ex){
			ex.printStackTrace();
			visitHistory = null;
		}
		return visitHistory; 
	}

}
