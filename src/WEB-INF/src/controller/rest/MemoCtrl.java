package controller.rest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.json.JSONObject;
import org.json.JSONArray;

import model.memo.MemoDAO;
import model.memo.MemoDTO;

import model.designer.DesignerDAO;
import model.designer.DesignerDTO;


import model.customer.CustomerDAO;
import model.customer.CustomerDTO;
import model.push.SendedPushDAO;

import java.io.BufferedReader;

@Controller
public class MemoCtrl{


	@ResponseBody
	@RequestMapping(value="/memo/{customerId}", method=RequestMethod.GET, consumes = {"application/json"}, produces={"application/json"})
	public String readMemo(@PathVariable("customerId") String customerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		MemoDAO mDAO = context.getBean(MemoDAO.class);

		int count = Integer.parseInt(request.getParameter("count"));
		String designerId = request.getParameter("designerId");
		String timestamp = request.getParameter("timestamp");

		List<MemoDTO> memoList = null;
		if(timestamp==null){
			memoList = mDAO.select(customerId, designerId, count);
		}else{
			memoList = mDAO.select(customerId, designerId, timestamp, count);
		}
		JSONObject result = new JSONObject();
		JSONArray memoArr= new JSONArray();
		for(int i=0;i<memoList.size();i++){
			JSONObject obj = new JSONObject();

			JSONObject customer = new JSONObject();
			customer.put("id", memoList.get(i).getCId());
			customer.put("name", memoList.get(i).getCName());
			obj.put("customer", customer);

			obj.put("content", memoList.get(i).getContent());
			obj.put("date", memoList.get(i).getDate());
			obj.put("timestamp", memoList.get(i).getTimestamp());
			obj.put("isHiMemo", memoList.get(i).getIsHiMemo());
			obj.put("proc", memoList.get(i).getProcedureType());
			obj.put("price", memoList.get(i).getPrice());
			obj.put("state", "success");
			memoArr.put(obj);
		}
		result.put("result", memoArr);




		return result.toString();
	}

	@ResponseBody
	@RequestMapping(value="/memo/{designerId}/{customerId}/{timestamp}", method=RequestMethod.DELETE, consumes = {"application/json"}, produces={"application/json"})
	public String deleteMemo(@PathVariable("designerId") String designerId, @PathVariable("customerId") String customerId, @PathVariable("timestamp") String timestamp, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		MemoDAO mDAO = context.getBean(MemoDAO.class);
		JSONObject result = new JSONObject();

		try{
			boolean re = mDAO.delete(designerId, customerId, timestamp);


			if(re){
				result.put("result", "success");
			}else{
				result.put("result", "fail");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){
				ex.printStackTrace();
			}
		}

		return result.toString();
	}

	@ResponseBody
	@RequestMapping(value="/memo/{designerId}/{customerId}/{timestamp}", method=RequestMethod.PUT, consumes = {"application/json"}, produces={"application/json"})
	public String updateMemo(@PathVariable("designerId") String designerId, @PathVariable("customerId") String customerId, @PathVariable("timestamp") String timestamp, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		MemoDAO mDAO = context.getBean(MemoDAO.class);
		JSONObject result = new JSONObject();

		try{
			StringBuilder sb = new StringBuilder();
			BufferedReader br = request.getReader();
			String str;
			while( (str = br.readLine()) != null ){
				sb.append(str);
			}    
			JSONObject jObj = new JSONObject(sb.toString());
			String content = jObj.getString("content");
			int price = jObj.getInt("price");
			String proc = jObj.getString("proc");

			MemoDTO memo = new MemoDTO();
			memo.setCId(customerId);
			memo.setDId(designerId);
			memo.setTimestamp(timestamp);
			memo.setPrice(price);
			memo.setProcedureType(proc);
			memo.setContent(content);

			boolean re = mDAO.update(memo);


			if(re){
				result.put("result", "success");
			}else{
				result.put("result", "fail");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){
				ex.printStackTrace();
			}
		}

		return result.toString();
	}

	@ResponseBody
	@RequestMapping(value="/memo", method=RequestMethod.POST )
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String memoSave(HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		MemoDAO mDAO = context.getBean(MemoDAO.class);
		SendedPushDAO sDAO = context.getBean(SendedPushDAO.class);

		boolean saveSucc=false;
		MemoDTO mDTO = new MemoDTO();
		JSONObject result = new JSONObject();
		try{

			StringBuilder sb = new StringBuilder();
			BufferedReader br = request.getReader();
			String str;
			while( (str = br.readLine()) != null ){
				sb.append(str);
			}    
			JSONObject jObj = new JSONObject(sb.toString());



			mDTO.setContent(jObj.getString("content"));
			mDTO.setCId(jObj.getString("cId"));
			mDTO.setDate(jObj.getString("date"));
			mDTO.setTimestamp(jObj.getString("timestamp"));
			mDTO.setDId(jObj.getString("dId"));
			mDTO.setIsHiMemo(jObj.getBoolean("isHiMemo"));
			mDTO.setPrice(jObj.getInt("price"));
			mDTO.setProcedureType(jObj.getString("proc"));
			if(jObj.has("pushId")){
				sDAO.setComplete(jObj.getString("pushId"));
			}
			saveSucc= mDAO.insert(mDTO);
			result.put("badge", sDAO.getUnfetchedCount(mDTO.getDId()));

			if(saveSucc){
				result.put("result", "success");
			}else{
				result.put("result", "fail");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){

			}
		}
		return result.toString();
	}

	@ResponseBody
	@RequestMapping(value="/allMemo/{designerId}", method=RequestMethod.GET, consumes = {"application/json"}, produces={"application/json"})
	public String readAllMemo(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		MemoDAO mDAO = context.getBean(MemoDAO.class);
		int count = Integer.parseInt(request.getParameter("count"));


		List<MemoDTO> memoList = mDAO.selectAll(designerId, count);
		JSONObject result = new JSONObject();
		JSONArray memoArr= new JSONArray();
		JSONObject cInfo;
		int i=0;
		if(memoList.size() > 0){
			/*
			for(i=memoList.size()-1;i>=0;i--){
				if(i!=memoList.size()-1 && !memoList.get(i).getCId().equals(memoList.get(i+1).getCId())){
					cInfo = new JSONObject();
					cInfo.put("name", memoList.get(i+1).getCName());
					cInfo.put("id", memoList.get(i+1).getCId());
					cInfo.put("memoList", memoArr);
					result.put(memoList.get(i+1).getCId(), cInfo);
					memoArr=new JSONArray();
				}
				JSONObject obj = new JSONObject();

				JSONObject customer = new JSONObject();

				customer.put("id", memoList.get(i).getCId());
				customer.put("name", memoList.get(i).getCName());
				obj.put("customer", customer);

				obj.put("content", memoList.get(i).getContent());
				obj.put("date", memoList.get(i).getDate());
				obj.put("timestamp", memoList.get(i).getTimestamp());
				obj.put("isHiMemo", memoList.get(i).getIsHiMemo());
				obj.put("state", "success");

				obj.put("proc_type", memoList.get(i).getProcedureType());
				obj.put("price", memoList.get(i).getPrice());

				memoArr.put(obj);
			}
			*/
			for(i=0;i<memoList.size();i++){
				if(i!=0 && !memoList.get(i).getCId().equals(memoList.get(i-1).getCId())){
					cInfo = new JSONObject();
					cInfo.put("name", memoList.get(i-1).getCName());
					cInfo.put("id", memoList.get(i-1).getCId());
					cInfo.put("memoList", memoArr);
					result.put(memoList.get(i-1).getCId(), cInfo);
					memoArr=new JSONArray();
				}
				JSONObject obj = new JSONObject();

				JSONObject customer = new JSONObject();

				customer.put("id", memoList.get(i).getCId());
				customer.put("name", memoList.get(i).getCName());
				obj.put("customer", customer);

				obj.put("content", memoList.get(i).getContent());
				obj.put("date", memoList.get(i).getDate());
				obj.put("timestamp", memoList.get(i).getTimestamp());
				obj.put("isHiMemo", memoList.get(i).getIsHiMemo());
				obj.put("state", "success");

				obj.put("proc", memoList.get(i).getProcedureType());
				obj.put("price", memoList.get(i).getPrice());

				memoArr.put(obj);
			}
			cInfo = new JSONObject();
			cInfo.put("name", memoList.get(i-1).getCName());
			cInfo.put("id", memoList.get(i-1).getCId());
			cInfo.put("memoList", memoArr);
			result.put(memoList.get(i-1).getCId(), cInfo);
		}


		JSONObject returnVal = new JSONObject();
		returnVal.put("result", result);



		return returnVal.toString();
	}

}
