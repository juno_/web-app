package controller.rest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.json.JSONObject;
import org.json.JSONArray;



import model.admin.AdminDTO;

import model.designer.DesignerDAO;
import model.designer.DesignerDTO;
import model.push.SendedPushDAO;
import model.push.SendedPushDTO;

import java.io.BufferedReader;

@Controller
@RequestMapping(value="/designer")
public class DesignerCtrl{

	private AdminDTO chkSession(HttpServletRequest request){
		return (AdminDTO)request.getSession().getAttribute("ADMIN");
	}

	@ResponseBody
	@RequestMapping(value="initPush/{designerId}", method=RequestMethod.GET)
	public String getNoCompletePush(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		try{
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	
			SendedPushDAO sDao = context.getBean(SendedPushDAO.class);	
			List<SendedPushDTO> list = sDao.getNotCompletePush(designerId);
			JSONArray pushList = new JSONArray();
			for(int i = 0;i<list.size();i++){
				JSONObject pushObj = new JSONObject();
				pushObj.put("dId", list.get(i).getDId());
				pushObj.put("data", list.get(i).getData());
				pushObj.put("pushId", list.get(i).getId());
				pushList.put(pushObj);
				
			}
			result.put("result", "success");
			result.put("data", pushList);
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}
	@ResponseBody
	@RequestMapping(value="fetchPush/{designerId}", method=RequestMethod.GET)
	public String fetchAllPush(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		try{
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	
			SendedPushDAO sDao = context.getBean(SendedPushDAO.class);	
			List<SendedPushDTO> list = sDao.fetchAllPush(designerId);
			JSONArray pushList = new JSONArray();
			int [] ids = new int[list.size()];
			for(int i = 0;i<list.size();i++){
				JSONObject pushObj = new JSONObject();
				pushObj.put("dId", list.get(i).getDId());
				pushObj.put("data", list.get(i).getData());
				pushObj.put("pushId", list.get(i).getId());
				pushList.put(pushObj);
				ids[i] = list.get(i).getNum();
				
			}
			sDao.setFetch(ids);
			result.put("result", "success");
			result.put("data", pushList);
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}
	@ResponseBody
	@RequestMapping(value="fetchPush/{designerId}/{pushId}", method=RequestMethod.GET)
	public String fetchPush(@PathVariable("designerId") String designerId, @PathVariable("pushId") String pushId, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());

		JSONObject result = new JSONObject();
		try{
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	
			SendedPushDAO sDao = context.getBean(SendedPushDAO.class);	
			SendedPushDTO fetchedPush = sDao.fetchPush(designerId, pushId);
			int [] ids = new int[1];
			if(fetchedPush !=null){
				JSONObject pushObj = new JSONObject();
				pushObj.put("dId", fetchedPush.getDId());
				pushObj.put("data", fetchedPush.getData());
				pushObj.put("pushId", fetchedPush.getId());
				result.put("data", pushObj);
				ids[0] = fetchedPush.getNum();
				sDao.setFetch(ids);
			}
			result.put("result", "success");

		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}




	@ResponseBody
	@RequestMapping(value="gcm/{designerId}", method=RequestMethod.POST)
	public String saveGCM(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());

		JSONObject result = new JSONObject();
		try{
			StringBuilder sb = new StringBuilder();
                        BufferedReader br = request.getReader();
                        String str;
                        while( (str = br.readLine()) != null ){
                                sb.append(str);
                        }    
                        JSONObject jObj = new JSONObject(sb.toString());
			System.out.println(jObj.toString());
			String regId = jObj.getString("regId");
			//String apiKey = jObj.getString("api");

			
			
			//VisitHistoryDAO vDao= context.getBean(VisitHistoryDAO.class);
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	
			//dDao.saveGCM(designerId, regId, apiKey);
			dDao.savePushKey(designerId, regId, "android");
			result.put("result", "success");
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}
	@ResponseBody
	@RequestMapping(value="token/{designerId}", method=RequestMethod.POST)
	public String saveToken(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());

		JSONObject result = new JSONObject();
		try{
			StringBuilder sb = new StringBuilder();
                        BufferedReader br = request.getReader();
                        String str;
                        while( (str = br.readLine()) != null ){
                                sb.append(str);
                        }    
                        JSONObject jObj = new JSONObject(sb.toString());
			String regId = jObj.getString("token");
			//String apiKey = jObj.getString("api");

			
			
			//VisitHistoryDAO vDao= context.getBean(VisitHistoryDAO.class);
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	
			//dDao.saveGCM(designerId, regId, apiKey);
			dDao.savePushKey(designerId, regId, "ios");
			result.put("result", "success");
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}
	@ResponseBody
	@RequestMapping(value="changePassword/{designerId}", method=RequestMethod.PUT)
	public String changePassword(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());

		JSONObject result = new JSONObject();
		try{
			StringBuilder sb = new StringBuilder();
                        BufferedReader br = request.getReader();
                        String str;
                        while( (str = br.readLine()) != null ){
                                sb.append(str);
                        }    
                        JSONObject jObj = new JSONObject(sb.toString());
			String oldPw = jObj.getString("old");
			String newPw = jObj.getString("new");
			
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	


			DesignerDTO dto = dDao.signIn(designerId, oldPw);
			if(dto == null){
				result.put("result", "fail");
				result.put("data", "unauthorized");
			}else{
				boolean re = dDao.changePassword(designerId, newPw);
				if(re){
					result.put("result", "success");
				}else{
					result.put("result", "fail");
				}
			}


		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}
}
