package controller.rest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.json.JSONObject;
import org.json.JSONArray;

import model.memo.MemoDAO;
import model.memo.MemoDTO;

import model.designer.DesignerDAO;
import model.designer.DesignerDTO;


import model.customer.CustomerDAO;
import model.customer.CustomerDTO;

import model.push.SendedPushDAO;


import java.io.BufferedReader;

@Controller
public class CustomerCtrl{

	@ResponseBody
	@RequestMapping(value="/myCustomer/{designerId}", method=RequestMethod.GET)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String myCustomer(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		DesignerDAO dDAO = context.getBean(DesignerDAO.class);


		List<CustomerDTO> myCustomerList = dDAO.selectMyCustomer(designerId);


		JSONObject result = new JSONObject();
		JSONArray customerArr = new JSONArray();
		if(myCustomerList.size() > 0){
			for(int i=0;i<myCustomerList.size();i++){
				JSONObject obj = new JSONObject();
				obj.put("id", myCustomerList.get(i).getId());
				obj.put("name", myCustomerList.get(i).getName());
				obj.put("gender", myCustomerList.get(i).getGender());
				obj.put("lastPhone", myCustomerList.get(i).getLastPhone());
				obj.put("fixedMemo", myCustomerList.get(i).getFixedMemo());
				customerArr.put(obj);
			}
		}
		result.put("result", customerArr);

		return result.toString();

	}

	@ResponseBody
	@RequestMapping(value="/customer/fixedMemo", method=RequestMethod.PUT)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String updateFixedMemo(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		CustomerDAO cDAO = context.getBean(CustomerDAO.class);


		StringBuilder sb = new StringBuilder();
		BufferedReader br = request.getReader();
		String str;
		while( (str = br.readLine()) != null ){
			sb.append(str);
		}    
		JSONObject jObj = new JSONObject(sb.toString());

		String fixedMemo = jObj.getString("fixedMemo");
		String customerId  = jObj.getString("customerId");
		String designerId = jObj.getString("designerId");

		boolean result = cDAO.updateFixedMemo(designerId, customerId, fixedMemo);
		JSONObject reObj = new JSONObject();
		if(result == true){
			reObj.put("result", "success");
		}else {
			reObj.put("result", "fail");
		}
		return reObj.toString();

	}
	@ResponseBody
	@RequestMapping(value="/customer/hideCustomer/{designerId}", method=RequestMethod.PUT)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String hideCustomer(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		DesignerDAO dDao= context.getBean(DesignerDAO.class);
		SendedPushDAO sDao = context.getBean(SendedPushDAO.class);


		StringBuilder sb = new StringBuilder();
		BufferedReader br = request.getReader();
		String str;
		while( (str = br.readLine()) != null ){
			sb.append(str);
		}    
		JSONObject jObj = new JSONObject(sb.toString());

		String customerId  = jObj.getString("customerId");
		String pushId = null;
		if(jObj.has("pushId")){
			pushId = jObj.getString("pushId");
			sDao.setComplete(pushId);
		}

		boolean result = dDao.hideCustomer(designerId, customerId);
		JSONObject reObj = new JSONObject();
		if(result == true){
			reObj.put("result", "success");
		}else {
			reObj.put("result", "fail");
		}
		return reObj.toString();
	}
}
