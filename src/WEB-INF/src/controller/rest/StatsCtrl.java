package controller.rest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import org.json.JSONObject;
import org.json.JSONArray;


import model.stats.StatsDTO;
import model.stats.StatsDAO;
import model.stats.StatsDTO;
import model.stats.StatsDetailDTO;

import java.io.BufferedReader;

@Controller
@RequestMapping(value="/stats")
public class StatsCtrl{
	@ResponseBody
	@RequestMapping(value="/main/{designerId}", method=RequestMethod.GET)
	public String main(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		StatsDAO sDAO = context.getBean(StatsDAO.class);
		JSONObject result = new JSONObject();
		JSONObject data = new JSONObject();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar eDateCal = Calendar.getInstance();
		Calendar sDateCal = Calendar.getInstance();
		String sDate, eDate;

		sDateCal.add(Calendar.DATE, -6);
		sDate = formatter.format(sDateCal.getTime());
		eDate = formatter.format(eDateCal.getTime());


		JSONObject genderObj = new JSONObject();
		JSONObject procObj = new JSONObject();

		List<StatsDTO> genderData = sDAO.getGender(designerId);
		int todayVisit = sDAO.getTodayVisit(designerId);
		List<StatsDTO> busyDayData = sDAO.getBusyDay(designerId, sDate, eDate);
		List<StatsDTO> procData = sDAO.getProceduresNoGender(designerId, sDate, eDate);

		for(int i=0;i<genderData.size();i++){
			genderObj.put(genderData.get(i).getX(), genderData.get(i).getY());
		}

		int busyDay = 0;
		int busyDayVal = 0 ;
		for(int i=0;i<busyDayData.size();i++){
			if(busyDayVal <= Integer.parseInt(busyDayData.get(i).getY())){
				busyDay = Integer.parseInt(busyDayData.get(i).getX());
				busyDayVal = Integer.parseInt(busyDayData.get(i).getY());
			}
		}
		procObj.put("y", procData.get(0).getY());
		procObj.put("x", procData.get(0).getX());


		data.put("gender", genderObj);
		data.put("today", todayVisit);
		data.put("busyDay", busyDay);
		data.put("proc", procObj);




		result.put("result", data);

		return result.toString();

	}
	@ResponseBody
	@RequestMapping(value="/visitWeek/{designerId}", method=RequestMethod.GET)
	public String visitWeek(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		StatsDAO sDAO = context.getBean(StatsDAO.class);
		String sDate = request.getParameter("sDate");
		String eDate = request.getParameter("eDate");


		List<StatsDetailDTO> statistic = sDAO.visitWithTime(designerId, sDate, eDate);


		JSONObject result = new JSONObject();
		JSONArray statsInfo = new JSONArray();
		JSONArray dataX = new JSONArray();
		JSONArray dataY = new JSONArray();

		if(statistic.size() > 0){
			for(int i=0;i<statistic.size();i++){
				if(i>0 && statistic.get(i).getDist().equals(statistic.get(i-1).getDist())){
					dataX.put(statistic.get(i).getX());
					dataY.put(statistic.get(i).getY());
				}else{
					JSONObject obj = new JSONObject();
					dataX = new JSONArray();
					dataY = new JSONArray();

					obj.put("date", statistic.get(i).getDist());
					obj.put("x", dataX);
					obj.put("y", dataY);
					statsInfo.put(obj);

					dataX.put(statistic.get(i).getX());
					dataY.put(statistic.get(i).getY());
				}
			}
		}
		result.put("result", statsInfo);

		return result.toString();

	}
	@ResponseBody
	@RequestMapping(value="/visitMonth/{designerId}", method=RequestMethod.GET)
	public String visitMonth(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		StatsDAO sDAO = context.getBean(StatsDAO.class);
		String yearMonth = request.getParameter("yearMonth");


		List<StatsDetailDTO> statistic = sDAO.visitWithMonth(designerId, yearMonth);

		int year = Integer.parseInt(yearMonth.substring(0, 4));
		int month = Integer.parseInt(yearMonth.substring(5, 7));
		Calendar startCal = Calendar.getInstance();
		Calendar endCal = Calendar.getInstance();

		startCal.set(Calendar.YEAR, year);
		startCal.set(Calendar.MONTH, month-1);
		startCal.set(Calendar.DATE, 1);
		if(month == 12){
			year = year +1;
		}
		endCal.set(Calendar.YEAR, year);
		endCal.set(Calendar.MONTH, startCal.get(Calendar.MONTH)+1);
		endCal.set(Calendar.DATE, 0);
		int weekCount = endCal.get(Calendar.WEEK_OF_YEAR) - startCal.get(Calendar.WEEK_OF_YEAR) + 1;


		JSONObject result = new JSONObject();
		JSONObject statsInfo = new JSONObject();
		JSONObject statsVal = new JSONObject();
		statsInfo.put("date", yearMonth);
		statsInfo.put("weekCount", weekCount);

		if(statistic.size() > 0){
			for(int i=0;i<statistic.size();i++){
				JSONObject item = new JSONObject();
				item.put("week", statistic.get(i).getDist());
				item.put("x", statistic.get(i).getX());
				item.put("y", statistic.get(i).getY());
				statsVal.put(statistic.get(i).getX(), item);
			}
		}
		statsInfo.put("data", statsVal);
		result.put("result", statsInfo);

		return result.toString();

	}
	@ResponseBody
	@RequestMapping(value="/visitYear/{designerId}", method=RequestMethod.GET)
	public String visitYear(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		StatsDAO sDAO = context.getBean(StatsDAO.class);
		String year= request.getParameter("year");


		List<StatsDetailDTO> statistic = sDAO.visitWithYear(designerId, year);

		JSONObject result = new JSONObject();
		JSONObject statsInfo = new JSONObject();
		JSONObject statsVal = new JSONObject();
		statsInfo.put("date", year);

		if(statistic.size() > 0){
			for(int i=0;i<statistic.size();i++){
				JSONObject item = new JSONObject();
				item.put("month", statistic.get(i).getDist());
				item.put("x", statistic.get(i).getX());
				item.put("y", statistic.get(i).getY());
				statsVal.put(statistic.get(i).getDist()+" "+statistic.get(i).getX(), item);
			}
		}
		statsInfo.put("data", statsVal);
		result.put("result", statsInfo);

		return result.toString();

	}

	@ResponseBody
	@RequestMapping(value="/totalCustomer/{designerId}", method=RequestMethod.GET)
	public String totalCustomer(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		StatsDAO sDAO = context.getBean(StatsDAO.class);


		List<StatsDTO> gender= sDAO.getGender(designerId);
		List<StatsDetailDTO> age= sDAO.getAge(designerId);

		JSONObject result = new JSONObject();
		JSONObject genderObj = new JSONObject();
		JSONObject ageObj = new JSONObject();
		JSONObject statsVal = new JSONObject();
		for(int i=0;i<gender.size();i++){
			genderObj.put(gender.get(i).getX(), gender.get(i).getY());
		}
		for(int i=0;i<age.size();i++){
			ageObj.put(age.get(i).getDist()+" "+age.get(i).getX(), age.get(i).getY());
		}
		statsVal.put("gender", genderObj);
		statsVal.put("age", ageObj);

		result.put("result", statsVal);

		return result.toString();

	}

	@ResponseBody
	@RequestMapping(value="/busyDay/{designerId}", method=RequestMethod.GET)
	public String busyDay(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		StatsDAO sDAO = context.getBean(StatsDAO.class);
		String sDate = request.getParameter("sDate");
		String eDate = request.getParameter("eDate");
		JSONObject result = new JSONObject();
		JSONObject data = new JSONObject();
		List<StatsDTO> busyDay = sDAO.getBusyDay(designerId, sDate, eDate);

		for(int i=0;i<busyDay.size();i++){
			data.put(busyDay.get(i).getX(), busyDay.get(i).getY());
		}

		result.put("result", data);

		return result.toString();

	}
	@ResponseBody
	@RequestMapping(value="/procStats/{designerId}", method=RequestMethod.GET)
	public String procStats(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		StatsDAO sDAO = context.getBean(StatsDAO.class);
		String sDate = request.getParameter("sDate");
		String eDate = request.getParameter("eDate");
		JSONObject result = new JSONObject();
		JSONObject data = new JSONObject();
		List<StatsDetailDTO> procVal = sDAO.getProcedures(designerId, sDate, eDate);

		for(int i=0;i<procVal.size();i++){
			data.put(procVal.get(i).getX()+" "+procVal.get(i).getDist(), procVal.get(i).getY());
		}

		result.put("result", data);

		return result.toString();

	}
}
