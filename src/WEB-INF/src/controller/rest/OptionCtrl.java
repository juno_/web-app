package controller.rest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.json.JSONObject;
import org.json.JSONArray;

import java.io.BufferedReader;

import model.procedure.ProcDAO;
import model.procedure.ProcDTO;

@Controller
@RequestMapping(value="/option")
public class OptionCtrl{

	@ResponseBody
	@RequestMapping(value="/proc/{branch}", method=RequestMethod.GET)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String getProc(@PathVariable("branch") String branch, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		ProcDAO pDAO = context.getBean(ProcDAO.class);

		List<ProcDTO> options= pDAO.selectAll(branch);


		JSONObject result = new JSONObject();
		JSONObject opInfo = new JSONObject();
		if(options.size() > 0){
			for(int i=0;i<options.size();i++){
				JSONObject opObj = new JSONObject();
				opObj.put("num", options.get(i).getNum());
				opObj.put("name", options.get(i).getName());
				opInfo.put(options.get(i).getNum()+"", opObj);
			}
		}
		result.put("result", opInfo);

		return result.toString();

	}
}
