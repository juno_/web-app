package controller.pcrest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.json.JSONObject;
import org.json.JSONArray;



import model.admin.AdminDTO;

import model.designer.DesignerDAO;
import model.designer.DesignerDTO;

import model.customer.CustomerDTO;
import model.customer.CustomerDAO;
import model.memo.MemoDTO;
import model.memo.MemoDAO;

import model.visithistory.VisitHistoryDAO;
import model.visithistory.VisitHistoryDTO;

import java.io.BufferedReader;
import java.util.Calendar;

import juno.GCMAPI;
import com.linkit.juno.JunoAPNS;

import model.PushData;

@Controller
@RequestMapping(value="/designer")
public class DesignerCtrl{

	private AdminDTO chkSession(HttpServletRequest request){
		return (AdminDTO)request.getSession().getAttribute("ADMIN");
	}

	@ResponseBody
	@RequestMapping(value="push/{designerId}", method=RequestMethod.POST)
	public String sendPush(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		AdminDTO session = chkSession(request);


		try{
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}
			
                        StringBuilder sb = new StringBuilder();
                        BufferedReader br = request.getReader();
                        String str;
                        while( (str = br.readLine()) != null ){
                                sb.append(str);
                        }   
			//System.out.println(sb.toString());
                        JSONObject jObj = new JSONObject(sb.toString());

			
			//VisitHistoryDAO vDao= context.getBean(VisitHistoryDAO.class);
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	
			CustomerDAO cDao = context.getBean(CustomerDAO.class);	
			MemoDAO mDAO = context.getBean(MemoDAO.class);
			VisitHistoryDAO vDao = context.getBean(VisitHistoryDAO.class);
			VisitHistoryDTO vDto = new VisitHistoryDTO();
			DesignerDTO designer = dDao.select(designerId);
			boolean isMapped=false;
			boolean isHideCustomer = false;


			System.out.println(jObj.toString());
			System.out.println(designerId);
			String customerId = ((JSONObject)jObj.get("customer")).getString("id");
			vDto.setCId(((JSONObject)jObj.get("customer")).getString("id"));
			vDto.setBranch(session.getBranch());
			vDto.setDId(designerId);

			isMapped = cDao.isMapped(designerId, customerId);
			if(isMapped){
				isHideCustomer = cDao.isHideCustomer(designerId, customerId); 
				if(isHideCustomer == true){
					cDao.updateCDMapShow(designerId, customerId, true);
				}
				vDao.insert(vDto);
			}else{
				cDao.insertCDMap(designerId, customerId);
				vDao.insert(vDto);
			}
			if(designer != null && designer.getPushKey() != null){
					
				int count=10;


				List<MemoDTO> memoList = mDAO.select(customerId, designerId, count);
				JSONObject memoData = new JSONObject();
				JSONArray memoArr= new JSONArray();
				JSONObject cInfo ;
				int i=0;
				if(memoList.size() > 0 && isHideCustomer == true){
					System.out.println(memoList.size());
					for(i=0;i<memoList.size();i++){
						JSONObject obj = new JSONObject();

						JSONObject customer = new JSONObject();
						customer.put("id", memoList.get(i).getCId());
						customer.put("name", memoList.get(i).getCName());
						obj.put("customer", customer);

						obj.put("content", memoList.get(i).getContent());
						obj.put("date", memoList.get(i).getDate());
						obj.put("timestamp", memoList.get(i).getTimestamp());
						obj.put("isHiMemo", memoList.get(i).getIsHiMemo());
						obj.put("price", memoList.get(i).getPrice());
						obj.put("proc", memoList.get(i).getProcedureType());
						obj.put("state", "success");
						memoArr.put(obj);
					}   
					cInfo = new JSONObject();
					cInfo.put("name", memoList.get(i-1).getCName());
					cInfo.put("id", memoList.get(i-1).getCId());
					
					memoData.put("data", memoArr);
					memoData.put("customer", cInfo);
				}else{
					memoData = null;
				}

				
				if(designer.getPhoneDist().equals("android")){
					GCMAPI push = new GCMAPI();
					push.pusher(PushData.API_KEY, designer.getPushKey(), PushData.visitAlarm((JSONObject)jObj.get("customer"), designerId, memoData, context));
				}else if(designer.getPhoneDist().equals("ios")){
					JunoAPNS pusher = new JunoAPNS(PushData.IOS_KEY_PATH_AD, "#jun0hair", PushData.IOS_KEY_PATH_DEV, "#jun0hair");
					pusher.push(designer.getPushKey(), PushData.visitAlarm((JSONObject)jObj.get("customer"), designerId, memoData, context));

				}
				result.put("result", "success");
			}else{
				result.put("result", "fail");
			}

		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result=new JSONObject();
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}
        @ResponseBody
        @RequestMapping(value="isExist/{designerId}", method=RequestMethod.GET)
        public String isExist(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response)
        {   
                ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
                JSONObject result = new JSONObject();
                try{
                        AdminDTO session = chkSession(request);
                        if(session == null){
                                result.put("result", "error");
                                result.put("data", "sessionOut");
                                return result.toString();
                        }   
    
    
                        DesignerDAO dDao = context.getBean(DesignerDAO.class);
                        boolean isExist = dDao.isExist(designerId);
                        if(isExist){
                                result.put("result", "success");
                                result.put("data", "exist");
                        }else{
                                result.put("result", "success");
                                result.put("data", "notExist");
                        }   
                }catch(Exception ex){
                        ex.printStackTrace();
                        try{
                                result = new JSONObject();
                                result.put("result", "fail");
                        }catch(Exception e){ 
                        }   
                }   

                return result.toString();

        }




	@ResponseBody
	@RequestMapping(value="", method=RequestMethod.POST)
	public String addDesigner(HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		AdminDTO session = chkSession(request);


		try{
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}

                        StringBuilder sb = new StringBuilder();
                        BufferedReader br = request.getReader();
                        String str;
                        while( (str = br.readLine()) != null ){
                                sb.append(str);
                        }   
                        JSONObject jObj = new JSONObject(sb.toString());
			
			
			//VisitHistoryDAO vDao= context.getBean(VisitHistoryDAO.class);
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	
			DesignerDTO designer = new DesignerDTO();;

			designer.setId(jObj.getString("id"));
			designer.setName(jObj.getString("name"));
			designer.setPassword(jObj.getString("pw"));
			designer.setBranch(session.getBranch());

			boolean succ = dDao.insert(designer);

			JSONObject dObj = new JSONObject();
			if(succ){
				result.put("result", "success");
			}else{
				result.put("result", "fail");
			}

		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result=new JSONObject();
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}

	@ResponseBody
	@RequestMapping(value="{designerId}", method=RequestMethod.GET)
	public String getDesigner(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		AdminDTO session = chkSession(request);


		try{
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}
			
			
			//VisitHistoryDAO vDao= context.getBean(VisitHistoryDAO.class);
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	
			DesignerDTO designer = dDao.select(designerId);

			JSONObject dObj = new JSONObject();
			if(designer == null){
				result.put("result", "success");
				result.put("data", "empty");
			}else{
				dObj.put("id", designer.getId());
				dObj.put("name", designer.getName());
				dObj.put("branch", designer.getBranch());

				result.put("result", "success");
				result.put("data", dObj);
			}


		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result=new JSONObject();
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}

	/*
	@ResponseBody
	@RequestMapping(value="myCustomer/{designerId}", method=RequestMethod.GET)
	public String myCustomer(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		AdminDTO session = chkSession(request);


		try{
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}
			
			
			//VisitHistoryDAO vDao= context.getBean(VisitHistoryDAO.class);
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	
			List<CustomerDTO> cList = dDao.selectMyCustomer(designerId);

			JSONArray cListObj = new JSONArray();
			for(int i=0;i<cList.size();i++){
				JSONObject dObj = new JSONObject();
				dObj.put("id", cList.get(i).getId());
				dObj.put("name", cList.get(i).getName());
				cListObj.put(dObj);
			}
			result.put("result", "success");
			result.put("data", cListObj);
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}
	*/
	@ResponseBody
	@RequestMapping(value="list", method=RequestMethod.GET)
	public String getList(HttpServletRequest request, HttpServletResponse response) 
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		AdminDTO session = chkSession(request);


		try{
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}
			
			
			//VisitHistoryDAO vDao= context.getBean(VisitHistoryDAO.class);
			DesignerDAO dDao = context.getBean(DesignerDAO.class);	
			List<DesignerDTO> dList = dDao.selectList(session.getBranch());

			JSONArray dListObj = new JSONArray();
			for(int i=0;i<dList.size();i++){
				JSONObject dObj = new JSONObject();
				dObj.put("id", dList.get(i).getId());
				dObj.put("name", dList.get(i).getName());
				dObj.put("branch", dList.get(i).getBranch());
				dListObj.put(dObj);
			}
			result.put("result", "success");
			result.put("data", dListObj);
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result.put("result", "fail");
			}catch(Exception e){
				e.printStackTrace();

			}
		}
		return result.toString();
	}
}
