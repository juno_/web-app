package controller.pcrest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.dao.DuplicateKeyException;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.json.JSONObject;
import org.json.JSONArray;


import model.visithistory.VisitHistoryDAO;
import model.visithistory.VisitHistoryDTO;

import model.admin.AdminDTO;
import model.designer.DesignerDTO;
import model.designer.DesignerDAO;

import model.customer.CustomerDAO;
import model.customer.CustomerDTO;

import java.io.BufferedReader;
import juno.GCMAPI;
import com.linkit.juno.JunoAPNS;
import model.PushData;

@Controller
@RequestMapping(value="/customer")
public class CustomerCtrl{

	private AdminDTO chkSession(HttpServletRequest request){
		return (AdminDTO)request.getSession().getAttribute("ADMIN");
	}

	@ResponseBody
	@RequestMapping(value="", method=RequestMethod.POST)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String insert(HttpServletRequest request, HttpServletResponse response)
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		try{
			AdminDTO session = chkSession(request);
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}

			StringBuilder sb = new StringBuilder();
			BufferedReader br = request.getReader();
			String str;
			while( (str = br.readLine()) != null ){
				sb.append(str);
			}
			JSONObject jObj = new JSONObject(sb.toString());

			
			
			CustomerDAO cDao = context.getBean(CustomerDAO.class);
			VisitHistoryDAO vDao = context.getBean(VisitHistoryDAO.class);
			DesignerDAO dDao = context.getBean(DesignerDAO.class);

			CustomerDTO customer = new CustomerDTO();
			customer.setId(jObj.getString("id"));
			customer.setName(jObj.getString("name"));
			customer.setBranch(session.getBranch());
			customer.setLastPhone(jObj.getString("lastPhone"));
			customer.setGender(jObj.getString("gender"));
			customer.setBirth(jObj.getString("birth"));

			boolean isSucc=cDao.insert(customer);
			if(jObj.has("designerId") == true){
				VisitHistoryDTO vh = new VisitHistoryDTO();
				DesignerDTO designer = dDao.select(jObj.getString("designerId"));
				
				vh.setCId(jObj.getString("id"));
				vh.setBranch(session.getBranch());
				vh.setDId(jObj.getString("designerId"));

				cDao.insertCDMap(jObj.getString("designerId"), customer.getId());
				vDao.insert(vh);

				JSONObject customerObj = new JSONObject();
				customerObj.put("id", customer.getId());
				customerObj.put("name", customer.getName());
				if(designer.getPhoneDist().equals("android")){
					GCMAPI push = new GCMAPI();
					push.pusher(PushData.API_KEY, designer.getPushKey(), PushData.visitAlarm(customerObj, designer.getId(), null, context));
				}else if(designer.getPhoneDist().equals("ios")){
					JunoAPNS pusher = new JunoAPNS(PushData.IOS_KEY_PATH_AD, "#jun0hair", PushData.IOS_KEY_PATH_DEV, "#jun0hair");
					pusher.push(designer.getPushKey(), PushData.visitAlarm(customerObj, designer.getId(), null, context));

				}

			}

			if(isSucc){
				result.put("result", "success");
			}else{
				result.put("result", "fail");
			}
			
			
		}catch(DuplicateKeyException ex){
			try{
				result = new JSONObject();
				result.put("result", "fail");
				result.put("data", "exist");
			}catch(Exception e){
			}
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result = new JSONObject();
				result.put("result", "fail");
			}catch(Exception e){
			}
		}

		return result.toString();

	}

	@ResponseBody
	@RequestMapping(value="{customerId}", method=RequestMethod.PUT)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String modify(@PathVariable("customerId") String customerId, HttpServletRequest request, HttpServletResponse response)
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		try{
			AdminDTO session = chkSession(request);
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}

			StringBuilder sb = new StringBuilder();
			BufferedReader br = request.getReader();
			String str;
			while( (str = br.readLine()) != null ){
				sb.append(str);
			}
			JSONObject jObj = new JSONObject(sb.toString());

			
			
			CustomerDAO cDao = context.getBean(CustomerDAO.class);
			CustomerDTO customer = new CustomerDTO();
			customer.setId(jObj.getString("id"));
			customer.setName(jObj.getString("name"));
			customer.setLastPhone(jObj.getString("lastPhone"));
			customer.setGender(jObj.getString("gender"));
			customer.setBirth(jObj.getString("birth"));

			boolean isSucc = cDao.update(customer);
			if(isSucc){

				List<DesignerDTO> dList = cDao.selectMyDesigner(customer.getId());
				for(int i=0;i<dList.size();i++){
					if(dList.get(i).getPhoneDist().equals("android")){
						GCMAPI push = new GCMAPI();
						push.pusher(PushData.API_KEY, dList.get(i).getPushKey(), PushData.changeCustomerInfo(jObj, dList.get(i).getId(), context)); 
					}else if(dList.get(i).getPhoneDist().equals("ios")){
						JunoAPNS pusher = new JunoAPNS(PushData.IOS_KEY_PATH_AD, "#jun0hair",PushData.IOS_KEY_PATH_DEV, "#jun0hair");
						pusher.push(dList.get(i).getPushKey(), PushData.changeCustomerInfo(jObj, dList.get(i).getId(), context));

					}
				}



				
				result.put("result", "success");
			}else{
				result.put("result", "fail");
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result = new JSONObject();
				result.put("result", "fail");
			}catch(Exception e){
			}
		}

		return result.toString();

	}

	@ResponseBody
	@RequestMapping(value="isExist/{customerId}", method=RequestMethod.GET)
	public String isExist(@PathVariable("customerId") String customerId, HttpServletRequest request, HttpServletResponse response)
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		try{
			AdminDTO session = chkSession(request);
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}
			
			
			CustomerDAO cDao = context.getBean(CustomerDAO.class);
			boolean isExist = cDao.isExist(customerId);
			if(isExist){
				result.put("result", "success");
				result.put("data", "exist");
			}else{
				result.put("result", "success");
				result.put("data", "notExist");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result = new JSONObject();
				result.put("result", "fail");
			}catch(Exception e){
			}
		}

		return result.toString();

	}
	@ResponseBody
	@RequestMapping(value="total", method=RequestMethod.GET)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String total(HttpServletRequest request, HttpServletResponse response)
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		try{
			AdminDTO session = chkSession(request);
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}
			
			
			CustomerDAO cDao = context.getBean(CustomerDAO.class);
			result.put("result", "success");
			result.put("data", cDao.getTotal(session.getBranch()));
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result = new JSONObject();
				result.put("result", "fail");
			}catch(Exception e){
			}
		}

		return result.toString();

	}
	@ResponseBody
	@RequestMapping(value="searchKey/{keyword}", method=RequestMethod.GET)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String searchKey(@PathVariable("keyword") String keyword, HttpServletRequest request, HttpServletResponse response)
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		try{
			AdminDTO session = chkSession(request);
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}
			
			
			CustomerDAO cDao = context.getBean(CustomerDAO.class);
			List<CustomerDTO> customer = cDao.selectByKeyword(keyword);
			
			JSONArray customerList = new JSONArray();
			for (int i=0;i<customer.size();i++){
				JSONObject customerObj = new JSONObject();
				customerObj.put("id", customer.get(i).getId());
				customerObj.put("name", customer.get(i).getName());
				customerObj.put("branch", customer.get(i).getBranch());
				customerObj.put("lastPhone", customer.get(i).getLastPhone());
				customerObj.put("gender", customer.get(i).getGender());
				customerObj.put("birth", customer.get(i).getBirth());
				customerList.put(customerObj);
			}
			result.put("result", "success");
			result.put("data", customerList);


		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result = new JSONObject();
				result.put("result", "fail");
			}catch(Exception e){
			}
		}

		return result.toString();

	}

	@ResponseBody
	@RequestMapping(value="{customerId}", method=RequestMethod.GET)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String getInfo(@PathVariable("customerId") String customerId, HttpServletRequest request, HttpServletResponse response)
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();
		try{
			AdminDTO session = chkSession(request);
			if(session == null){
				result.put("result", "error");
				result.put("data", "sessionOut");
				return result.toString();
			}
			
			
			CustomerDAO cDao = context.getBean(CustomerDAO.class);
			CustomerDTO customer = cDao.select(customerId);
			
			JSONObject customerObj = new JSONObject();
			if(customer!=null){
				customerObj.put("id", customer.getId());
				customerObj.put("name", customer.getName());
				customerObj.put("gender", customer.getGender());
				customerObj.put("lastPhone", customer.getLastPhone());
				customerObj.put("birth", customer.getBirth());
				result.put("result", "success");
				result.put("data",customerObj); 
			}else{
				result.put("result", "success");
				result.put("data", "empty"); 
			}
		}catch(Exception ex){
			ex.printStackTrace();
			try{
				result = new JSONObject();
				result.put("result", "fail");
			}catch(Exception e){
			}
		}

		return result.toString();

	}
	@ResponseBody
	@RequestMapping(value="visitInfo/{customerId}", method=RequestMethod.GET)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String visitInfo(@PathVariable("customerId") String customerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		JSONObject result = new JSONObject();

		AdminDTO session = chkSession(request);
		if(session == null){
			result.put("result", "error");
			result.put("data", "sessionOut");
			return result.toString();
		}
		
		
		VisitHistoryDAO vDao= context.getBean(VisitHistoryDAO.class);
		CustomerDAO cDao = context.getBean(CustomerDAO.class);
		VisitHistoryDTO dto = vDao.getLastVisit(customerId, session.getBranch());


		if(dto!=null){	// n 번째 방문 n>1. 
			JSONObject visitObj = new JSONObject();
			JSONObject customerObj = new JSONObject();
			JSONObject designerObj = new JSONObject();
			customerObj.put("id", dto.getCId());
			customerObj.put("name", dto.getCName());
			customerObj.put("visitDate", dto.getVisitDate());

			designerObj.put("id", dto.getDId());
			designerObj.put("name", dto.getDName());


			visitObj.put("customer", customerObj);
			visitObj.put("designer", designerObj);

			result.put("result", "success");
			result.put("data", visitObj);
		}else{	//첫방문
			VisitHistoryDTO firstVisit = cDao.selectFirstVisit(customerId, session.getBranch());
			if(firstVisit == null){
				result.put("result", "success");
				result.put("data", "empty");
			}else{
				/*
				JSONObject dataObj = new JSONObject();
				JSONObject customerObj = new JSONObject();

				result.put("result", "success");
				customerObj.put("id", customer.getId());
				customerObj.put("name", customer.getName());
				dataObj.put("customer", customerObj);
				result.put("data", dataObj);
				*/
				
				JSONObject visitObj = new JSONObject();
				JSONObject customerObj = new JSONObject();
				JSONObject designerObj = new JSONObject();
				customerObj.put("id", firstVisit.getCId());
				customerObj.put("name", firstVisit.getCName());
				visitObj.put("customer", customerObj);


				if(firstVisit.getDId()!= null){
					designerObj.put("id", firstVisit.getDId());
					designerObj.put("name", firstVisit.getDName());
					visitObj.put("designer", designerObj);
				}

				result.put("result", "success");
				result.put("data", visitObj);
			}
		}


		return result.toString();

	}
	@ResponseBody
	@RequestMapping(value="byDesigner/{designerId}", method=RequestMethod.GET)
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String getByDesigner(@PathVariable("designerId") String designerId, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());

		JSONObject result = new JSONObject();
		AdminDTO session = chkSession(request);
		if(session == null){
			result.put("result", "error");
			result.put("data", "sessionOut");
			return result.toString();
		}
		
		
		DesignerDAO dDao = context.getBean(DesignerDAO.class);
		List<CustomerDTO> cList = dDao.selectMyCustomer(designerId);

                JSONArray customerArr = new JSONArray();
                if(cList.size() > 0){ 
                        for(int i=0;i<cList.size();i++){
                                JSONObject obj = new JSONObject();
                                obj.put("id", cList.get(i).getId());
                                obj.put("name", cList.get(i).getName());
                                obj.put("gender", cList.get(i).getGender());
                                obj.put("lastPhone", cList.get(i).getLastPhone());
                                customerArr.put(obj);
                        }   
                }   
                result.put("result", "success");
                result.put("data", customerArr);
	




		return result.toString();

	}
}
