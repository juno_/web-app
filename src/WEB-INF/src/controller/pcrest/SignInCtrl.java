package controller.pcrest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.json.JSONObject;
import org.json.JSONArray;

import model.memo.MemoDAO;
import model.memo.MemoDTO;

import model.designer.DesignerDAO;
import model.designer.DesignerDTO;

import model.admin.AdminDAO;
import model.admin.AdminDTO;

import model.customer.CustomerDAO;
import model.customer.CustomerDTO;

import java.io.BufferedReader;

@Controller
public class SignInCtrl{
	@ResponseBody
	@RequestMapping(value="/adminSignIn", method=RequestMethod.POST )
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String adminSignIn(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		AdminDAO dDAO = context.getBean(AdminDAO.class);


		StringBuilder sb = new StringBuilder();
		BufferedReader br = request.getReader();
		String str;
		while( (str = br.readLine()) != null ){
			sb.append(str);
		}    
		JSONObject jObj = new JSONObject(sb.toString());



		AdminDTO designer = dDAO.signIn(jObj.getString("id"), jObj.getString("pw"));

		if(designer != null){
			request.getSession().setMaxInactiveInterval(-1);
			request.getSession().setAttribute("ADMIN", designer);
			JSONObject result = new JSONObject();
			JSONObject dObj = new JSONObject();
			dObj.put("id", designer.getId());
			dObj.put("branch", designer.getBranch());
			dObj.put("name", designer.getName());
			dObj.put("branchName", designer.getBranchName());
			result.put("result", "success");
			result.put("designer", dObj);

			return result.toString();
		}else{
			return "{\"result\":\"fail\"}";
		}

	}

	@ResponseBody
	@RequestMapping(value="/adminSignOut", method=RequestMethod.POST )
	//public String memoSave(@RequestBody MemoDTO pdto) throws Exception
	public String adminSignOut(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		AdminDAO dDAO = context.getBean(AdminDAO.class);

		request.getSession().invalidate();

		JSONObject result = new JSONObject();
		result.put("result", "success");

		return result.toString();


	}
}
