package controller.common;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@Controller
@RequestMapping("/admin")
public class MainController {


	@RequestMapping("")
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
		ModelAndView mav = new ModelAndView();
		if(request.getSession().getAttribute("ADMIN") == null){
			mav.setViewName("index");
		}else{
			mav.setViewName("redirect:/admin/main");
		}

		return mav;
	}
	@RequestMapping("main")
	public ModelAndView main(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ModelAndView mav = new ModelAndView();
		if(request.getSession().getAttribute("ADMIN") == null){
			mav.setViewName("redirect:/admin");
		}else{
			mav.setViewName("main");
		}

		return mav;
	}
	@RequestMapping("signInPage")
	public ModelAndView reSignIn(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ModelAndView mav = new ModelAndView();
		if(request.getSession().getAttribute("ADMIN") == null){
			mav.setViewName("signInPage");
		}else{
			mav.setViewName("redirect:/admin/main");
		}

		return mav;
	}
}
