

javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/controller/common/MainController.java

javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/controller/rest/SignInCtrl.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/controller/rest/CustomerCtrl.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/controller/rest/MemoCtrl.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/controller/rest/DesignerCtrl.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/controller/rest/StatsCtrl.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/controller/rest/OptionCtrl.java


javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/controller/pcrest/CustomerCtrl.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/controller/pcrest/SignInCtrl.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/controller/pcrest/DesignerCtrl.java

javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/memo/MemoDAO.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/memo/MemoDTO.java

javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/designer/DesignerDAO.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/designer/DesignerDTO.java

javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/customer/CustomerDTO.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/customer/CustomerDAO.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/customer/CDMapDTO.java


javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/admin/AdminDAO.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/admin/AdminDTO.java

javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/visithistory/VisitHistoryDTO.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/visithistory/VisitHistoryDAO.java

javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/stats/StatsDTO.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/stats/StatsDetailDTO.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/stats/StatsDAO.java


javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/procedure/ProcDAO.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/procedure/ProcDTO.java

javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/push/SendedPushDAO.java
javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/push/SendedPushDTO.java



javac -Xlint -d ./classes -sourcepath ./src -cp "./lib/*" ./src/model/PushData.java
