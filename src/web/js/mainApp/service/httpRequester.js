mainApp.factory("httpRequester", function($http, $location){
	var SERVER_ADDR = "http://k2u4yt.vps.phps.kr/MI/";

	function request(param){
		var opt = {
			url:param.url,
			method:param.method || "GET",
			data:param.data || {},
			timeout:param.timeout || 6000,
			success:param.success|| undefined,
			error:param.error|| undefined
		}
		var headerObj = {};
		if(opt.method == "GET"){
			headerObj = {
				"Content-Type":  "application/x-www-form-urlencoded;charset=UTF-8",
				"Accept":  "application/json;charset=UTF-8"
			};
			if(opt.data!=undefined){
				opt.url=opt.url+"?"	
				for(var property in opt.data){
					opt.url+=property+"="+opt.data[property]+"&";	
				}
				opt.data={};
			}
		}else{
			headerObj = {
				"Content-Type":  "application/json;charset=UTF-8",
				"Accept":  "application/json;charset=UTF-8"
			};
			opt.data = JSON.stringify(opt.data);
		}
		return $http({
			method : opt.method,
			url : SERVER_ADDR+opt.url,
			headers: headerObj,
			timeout:opt.timeout,
			responseType:"json",
			data :opt.data

		}).success(function(data){
			if(typeof data === "string"){
				data= JSON.parse(JSON.parse(data));
			}
			console.log(data);
			if(data.result == "error"){
				if(data.data == "sessionOut"){
					///go to session out page
					console.log("session out");
					window.location.href=window.ROOT;
					return;
				}
			}

			if(opt.method == "GET"){
				if(opt.success != undefined){
					opt.success(data);
				}
			}else{
				if(data.result === "fail"){
					if(opt.error!= undefined){
						opt.error(data);
					}

				}else if(data.result === "success"){
					if(opt.success != undefined){
						opt.success(data);
					}
				}
			}
			

		}).error(function(data, code){
			if(typeof data === "string"){
				data= JSON.parse(JSON.parse(data));
			}
			if(opt.error != undefined){
				opt.error({result:"fail", data:data});
			}
		});

			
	};


	return{
		request:request
		
	};
});
