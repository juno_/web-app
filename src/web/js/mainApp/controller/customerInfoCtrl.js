mainApp.controller("regCustomerCtrl", function($scope, $location, $stateParams, httpRequester, $modal, ngToast){
	$scope.customer={};
	//$scope.onlyNumbers=/^d+$/;
	$scope.onlyNumbers=/^[0-9]*$/;
	$scope.init=function(){
		
		if($scope.onlyNumbers.test($stateParams.customerId)){
			$scope.customer.id = $stateParams.customerId;
		}else{
			$scope.customer.name = $stateParams.customerId;
		}
		$scope.customer.gender="M";
		$scope.exist=-1;
		$scope.customerIdComment="고객번호를 입력하세요.";
	};

	$scope.openDesignerModal = function(){
		var modalInstance = $modal.open({
			templateUrl : JS_ROOT+"/js/template/modal/designerList.html",
			size:'lg',
			controller:"designerListModalCtrl",
			resolve:{
				btnText : function(){return "담당지정"; }
			}
		});
		modalInstance.result.then(function(designer){
				//close
				$scope.selectedDesigner=designer;
				console.log($scope.selectedDesigner);

		},
		function(result){
				//dismiss

		});
	};
	$scope.chkId= function(form){
		console.log($scope.customer.id);
		httpRequester.request({
			url:"/customer/isExist/"+$scope.customer.id,
			method:"GET",
			success:function(data){
				if($scope.customer.id){
					console.log(data);
					if(data.data=="exist"){
						$scope.exist=1;
						form.customerId.$pristine=false;
						form.customerId.$invalid=true;
						$scope.customerIdComment="이미 존재하는 고객번호입니다.";
					}else if(data.data=="notExist"){
						$scope.exist=0;
						form.customerId.$pristine=true;
						form.customerId.$invalid=false;
						$scope.customerIdComment="고객번호를 입력하세요.";
						
					}
				}else{
					$scope.exist=-1;
					form.customerId.$pristine=false;
					form.customerId.$invalid=true;
					$scope.customerIdComment="고객번호를 입력하세요.";
				}
			},
			error:function(data){
				console.log(data);
				$scope.exist=-1;
				form.customerId.$pristine=true;
				form.customerId.$invalid=false;
				$scope.customerIdComment="고객번호를 입력하세요.";
			}
		});
	}
	$scope.register = function(form){
		if(form.$invalid ){
			if(!$scope.customer.name){
				form.customerName.$pristine=false;
			}
			if(!$scope.customer.id || $scope.exist==1){
				form.customerId.$pristine=false;
			}
			if(!$scope.customer.birth){
				form.birth.$pristine=false;
			}
			if(!$scope.customer.lastPhone){
				form.lastPhone.$pristine=false;
			}
			return;
		}

		var data = {};
		angular.copy($scope.customer, data);
		if($scope.selectedDesigner){
			data.designerId = $scope.selectedDesigner.id;
		}
		httpRequester.request({
			url:"/customer",
			method:"POST", 
			data:data,
			success:function(data){
				ngToast.create({
					content:"<small><b>"+$scope.customer.name+"</b> 고객님이 등록 되었습니다.</small>"
				});
				$scope.customer={};
				$scope.customer.gender="M";
				$scope.selectedDesigner=undefined;
				form.customerName.$pristine=true;
				form.customerId.$pristine=true;
				form.lastPhone.$pristine=true;
				form.birth.$pristine=true;
				$scope.exist=-1;
			},
			error:function(data){
				$scope.customer={};
				$scope.selectedDesigner=undefined;
				form.customerName.$pristine=true;
				form.customerId.$pristine=true;
				form.lastPhone.$pristine=true;
				form.birth.$pristine=true;
				$scope.exist=-1;
				if(data.data == "exist"){
					ngToast.create({
						content:"<small>이미 등록된 고객번호 입니다.</small>",
						class:"danger"
					});
				}else{
					ngToast.create({
						content:"<small>고객등록에 실패했습니다.</small>",
						class:"danger"
					});
				}

			}
			
		});
		console.log("register customer");
	};
	
	$scope.init();
});
mainApp.controller("viewCustomerCtrl", function($state, $scope, $location, $stateParams, httpRequester, $modal){

	$scope.init=function(){
		httpRequester.request({
			url:"/customer/total",
			method:"GET",
			success:function(data){
				$scope.totalComment="총 "+data.data+"명의 고객분들이 등록되어있습니다.";
			},
			error:function(data){
				$scope.totalComment="총 고객 인원을 읽어오는데 실패했습니다.";
			}
		});

		if($stateParams.customerId){
			$scope.customerId = $stateParams.customerId;	
			$scope.getCustomerViewInfo($stateParams.customerId);

		}
	};
	$scope.searchCustomer = function(form){
		if(!$scope.searchKey){
			form.searchKey.$pristine=false;
			return;
		}
		httpRequester.request({
			url :"/customer/searchKey/"+$scope.searchKey,
			method:"GET",
			success:function(data){
				if(data.data.length > 1){
					$scope.showSelectModal(data.data, form);
				}else if(data.data.length == 1){
					$scope.getCustomerViewInfo(data.data[0].id);
				}else{
					$scope.customerInfo=undefined; 
					$scope.designerInfo=undefined; 
					$scope.showNoCustomer=true;
				}
			},
			error:function(data){
				console.log(data);
			}
		});

	};
	$scope.showSelectModal = function(list){
		var modalInstance = $modal.open({
			templateUrl:JS_ROOT+"/js/template/modal/selCustomerModal.html",
			resolve : {
				list : function(){return list;}
			},
			controller:"selCustomerModalCtrl"
		});
		modalInstance.result.then(function(result){
			$scope.getCustomerViewInfo(result.id);
			$scope.searchKey = result.id;

		}, function(result){
			$scope.searchKey ="";
			$scope.viewForm.searchKey.$pristine=true;
		});
	};
	$scope.getCustomerViewInfo = function(customerId){
		/*
		if(!$scope.searchKey){
			form.searchKey.$pristine=false;
			return;
		}
		*/


		httpRequester.request({
			url:"/customer/visitInfo/"+customerId,
			method : "GET",
			success:function(data){
				if(data.data.designer){
					$scope.customerInfo= data.data.customer;
					$scope.designerInfo= data.data.designer;
					$scope.showNoCustomer=false;
				}else if(data.data == "empty" ){
					$scope.customerInfo=undefined; 
					$scope.designerInfo=undefined; 
					$scope.showNoCustomer=true;
				
				}else{
					$scope.customerInfo= data.data.customer;
					$scope.designerInfo=undefined; 
					$scope.showNoCustomer=false;
				}
			},
			error:function(data){
				console.log(data);
			}
		});
		
		
	};
	$scope.openDesignerModal = function(){
		var modalInstance = $modal.open({
			templateUrl : JS_ROOT +"/js/template/modal/designerList.html",
			size:"lg",
			controller:"designerListModalCtrl",
			resolve:{
				btnText:function(){return "알림전송";}
			}
		});
		modalInstance.result.then(function(designer){
			httpRequester.request({
				url:"/designer/push/"+designer.id,
				method:"POST",
				data:{
					customer:{
						id : $scope.customerInfo.id,
						name : $scope.customerInfo.name
					}
				},
				success:function(data){
					$state.go($state.current, {}, {reload:true});
				},
				error:function(data){
					console.log(data);
				}

			});
		},
		function(result){
		});
	};
	$scope.sendPush = function(){
		httpRequester.request({
			url:"/designer/push/"+$scope.designerInfo.id,
			method:"POST",
			data:{
				customer:{
					id : $scope.customerInfo.id,
					name : $scope.customerInfo.name
				}
			},
			success:function(data){
				$state.go($state.current, {}, {reload:true});
			},
			error:function(data){
				console.log(data);
			}
		});

	};

	$scope.init();
});
mainApp.controller("modifyCustomerCtrl", function($scope, $location, $stateParams, httpRequester, ngToast){


	$scope.customer={};
	$scope.isExistCustomer = -1;
	$scope.getCustomerInfo = function(form){
		if(!$scope.customer.id){
			form.customerId.$pristine=false;
			return;
		}


		httpRequester.request({
			method:"GET",
			url:"/customer/"+$scope.customer.id,
			success:function(data){
				console.log(data);
				if(data.data == "empty"){
					$scope.isExistCustomer=0;
				}else{
					$scope.isExistCustomer=1;
					$scope.customer=data.data;
				}
			},
			error:function(data){
				console.log(data);
			}
		});

	};

	$scope.modify = function(form){

		if(form.$invalid ){
			if(!$scope.customer.name){
				form.customerName.$pristine=false;
			}
			if(!$scope.customer.birth){
				form.birth.$pristine=false;
			}
			if(!$scope.customer.lastPhone){
				form.lastPhone.$pristine=false;
			}
			return;
		}
		
		httpRequester.request({
			method:"PUT",
			url:"/customer/"+$scope.customer.id,
			data:$scope.customer,
			success:function(data){
				ngToast.create({
					content:"<small>회원정보가 변경되었습니다.</small>",
				});
				//$scope.customer={};	
				$scope.isExistCustomer = -1;
				
			},
			error:function(data){
				ngToast.create({
					content:"<small>회원정보 변경에 실패했습니다.</small>",
					class:"danger"

				});
				console.log(data);
			}
		});
		
	};
	




});


mainApp.controller("viewByDesignerCtrl", function($scope, $location, $stateParams, httpRequester, $modal, ngToast){
	$scope.init=function(){
		$scope.customerList = undefined;
		$scope.noDataMsg = "디자이너를 선택하세요.";
		httpRequester.request({
			url:"/designer/list",
			method:"GET",
			success: function(data){
				console.log(data);
				$scope.designerList = data.data;
			},
			error: function(data){
			}

		});

	};
	$scope.selDesigner = function(designer){
		$scope.selDe = designer;
		$scope.search="";
		httpRequester.request({
			url:"/customer/byDesigner/"+designer.id,
			method:"GET",
			success: function(data){
				$scope.customerList = data.data;
				if($scope.customerList.length == 0){
					$scope.noDataMsg = "등록된 담당 고객이 없습니다.";
				}
			},
			error: function(data){
			}

		});
		

	};
	$scope.mouseIn= function(){
		this.hover = true;
	};
	$scope.mouseOut= function(){
		this.hover = false;
	};
	$scope.sendPush = function(customer){
		httpRequester.request({
			url:"/designer/push/"+$scope.selDe.id,
			method:"POST",
			data:{
				customer:{
					id : customer.id,
					name : customer.name
				}
			},
			success:function(data){
				var str = ""
				ngToast.create({
					content:"<small><b>"+$scope.selDe.name+"</b> 디자이너님에게<b>"+customer.name+"</b> 고객님 방문 알림이 전송되었습니다.</small>"
				});
			},
			error:function(data){
				ngToast.create({
					content:"<small>알림 전송에 실패했습니다.</small>"
				});
			}
		});
	};
	$scope.init();
});
