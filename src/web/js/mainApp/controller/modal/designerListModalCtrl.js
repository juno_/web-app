mainApp.controller("designerListModalCtrl", function($scope, $modalInstance, httpRequester, btnText){
	$scope.designerList=[];
	$scope.init = function(){
		$scope.btnText = btnText;
		httpRequester.request({
			url:"/designer/list",
			method:"GET",
			success:function(data){
				$scope.designerList = data.data;
			},
			error:function(data){
			}
		});

	};
	$scope.clickDesigner = function(designer){
		$modalInstance.close(designer);
	};
	$scope.close  = function(){
		$modalInstance.dismiss('cancel');
	};
	$scope.init();
});
