mainApp.controller("delDesignerModalCtrl", function($scope, $modalInstance, httpRequester, designer){
	$scope.init = function(){
		$scope.designer = designer;
		$scope.byebyeImg = JS_ROOT+"/img/byebye.png";

	};
	$scope.del= function(){
		$modalInstance.close(designer);
	};
	$scope.close  = function(){
		$modalInstance.dismiss('cancel');
	};
	$scope.init();
});
