mainApp.controller("selCustomerModalCtrl", function($scope, $modalInstance, httpRequester, list){
	

	$scope.init = function(){
		$scope.customerList = list;

	};
	$scope.selCustomer= function(customer){
		$modalInstance.close(customer);
	};
	$scope.close  = function(){
		$modalInstance.dismiss('cancel');
	};
	$scope.init();
});
