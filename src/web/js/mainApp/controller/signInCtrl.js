mainApp.controller("signInCtrl", function($scope, httpRequester){
	$scope.signIn = function(form){
		if(form.$invalid){
			if(form.adminId.$invalid){
				form.adminId.$pristine=false;
			}
			if(form.adminPw.$invalid){
				form.adminPw.$pristine=false;
			}
			return;
		}


		httpRequester.request({
			method:"POST",
			url:"/adminSignIn",
			data:{
				id:$scope.adminId,
				pw:$scope.adminPw
			},
			success : function(data){
			console.log(data);
				if(data.result=="success"){
					location.href=ROOT+"/main";
				}
			},
			error : function(data){
				if(data.result == "fail"){
					console.log("login fail");
				}
			}
		});

	}
});
