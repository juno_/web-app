mainApp.controller("homeCtrl", function($scope, httpRequester, $modal, $state){
	$scope.init=function(){
		console.log("homeCtrl");
	};

	$scope.searchCustomer=function(form){
		if(form.$invalid){
			form.searchKey.$pristine=false;
			return;
		}
		httpRequester.request({
			url:"/customer/searchKey/"+$scope.searchKey,
			method:"GET", 
			success:function(data){
			console.log(data);
				if(data.data.length > 1){
					$scope.showSelectModal(data.data);
					
				}else{
					if(data.data.length == 0){
						$state.go("customerSearch",{customerId:$scope.searchKey});
					}else{
						$state.go("customerSearch",{customerId:data.data[0].id});
					}
				}
				
			},
			error:function(data){
				console.log(data);
			}
		});


	};
	$scope.showSelectModal = function(list){
		var modalInstance = $modal.open({
			templateUrl :JS_ROOT+"/js/template/modal/selCustomerModal.html",
			resolve : {
				list: function(){return list;}
			},
			controller: "selCustomerModalCtrl"
		});
		modalInstance.result.then(function(result){
				//close
				$state.go("customerSearch",{customerId:result.id});

		}, 
		function(result){
				$scope.searchKey="";
				$scope.searchForm.searchKey.$pristine=true;
				//dismiss
		});
	};


	$scope.init();
});


