mainApp.controller("customerSearchCtrl", function($scope, $stateParams, httpRequester, $modal, $state, ngToast){
	$scope.customerId="";
	$scope.init=function(){
		console.log("customerSearchCtrl");

		$scope.customerId = $stateParams.customerId;
		httpRequester.request({
			url:"/customer/visitInfo/"+$stateParams.customerId,
			method:"GET",
			success : function(data){
				console.log(data);

				if(data.data === "empty"){	//no member
					$scope.showNotRegister = true;
				}else if(data.data.customer.visitDate){		//n-th visit
					$scope.showCustomerVisit = true;
					$scope.customer= data.data.customer;
					$scope.designer= data.data.designer;
				}else if(!data.data.customer.visitDate && data.data.designer){	//mapped, first visit
					$scope.customer= data.data.customer;
					$scope.designer= data.data.designer;
					$scope.showMappedDesigner= true;
					$scope.showFirstVisit = true;

				}else{		//not mapped, first visit
					$scope.showFirstVisit = true;
					$scope.customer = data.data.customer;
				}


			}
		});
	};
	$scope.regCustomer = function(){
		$state.go("regCustomer",{customerId:$scope.customerId});
	}
	$scope.openDesignerModal = function(){
		var modalInstance = $modal.open({
			templateUrl : JS_ROOT+"/js/template/modal/designerList.html",
			size:'lg',
			resolve:{
				btnText : function(){return "알림전송";}
			},
			controller:"designerListModalCtrl"
		});
		modalInstance.result.then(function(result){
				//close
			httpRequester.request({
				url:"/designer/push/"+result.id,
				method:"POST",
				data:{
					customer:{
						id:$scope.customer.id,	
						name:$scope.customer.name
					}
				},
				success : function(data){
					console.log(data);
					$state.go("/");
					

				},
				error : function(data){
					console.log(data);
				}
			});

		},
		function(result){
				//dismiss

		});
	};

	$scope.sendPush = function(){
		$scope.sendBtnDis = "true";
		httpRequester.request({
			url:"/designer/push/"+$scope.designer.id,
			method:"POST",
			data:{
				customer:{
					id:$scope.customer.id,	
					name:$scope.customer.name
				}
			},
			success : function(data){
				console.log(data);
				$scope.sendBtnDis = undefined;
				ngToast.create({
					content : "<small>방문알림이 전송되었습니다.</small>"
				});

				$state.go("/");

			},
			error : function(data){
				$scope.sendBtnDis = undefined;
				console.log(data);
			}
		});

	};
	$scope.init();
});


