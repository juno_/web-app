mainApp.controller("viewDesignerCtrl", function($scope, httpRequester, $modal){
	$scope.init = function(){
		httpRequester.request({
			url:"/designer/list",
			method:"GET",
			success:function(data){
				$scope.designerList = data.data;
			},
			error:function(data){
				console.log(data);
			}
		});
	};
	$scope.delDesigner = function(designer){
		var modalInstance = $modal.open({
			templateUrl:JS_ROOT+"/js/template/modal/delDesignerModal.html",
			resolve :{
				designer : function(){return designer;}
			},
			controller:"delDesignerModalCtrl"
		});

	};

	$scope.init();
});


mainApp.controller("regDesignerCtrl", function($scope, httpRequester, $modal, ngToast){
	$scope.init = function(){
		$scope.confirmPw=false;
		$scope.designer={};
		$scope.designerIdComment="이메일 형식으로 입력하세요."; 
		//$scope.designerIdComment="아이디를 입력하세요.";
	};
	$scope.chkPw = function(chk){
		$scope.confirmPw = chk;
		if(chk == true){
			$("#designerPw").get(0).type="text";
		}else{
			$("#designerPw").get(0).type="password";
		}
	};

        $scope.chkId= function(form){
		console.log("Asdfasdf");
                httpRequester.request({
                        url:"/designer/isExist/"+$scope.designer.id,
                        method:"GET",
                        success:function(data){
				if(data.data=="exist"){
					$scope.exist=1;
					form.designerId.$pristine=false;
					form.designerId.$invalid=true;
					$scope.designerIdComment="이미 존재하는 아이디입니다.";
				}else if(data.data=="notExist"){
					$scope.exist=0;
					form.designerId.$pristine=true;
					form.designerId.$invalid=false;
					$scope.designerIdComment="이메일 형식으로 입력하세요."; 

				}   
                        },  

                        error:function(data){
                                console.log(data);
                                $scope.exist=-1;
				/*
                                $scope.exist=-1;
                                form.designerId.$pristine=true;
                                form.designerId.$invalid=false;
				$scope.designerIdComment="이메일 형식으로 입력하세요."; 
				*/
                        }
                });
        }

	$scope.register = function(form){
		if(form.$invalid){
			console.log($scope.designer.id);
			if(!$scope.designer.id){
				form.designerId.$pristine = false;
			}
			if(!$scope.designer.name){
				form.designerName.$pristine = false;
			}
			if(!$scope.designer.pw){
				form.designerPw.$pristine = false;
			}
			return;
		}


		httpRequester.request({
			url:"/designer",
			method:"POST",
			data:$scope.designer,
			success:function(data){
				ngToast.create({
					content:"<small><b>"+$scope.designer.name+"</b> 디자이너님이 등록되었습니다.</small>"
				});
				$scope.designer={};
				form.designerId.$pristine=true;
				form.designerName.$pristine=true;
				form.designerPw.$pristine=true;
			},
			error:function(data){
				ngToast.create({
					content:"<small>디자이너등록에 실패했습니다.</small>",
					class:"danger"

				});
				$scope.designer={};
				form.designerId.$pristine=true;
				form.designerName.$pristine=true;
				form.designerPw.$pristine=true;
			}


		});

	};
	$scope.init();
});
mainApp.controller("regExistDesignerCtrl", function($scope, httpRequester, $modal, ngToast){
	$scope.init = function(){
		$scope.designer={};
	};
	$scope.searchDesigner = function(form){
		if(!$scope.designer.id){
			form.designerId.$pristine=false;
			return;
		}
	};

	$scope.init();
});
mainApp.controller("delDesignerCtrl", function($scope, httpRequester, $modal){
	$scope.init = function(){
	};
	$scope.init();
});











