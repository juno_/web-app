mainApp.controller("mainCtrl", function($scope,httpRequester, $state){
	$scope.signOut = function(){
		httpRequester.request({
			url : "/adminSignOut",
			method:"POST",
			success:function(data){
				
				location.href=ROOT+"";
			},
			error:function(){
				console.log("signOut error");
			}
		});
	};

});
