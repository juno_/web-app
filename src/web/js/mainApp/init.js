mainApp.config(function($stateProvider, $urlRouterProvider, $httpProvider, ngToastProvider){

	$urlRouterProvider.otherwise("/");



	$stateProvider.state("/", {
		url:"/",
		templateUrl:JS_ROOT+"/js/template/home.html",
		controller:"homeCtrl"

	}).state("customerSearch", {
		url:"/customerSearch/:customerId",
		templateUrl:JS_ROOT+"/js/template/customerSearch.html",
		controller:"customerSearchCtrl"
		
	}).state("regCustomer", {
		url:"/regCustomer/:customerId",
		templateUrl:JS_ROOT+"/js/template/customer/regCustomer.html",
		controller:"regCustomerCtrl",
	}).state("modifyCustomer", {
		url:"/modifyCustomer",
		templateUrl:JS_ROOT+"/js/template/customer/modifyCustomer.html",
		controller:"modifyCustomerCtrl"
	}).state("viewCustomer", {
		url:"/viewCustomer/:customerId",
		templateUrl:JS_ROOT+"/js/template/customer/viewCustomer.html",
		controller:"viewCustomerCtrl",
	}).state("viewByDesigner", {
		url:"/viewByDesigner",
		templateUrl:JS_ROOT+"/js/template/customer/viewByDesigner.html",
		controller:"viewByDesignerCtrl",
	}).state("viewDesigner", {
		url:"/viewDesigner",
		templateUrl:JS_ROOT+"/js/template/designer/viewDesigner.html",
		controller:"viewDesignerCtrl"
	}).state("regDesigner", {
		url:"/regDesigner",
		templateUrl:JS_ROOT+"/js/template/designer/regDesigner.html",
		controller:"regDesignerCtrl"
	}).state("regExistDesigner", {
		url:"/regExistDesigner",
		templateUrl:JS_ROOT+"/js/template/designer/regExistDesigner.html",
		controller:"regExistDesignerCtrl"
	}).state("delDesigner", {
		url:"/delDesigner",
		templateUrl:JS_ROOT+"/js/template/designer/delDesigner.html",
		controller:"delDesignerCtrl"
		/*
	}).state("/detailDesigner/:designerId", {
		templateUrl:JS_ROOT+"/js/template/designer/detailDesigner.html",
		controller:"detailDesignerCtrl"
		*/
	}).state("contact", {
		url:"/contact",
		templateUrl:JS_ROOT+"/js/template/contact.html"
	});

	/*
	ngToastProvider.configure({
		verticalPosition:"bottom"
	});
	*/
});
