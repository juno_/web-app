mainApp.directive("designerMenu", function(){
	return{
		restrict:"EA",
		templateUrl:JS_ROOT+"/js/template/designer/menu.html",
		controller:"designerMenuCtrl"
	};
});

mainApp.controller("designerMenuCtrl", function($scope, $attrs, $state){
	$scope.activeIdx = $attrs.menuIdx;
	$scope.viewDesigner= function(){
		console.log("viewDesigner");
		$state.go("viewDesigner");
	};  
	$scope.regDesigner = function(){
		console.log("regDesigner");
		$state.go("regDesigner");
	};  
	$scope.regExistDesigner = function(){
		console.log("regExistDesigner");
		$state.go("regExistDesigner");
	};  

});

