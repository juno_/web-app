mainApp.directive("customerMenu", function(){
	return{
		restrict:"EA",
		templateUrl:JS_ROOT+"/js/template/customer/menu.html",
		controller:"customerMenuCtrl"
	};
});

mainApp.controller("customerMenuCtrl", function($scope, $attrs, $state){
	$scope.activeIdx = $attrs.menuIdx;

});

