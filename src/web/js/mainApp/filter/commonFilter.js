mainApp.filter("dateTerm", function(){
	return function(input){
		var year = 0,
		    month = 0,
		    date = 0,
		    lastDate,
		    today, 
		    diffTime, periodDate, result;


		year = input.substring(0, 4);
		month = input.substring(5, 7);
		date = input.substring(8, 10);
		lastDate = new Date(year, parseInt(month)-1, date);
		today = new Date();
		diffTime = today.getTime() - lastDate.getTime();

		periodDate = Math.floor(diffTime/(1000 * 60 * 60 * 24));



		if(Math.floor(periodDate / 365) > 0){
			result = Math.round(periodDate / 365);
			result +="년 전";

		}else if(Math.floor(periodDate / 56) > 0){
			result = Math.round(periodDate / 30);
			result +="개월 전";

		}else if(Math.floor(periodDate / 7) > 0){
			result = Math.round(periodDate / 7);
			result +="주 전";

		}else{
			result =periodDate+"일 전";
		}
		return result;
	};
}).filter("dateString", function(){
	return function(input){
		var year = 0,
		    month = 0,
		    date = 0,
		    lastDate,
		    result, day;

		var week = new Array("일", "월", "화", "수", "목", "금", "토");


		try{
			year = input.substring(0, 4);
			month = input.substring(5, 7);
			date = input.substring(8, 10);
			lastDate = new Date(year, parseInt(month)-1, date);
			day = week[lastDate.getDay()];
			result = year+"년 "+month+"월 "+date+"일 ("+day+")";
		}catch(e){
			return input;
		}

		return result;
	};
});


mainApp.filter("fullDate", function(dateStringFilter, dateTermFilter){
	return function(input){
		var today = new Date(),
		    returnStr;

		if(!input){
			return input;
		}

		year = input.substring(0, 4);
		month = input.substring(5, 7);
		date = input.substring(8, 10);
		hour = input.substring(11, 13);
		min = input.substring(14, 16);

		lastDate = new Date(year, parseInt(month)-1, date);
		if(today.toDateString() == lastDate.toDateString()){
			
			hour=parseInt(hour);
			if(hour >= 12){
				returnStr = "오후";
				if(hour > 12){
					hour=hour-12;
				}
			}else{
				returnStr = "오전";
			}
			returnStr += " "+hour+" : "+min;

			return returnStr;

		}else{
			return dateStringFilter(input)+" ... "+dateTermFilter(input);

		}


	};

});


mainApp.filter("trusted", function($sce){
	return function(text){
		return $sce.trustAsHtml(text);
	};
});
