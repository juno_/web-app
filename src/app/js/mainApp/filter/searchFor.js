mainApp.filter("searchFor", function(){
	return function(arr, searchKey){
		if(!searchKey){
			return arr; 
		}
		var result = [];
		angular.forEach(arr, function(item){
			if(item.name.indexOf(searchKey)>-1){
				result.push(item);
			}
		});
		return result;
	};
});
