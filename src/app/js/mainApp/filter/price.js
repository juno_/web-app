mainApp.filter("price", function(){
	return function(val){
		var result 
		if(isNaN(val)){
			return val;
		}
		
		result = (val+"").replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");


		return result;
	};
});
