mainApp.factory("httpRequester", function($http, resender, $ionicLoading){
	var SERVER_ADDR = "http://k2u4yt.vps.phps.kr/MI";

	function request(param){
		var opt = {
			url:param.url,
			method:param.method || "GET",
			data:param.data || {},
			timeout:param.timeout || 6000,
			success:param.success|| undefined,
			error:param.error|| undefined,
			resend : param.resend || false,
			loading: param.loading || true
		}
		/*
		if(param.loading == false){
			opt.loading = param.loading;
		}else{
			opt.loading = true;
		}
		*/

		var headerObj = {};
		if(opt.method == "GET"){
			headerObj = {
				"Content-Type":  "application/x-www-form-urlencoded;charset=UTF-8",
				"Accept":  "application/json;charset=UTF-8"
			};
			if(opt.data!=undefined){
				opt.url=opt.url+"?"	
				for(var property in opt.data){
					opt.url+=property+"="+opt.data[property]+"&";	
				}
				opt.data={};
			}
		}else{
			headerObj = {
				"Content-Type":  "application/json;charset=UTF-8",
				"Accept":  "application/json;charset=UTF-8"
			};
			opt.data = JSON.stringify(opt.data);
		}
		if(opt.loading == true){
			$ionicLoading.show({
				showBackdrop:true,
				showDelay:0
			});
		}
		return $http({
			method : opt.method,
			url : SERVER_ADDR+opt.url,
			headers: headerObj,
			timeout:opt.timeout,
			responseType:"json",
			data :opt.data

		}).success(function(data, status){
			if(status!=200){
				$ionicLoading.hide();
				return;
			}
			if(typeof data ==="string"){
				data = JSON.parse(data);
			}

			if(opt.method == "GET"){
				if(opt.success != undefined){
					opt.success(data);
				}
			}else{
				if(data.result === "fail" ){
					//resend here if resend is true
					if( opt.resend == true){
						resender.register(opt);
					}
					if(opt.error!= undefined){
						opt.error(data);
					}

				}else if(data.result === "success"){
					if(opt.success != undefined){
						opt.success(data);
					}
				}
			}
			$ionicLoading.hide();
			

		}).error(function(data, code){
			if(code == 404){
				$ionicLoading.hide();
				return;
			}else if (code == 500){
				console.log(data);
				$ionicLoading.hide();
				return;
			}
			if(typeof data === "string"){
				data = JSON.parse(data);
			}
			if(opt.resend == true){
				resender.register(opt);
			}
			if(opt.error != undefined){
				opt.error({result:"fail", data:data});
			}
			$ionicLoading.hide();
		});

			
	};


	return{
		request:request
		
	};
});
