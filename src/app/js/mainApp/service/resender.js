mainApp.factory("resender", function($timeout, $http, storageManager, $q, memoCache){

	var SERVER_ADDR = "http://k2u4yt.vps.phps.kr/MI";
	var key="resend ",
	    resendPeriod = 10000,
	    resendCount = 3;
	    resendTimer=undefined,
	    isResendSucc=false;

	function keyGen(){
		return key + new Date().getTime();
	}

	function startResend(term){
		
		if(term == undefined){
			term = resendPeriod;
		}
		resendTimer = $timeout(sendWorker, term);
		//sendWorker();
	}

	function httpSender(data){
		var headerObj = {};
		var opt = data.value;
		var key = data.key;
		var dataObj = {};

                if(opt.method == "GET"){
                        headerObj = {
                                "Content-Type":  "application/x-www-form-urlencoded;charset=UTF-8",
                                "Accept":  "application/json;charset=UTF-8"
                        };
                        if(opt.data!=undefined){
                                opt.url=opt.url+"?"
                                for(var property in opt.data){
                                        opt.url+=property+"="+opt.data[property]+"&";
                                }
                                opt.data={};
                        }
                }else{
                        headerObj = {
                                "Content-Type":  "application/json;charset=UTF-8",
                                "Accept":  "application/json;charset=UTF-8"
                        };
			
			while(typeof opt.data == "string"){
				opt.data = JSON.parse(opt.data);
			}
                }

                return $http({
                        method : opt.method,
                        url : SERVER_ADDR+opt.url,
                        headers: headerObj,
                        timeout:opt.timeout,
                        responseType:"json",
                        data :opt.data

                }).success(function(data){
                        console.log("resend success");
                        if(typeof data ==="string"){
                                data = JSON.parse(data);
                        }

                        if(data.result === "fail"){
                                //resend here if resend is true
				isResendSucc=false;
                        }else if(data.result === "success"){
				isResendSucc=true;
				storageManager.remove(key);
				if(opt.data.content && opt.data.cId && opt.data.timestamp && opt.method == "POST" ){	//memo resend
					memoResended(opt.data.cId, opt.data.timestamp);
				}
				if(opt.success != undefined){
					opt.success(data);
				}
			}
                }).error(function(data){
			//for test////
			console.log("resend fail");
			isResendSucc = true;
			/////////////

                });
	}
	function memoResended(customerId, timestamp){
		var idx= memoCache.idx[customerId];
		var list,i;
		if(idx == undefined){
			return;
		}
		list = memoCache.list[idx];

		var len = list.data.length;
		i=0;
		while(i<len){
			if(list.data[i].timestamp == timestamp){
				list.data[i].state="success";
				break;
			}
			i++;
		}
		storageManager.save("memoRoom "+ customerId, list);
	};
	function sendWorker(){
		console.log("resender::sendWorker start");
		var savedResendData = storageManager.loadAllWithKey(key);
		var i=0;
		function orderedSend(idx){
			var q = httpSender(savedResendData[idx]);
			var orderSendPromise = q.then(function onSccess(data){
				console.log("resend promise succ");
				idx++;	
				if(isResendSucc && savedResendData.length > idx){
					orderedSend(idx);
				}else{
					console.log(isResendSucc);
				}
			}, function onError(data){
				console.log("resend promise fail");

				/*
				idx++;	
				if(isResendSucc && savedResendData.length > idx){
					orderedSend(idx);
				}else{
					console.log(isResendSucc);
				}
				*/
			});
			return orderSendPromise;
		};
		if(savedResendData.length == 0){
			console.log("no resend data");
		}else{
			orderedSend(i);
		}
		resendTimer=undefined;
	};


	var facObj={};

	facObj.register = function(opt){
		console.log("resender register");
		storageManager.save(keyGen(), opt);
		/*
		if(resendTimer === undefined){
			startResend();
		}
		*/
	};
	facObj.resend = function(term){
		console.log(resendTimer);
		if(resendTimer === undefined){
			startResend(term);
		}
	};

	
	return facObj;
	
	
});
