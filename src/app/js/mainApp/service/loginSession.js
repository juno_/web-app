mainApp.provider("loginSession", function(storageManagerProvider){
	var session={};
	
	this.chkAlreadySignIn = function(){

		session = storageManagerProvider.load("session");
		if(session==undefined){
			session={};
			return false;
		}else{
			return true;
		}
	};
	this.$get=function(storageManager){
		return {
			setSession:function (designerInfo){
				angular.copy(designerInfo, session);
				storageManager.save("session", session);
				
			},
			getSession:function(){
				return session;
			},
			signOut:function(){
				window.localStorage.clear();
			}
		}

	}
	
});
/*
mainApp.factory("loginSession", function(storageManager){
	var session={};

	var facObj={};
	
	facObj.setSession=function (designerInfo){
		angular.copy(designerInfo, session);
		storageManager.save("session", session);
		
	};
	facObj.getSession=function (){
		return session;
	};
	facObj.signOut=function (){
		window.localStorage.clear();
	};
	facObj.chkAlreadySignIn=function (){
		session = storageManager.load("session");
		if(session==undefined){
			session={};
			return false;
		}else{
			return true;
		}
	};
	
	return facObj;	
});

*/
