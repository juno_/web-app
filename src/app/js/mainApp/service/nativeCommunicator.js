mainApp.provider("nativeCall", function(){

	var phoneDist = "";
	var ANDROID = "adnroid";
	var IOS = "ios";

	return {
		setPhoneDist : function(){
			var userAgent = window.navigator.userAgent.toLowerCase();
			var ios = /iphone|ipod|ipad/.test( userAgent );
			var android = /android/.test( userAgent );
			if(ios){
				phoneDist = IOS;
			}else if(android){
				phoneDist = ANDROID;
			}
		},
		$get :function(){
			return {
				showToast : function(str){
					try{
						window.native.showToast(str);
					}catch(e){
						console.error(e);
					}
				},
				getSetReady : function(){
					try{
						window.native.GetSetReady();
					}catch(e){
						console.error(e);
					}
				},
				removeNoti: function(){
					try{
						window.native.removeNoti();
					}catch(e){
						console.error(e);
					}
				},
				initDesigner: function(id){
					try{
						window.native.initDesigner(id);
					}catch(e){
						console.error(e);
					}
				},
				exitApp: function(){
					try{
						window.native.exitApp();
					}catch(e){
						console.error(e);
					}
				}
			};
		}
	}

});
