mainApp.value("memoCache", {});
mainApp.factory("memoStorage", function(storageManager, httpRequester, loginSession, $q, $rootScope, memoCache, scrollGoTop, nativeCall){
	
	//storage key : <designerId customerId time>
	var key = "memoRoom ";
	var maxSaveCount = 10;
	var facObj={};

	//var cacheData = undefined;
	var visitCount = new Number(0);

	//call just once in application life time
	function getLastMemo(){
		var result = storageManager.loadAll(key);
		result.sort(function(a, b){
		//	if(b.isVisit == a.isVisit){
			if(b.isVisit != undefined &&  a.isVisit != undefined){ 
				return b.isVisit < a.isVisit ? -1 : 1;
			}else if(b.isVisit == undefined &&  a.isVisit == undefined){
				if(b.data.length == 0){
					return 1;
				}else if(a.data.length == 0){
					return -1;
				}else{
					//return b.data[b.data.length-1].timestamp - a.data[a.data.length-1].timestamp;
					return b.data[0].timestamp - a.data[0].timestamp;
				}
			}else{
				if(a.isVisit){
					return -1;
				}else if(b.isVisit){
					return 1
				}
			}
		});
		return result;	
	};

	function saveComplete (memo, isSuccess){
		var savedMemo = {},
		    len=0, i=0;
		//savedMemo = storageManager.load(key+memo.customer.id) || {isVisit:false, data:[]};
		savedMemo = facObj.loadCustomer(memo.customer);
		if(isSuccess){
			memo.state="success";
		}

		len = savedMemo.data.length;
		for(i =len-1 ;i>=0;i--){
			if(savedMemo.data[i].state == "saving"){
				break;
			}
		}
		if(i > 0 ){	//some memo transfer not complete;
			if(i >= maxSaveCount-1){
				savedMemo.data.slice(i+1, savedMemo.data.length);
			}else{
				savedMemo.data.slice(maxSaveCount-1, savedMemo.data.length);
			}
		}else{		//all memo transfer complete
			savedMemo.data.splice(maxSaveCount-1, savedMemo.data.length);
		}
		savedMemo.data.unshift(memo);
		savedMemo.isVisit=undefined;
		savedMemo.pushId=undefined;
		savedMemo.customer={};
		savedMemo.customer.id=memo.customer.id;
		savedMemo.customer.name=memo.customer.name;;
		storageManager.save(key+memo.customer.id, savedMemo);



		insertWritten(memo.customer.id);
		$rootScope.$broadcast("$scrollToTop");
		scrollGoTop.state = true;
	};
	function insertWritten(customerId){
		var index =  memoCache.idx[customerId];
		var elem = memoCache.list[index];
		var insertIdx =0;
		while(insertIdx < memoCache.list.length){
			if(memoCache.list[insertIdx].isVisit ||
				insertIdx == index ||
				memoCache.list[insertIdx].data[0].date > elem.data[0].date){
				
				insertIdx++;
			}else{
				break;
			}
		}

		if(index <= insertIdx){
			insertIdx--;
		}

		memoCache.list.splice(index, 1);
		memoCache.list.splice(insertIdx, 0, elem);
		reorderIdx();
	};
	facObj.initMemoInVisit = function(customer){
		var initData = {}
		initData.data=[];
		initData.customer = {
			id:customer.id,
			name:customer.name
		};
		storageManager.save(key+customer.id, initData);

		memoCache.list.push(initData);
		reorderIdx();
		return memoCache.list[memoCache.idx[customer.id]];
	};

	facObj.save = function (memo){
		var url = "";
		var data = {};
		data.cId = memo.customer.id;
		data.dId = loginSession.getSession().id;
		data.content = memo.content;
		data.date = memo.date;
		data.isHiMemo = memo.isHiMemo;
		data.timestamp = memo.timestamp;
		data.price = memo.price;
		data.proc = memo.proc;
		data.pushId = memo.pushId;

		var q = httpRequester.request({
			url:"/memo", 
			method:"POST", 
			resend:true,
			data:data, 
			success:function(response){
				saveComplete(memo, true);
			},
			error:function(data){
				saveComplete(memo, false);
			}
		});
		return q;
	};
	facObj.delete = function(customer, idx){
		var memoList = facObj.loadCustomer(customer);
		var lastMemo;
		var delMemo = memoList.data[idx];
		var q ;
		memoList.data.splice(idx, 1);
		if(memoList.data.length >= maxSaveCount){
			var saveData = {
				data : memoList.data.slice(0, maxSaveCount),
				customer : memoList.customer,
				isVisit : memoList.isVisit
			}
			storageManager.save(key+customer.id, saveData);
			q=httpRequester.request({
				url:"/memo/"+loginSession.getSession().id+"/"+customer.id+"/"+delMemo.timestamp, 
				method:"DELETE", 
				resend:true
			});
		}else if(memoList.data.length > 0 && memoList.data.length < maxSaveCount){

			q=httpRequester.request({
				url:"/memo/"+loginSession.getSession().id+"/"+customer.id+"/"+delMemo.timestamp, 
				method:"DELETE", 
				resend:true,
				success:function(data){
					if(idx < maxSaveCount){
						httpRequester.request({
							url:"/memo/"+customer.id,
							method:"GET",
							data:{
								count:1,
								designerId : loginSession.getSession().id,
								timestamp : memoList.data[memoList.data.length-1].timestamp
							},
							success:function(memoData){
								if(memoData.result.length != 0){
									memoList.data.push(memoData.result[0]);
									storageManager.save(key+customer.id, memoList);
								}
							}
						});
					}
				}
			});

		}else{
			facObj.removeRoom(key+customer.id);
			q=httpRequester.request({
				url:"/memo/"+loginSession.getSession().id+"/"+customer.id+"/"+delMemo.timestamp,
				method:"DELETE", 
				resend:true
			});
			
		}
		return q;
	};
	facObj.update = function(memo, idx){
		var memoList = facObj.loadCustomer(memo.customer);
		var i =0;
		memoList.data[idx].content = memo.content;
		memoList.data[idx].price= memo.price;
		memoList.data[idx].proc= memo.proc;
		memo.timestamp = memoList.data[idx].timestamp;


		var storageData = storageManager.load(key+memo.customer.id);
		if(storageData.data.length > idx){
			storageData.data[idx].content = memo.content;
			storageData.data[idx].price = memo.price;
			storageData.data[idx].proc= memo.proc;
			storageManager.save(key+memo.customer.id, storageData);
		}


		var q = httpRequester.request({
			url:"/memo/"+loginSession.getSession().id+"/"+memo.customer.id+"/"+memo.timestamp, 
			method:"PUT", 
			resend:true,
			data:{
				content : memo.content,
				price : memo.price,
				proc : memo.proc
			}
		});
		return q;

	};
	facObj.removeRoom = function (id){
		var idx = memoCache.idx[id];
		if(memoCache.list[idx].pushId){
			nativeCall.removeNoti();
		}
		memoCache.list.splice(idx, 1);
		reorderIdx();
		storageManager.remove(key+id);
		$rootScope.$broadcast("$scrollToTop");

	};
	facObj.load = function(){
		return memoCache;
	};
	facObj.loadCustomer=function (customer){
		var idx = memoCache.idx[customer.id];
		if(idx == undefined){
			
			return facObj.initMemoInVisit(customer);
		}else {
			return memoCache.list[idx];
		}
		//return storageManager.load(key+customer.id) || {isVisit:undefined, data:[], customer:{id:customer.id,name:customer.name}};
	};
	
	/*
	facObj.loadNotSended=function (customerId){
		var result=[];

		result = storageManager.loadAll(customerId +" ");

		return result;
	};
	*/


	facObj.init=function (){
		var promise = httpRequester.request({
			url:"/allMemo/"+loginSession.getSession().id,
			method:"GET",
			data:{count:maxSaveCount},
			success:function(data){
				for(var who in data.result){
					var saveObj = {};
					saveObj.isVisit=undefined;
					saveObj.data=data.result[who].memoList;
					saveObj.customer={};
					saveObj.customer.id=data.result[who].id;
					saveObj.customer.name=data.result[who].name;
					storageManager.save(key+who, saveObj);
				}
				initCache();
			}
		});
		return promise;
	};

	facObj.setVisit = function(pushData, pushId){
		var memoItem ;
		memoItem = facObj.loadCustomer(pushData.customer);
		if(pushData.memo){
				/*
				for(var i=0;i<pushData.memo.data.length;i++){
					memoItem.data.push(pushData.memo.data[i]);
				}
				*/
			var index =  memoCache.idx[pushData.customer.id];
			if(index){
				memoCache.list.splice(index, 1);
				//memoCache.list.splice(0, 0, memoItem);
				memoCache.list.unshift(memoItem);
				memoItem.data = pushData.memo.data;
				reorderIdx();
			}
		}else{
			var index =  memoCache.idx[pushData.customer.id];
			var elem = memoCache.list[index];
			memoCache.list.splice(index, 1);
			memoCache.list.splice(0, 0, memoItem);
			reorderIdx();
		}
		memoItem.isVisit = pushData.time;
		memoItem.pushId= pushId;
		storageManager.save(key+pushData.customer.id, memoItem);
	};

	facObj.updateCustomerInfo = function(customer){
		customerMemo = facObj.loadCustomer(customer);
		customerMemo.customer.name = customer.name;
		storageManager.save(key+customer.id, customerMemo);
	};

	function initCache (){
		//memoCache={};
		memoCache.list = getLastMemo();
		memoCache.getVisitCount = function(){
			var i =0;
			var len = memoCache.list.length;
			//memoCache.list.
			while(len>i){
				if(memoCache.list[i++].isVisit){

				}else{
					i--;
					break;
				}
			}
			return i;
		};

		reorderIdx();
	};
	function reorderIdx(){
		var i = memoCache.list.length-1;
		memoCache.idx={};
		if(i > -1 ){
			while(true){
				if(i<0){
					break;
				}
				memoCache.idx[memoCache.list[i].customer.id] = i;
				i--;
			}
		}
	};
	initCache();

	return facObj;	
});

