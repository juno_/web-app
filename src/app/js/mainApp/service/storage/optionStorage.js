mainApp.factory("optionStorage", function(storageManager, httpRequester, loginSession, $q, $rootScope){
	
	//storage key : <designerId customerId time>
	var key = "option";
	var procKey = "proc";
	var facObj={};

	function init(){
		facObj.proc = storageManager.load(key+procKey) || [];
	};
	init();
	facObj.load=function (){
		var result = [];
		result = storageManager.load(key+procKey) || [];
		return result;
	};


	facObj.init=function (){
		var promise = httpRequester.request({
			url:"/option/proc/"+loginSession.getSession().branch,
			method:"GET",
			success:function(data){

				var optionObj = data.result;
				facObj.proc = data.result;
				storageManager.save(key+procKey, optionObj);
			//	storageManager.save(key+who, saveObj);
			}
		});
		return promise;
	};

	return facObj;	
});

