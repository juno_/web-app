mainApp.provider("storageManager", function(){
	

	function save(key, obj){
		window.localStorage.setItem(key, JSON.stringify(obj));
	};
	function load(key){

		var obj;
		obj = JSON.parse(window.localStorage.getItem(key));	
		return obj
	}
	function remove(key){
		window.localStorage.removeItem(key);
	}
	function loadAll(key){
		var result=[];
		for(var i in window.localStorage){
			if(i.indexOf(key) == 0){
				result.push(JSON.parse(window.localStorage.getItem(i)));	
			}
		};
		return result; 
	}
	function loadAllWithKey(key){
		var result=[];
		for(var i in window.localStorage){
			if(i.indexOf(key) == 0){
				var obj={};
				obj.key = i;
				obj.value = JSON.parse(window.localStorage.getItem(i));
				result.push(obj);	
			}
		};
		return result; 
	}
	
	return {
		load:load,
		$get:function(){
			return {
				save:save,
				load:load,
				remove:remove,
				loadAll:loadAll,
				loadAllWithKey:loadAllWithKey
			}
		}

	 
	};	
});
/*
mainApp.factory("storageManager", function(){
	
	var facObj={};

	facObj.save=function (key, obj){
		window.localStorage.setItem(key, JSON.stringify(obj));
	};
	facObj.load=function (key){

		var obj;
		obj = JSON.parse(window.localStorage.getItem(key));	
		return obj
	}
	facObj.remove=function (key){
		window.localStorage.removeItem(key);
	}
	facObj.loadAll=function (key){
		var result=[];
		for(var i in window.localStorage){
			if(i.indexOf(key) == 0){
				result.push(JSON.parse(window.localStorage.getItem(i)));	
			}
		};
		return result; 
	}
	facObj.loadAllWithKey=function (key){
		var result=[];
		for(var i in window.localStorage){
			if(i.indexOf(key) == 0){
				var obj={};
				obj.key = i;
				obj.value = JSON.parse(window.localStorage.getItem(i));
				result.push(obj);	
			}
		};
		return result; 
	}
	
	return facObj;	
});
*/
