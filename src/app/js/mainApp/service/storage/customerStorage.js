mainApp.factory("customerStorage", function(storageManager, httpRequester, loginSession, $rootScope){
	
	var key="customer",
	    cacheData = undefined;


	var facObj={};

	cacheData=initCache();
	function initCache (){
		var result=storageManager.load(key);
		if(result == undefined){
			return [];
		}else{
			return result;
		}
	};
	function nameComparator(val1, val2){
		if(val1.name  < val2.name){
			return -1;
		}else if(val1.name  > val2.name){
			return 1;
		}else if(val1.name  == val2.name){
			return 0;
		}
			
	};
	function insertPoint(sortedArr, val, comparator){
		var low = 0, high = sortedArr.length;
		var mid = -1, c = 0;
		while(low < high)   {
			mid = parseInt((low + high)/2);
			c = comparator(sortedArr[mid], val);
			if(c < 0)   {
				low = mid + 1;
			}else if(c > 0) {
				high = mid;
			}else {
				return mid;
			}
		}

		return low;
	};




	//////////////////////////////////////////
	facObj.find = function(customer){
		/*
		var insertIdx = insertPoint(cacheData, customer, nameComparator);
		var len = cacheData.length;
		for(var i=insertIdx;i<len;i++){
			if(cacheData[i].id == customer.id){
				return true;
			}
			if(cacheData[i].name != customer.name){
				return false;
			}
		}
		return false;
		*/

		var len=cacheData.length;
		for(var i=0;i<len;i++){
			if(cacheData[i].id == customer.id){
				return i;
			}
		}
		return -1;
	};

	facObj.isExist = function(){
		var result=storageManager.load(key);
		if(result == undefined){
			return false;
		}else{
			return true;
		}

	}


	facObj.modFixedMemo = function(selCustomer, fixedMemo){

		var idx= insertPoint(cacheData, selCustomer, nameComparator);
		cacheData[idx].fixedMemo = fixedMemo;
		storageManager.save(key, cacheData);

		httpRequester.request({
			url:"/customer/fixedMemo",
			method:"PUT",
			resend:true,
			data:{
				customerId : cacheData[idx].id,
				designerId: loginSession.getSession().id,
				fixedMemo: fixedMemo
			},
			success:function(data){

			},
			error:function(data){
				//add to resender
			}
		});
	};
	
	/*
	facObj.modFixedMemo = function(idx, fixedMemo){

		cacheData[idx].fixedMemo = fixedMemo;
		storageManager.save(key, cacheData);

		httpRequester.request({
			url:"/customer/fixedMemo",
			method:"PUT",
			resend:true,
			data:{
				customerId : cacheData[idx].id,
				designerId: loginSession.getSession().id,
				fixedMemo: fixedMemo
			},
			success:function(data){

			},
			error:function(data){
				//add to resender
			}
		});
	};
	*/
	facObj.changeInfo =function(customer){
		var i=cacheData.length-1;
		while(i>=0){
			if(cacheData[i].id == customer.id){
				if(cacheData[i].name != customer.name){
					cacheData.splice(i, 1);
					var newCustomer={};
					newCustomer.name = customer.name;
					newCustomer.id= customer.id;
					newCustomer.gender= customer.gender;
					newCustomer.birth= customer.birth;
					newCustomer.lastPhone= customer.lastPhone;
					newCustomer.fixedMemo = cacheData[i].fixedMemo;

					var insertIdx = insertPoint(cacheData, newCustomer, nameComparator);
					cacheData.splice(insertIdx, 0, newCustomer);
				}else{
					cacheData[i].name = customer.name;
					cacheData[i].id= customer.id;
					cacheData[i].gender= customer.gender;
					cacheData[i].birth= customer.birth;
					cacheData[i].lastPhone= customer.lastPhone;
				}
				break;
			}
			i--;
		}

		storageManager.save(key, cacheData);

	};

	facObj.save = function(customer){
		var customerList; 
		if(Object.prototype.toString.call(customer) === "[object Array]" ){	//only for init?
			storageManager.save(key, customer);	
			cacheData = customer;
		}else{
			var insertIdx = insertPoint(cacheData, customer, nameComparator);
			cacheData.splice(insertIdx, 0, customer);
			storageManager.save(key, cacheData);
			$rootScope.$broadcast("$scrollToTop");
		}
	};
	facObj.removeAll = function(){
		storageManager.remove(key);
		cacheData.length = 0;
	}
	facObj.remove = function(customerId){
		var i=cacheData.length-1;
		
                while(i >= 0){
                        if(cacheData[i].id == customerId){
                                cacheData.splice(i, 1); 
                                break;
                        }   
                        i--;
                }   
                storageManager.save(key, cacheData);
		$rootScope.$broadcast("$scrollToTop");

	};
	facObj.load = function(){
		//return cacheData.slice(0);
		return cacheData;
	};
	facObj.size = function(){
		return cacheData.length;
	};
	facObj.init = function(){
		var promise = httpRequester.request({
			url:"/myCustomer/"+loginSession.getSession().id,
			method:"GET", 
			success:function(data){
				facObj.save(data.result);
			}
		});

		return promise;


	};

	return facObj;
	
});

