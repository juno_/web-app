mainApp.controller("statsCtrl", ["$scope", "httpRequester", "loginSession",  function($scope, httpRequester, loginSession){
	$scope.init = function(){
		$scope.main = {};
		httpRequester.request({
			url:"/stats/main/"+loginSession.getSession().id,
			method:"GET",
			timeout:10000,
			success:function(data){
				console.log(data);
				var weekNameShort = ["일", "월", "화", "수", "목", "금", "토"];
				$scope.main.today = data.result.today;
				$scope.main.total ={}; 
				$scope.main.total.m = ((data.result.gender.M || 0) * 1);
				$scope.main.total.f = ((data.result.gender.F || 0) * 1);
				$scope.main.total.tot = $scope.main.total.f + $scope.main.total.m; 
				$scope.main.busyDay = weekNameShort[data.result.busyDay];
				$scope.main.proc = {};
				$scope.main.proc.val = data.result.proc.y;
				$scope.main.proc.procTxt = data.result.proc.x;
			}

		});
	};
	$scope.init();

}]);
mainApp.directive("periodChart", function(){
	var ctx;
	return{
		restrict : "E",
		scope:{
			x:"=",
			y:"=",
			axisXTerm:"="
		},
		link : function($scope, $element, $attrs){
			ctx = $element[0].querySelector("canvas").getContext("2d");

			//$scope.y.splice(0, 9);
			//$scope.x.splice(0, 9);

			var option={};
			var chartElem=undefined;
			option.animation=false;
			option.scaleGridLineColor="#ddd";
			option.scaleLineColor="#ddd";
			option.tooltipTemplate="<%=value%>명";
			option.scaleShowVerticalLines=false;
			option.scaleShowHorizontalLines=false;
			option.showTooltips = true;
			
			option.labelsFilter =function(value, index){
				return (index) % $scope.axisXTerm !==0;
			}

			chartElem = new Chart(ctx).Bar({
				labels:$scope.x,
				datasets:[
					{   
						fillColor:"rgba(49, 164, 188, 1)",
						strokeColor:"rgba(49, 164, 188, 1)",
						pointColor:"rgba(49, 164, 188, 1)",
						pointStrokeColor:"#fff",
						//pointHighlightStroke:"#f26162",
						pointHighlightStroke:"rgba(49, 164, 188, 1)",
						pointHighlightFill:"#fff",
						data:$scope.y
					}   
				]   
			}, option);

		},
		template : function($element, $attrs, $scope){
			var tmp ="";
			tmp="<canvas style='width:100%; height:150px;' ></canvas>";
			return tmp;
		}
	}
});
mainApp.directive("periodChartTot", function(){
	var ctx;
	return{
		restrict : "E",
		scope:{
			coord:"="
		},
		link : function($scope, $element, $attrs){
			ctx = $element[0].querySelector("canvas").getContext("2d");
			$scope.$watch("coord", function(newVal, oldVal){
				$scope.drawChart();
			});

			var chartElem=undefined;
			var option={};
			option.animation=false;
			option.scaleGridLineColor="#ddd";
			option.scaleLineColor="#ddd";
			option.tooltipTemplate="<%=value%>명";
			option.scaleShowVerticalLines=false;
			option.scaleShowHorizontalLines=false;
			option.showTooltips = true;
			option.removeAxisY = true;
			option.bezierCurve=false;
			
			/*
			option.labelsFilter =function(value, index){
				return (index) %3 !==0;
			}
			*/

			$scope.drawChart = function(){
				if(chartElem != undefined){
					chartElem.destroy();
				}
				chartElem = new Chart(ctx).Line({
					labels:$scope.coord.x,
					datasets:[
						{   
							fillColor:"rgba(49, 164, 188, 0)",
							strokeColor:"rgba(49, 164, 188, 1)",
							pointColor:"rgba(49, 164, 188, 1)",
							pointStrokeColor:"#fff",
							//pointHighlightStroke:"#f26162",
							pointHighlightStroke:"rgba(49, 164, 188, 1)",
							pointHighlightFill:"#fff",
							data:$scope.coord.y
						}   
					]   
				}, option);
			}
			$scope.drawChart();

		},
		template : function($element, $attrs, $scope){
			var tmp ="";
			tmp="<canvas style='width:100%; height:150px;' ></canvas>";
			return tmp;
		}
	}
});
mainApp.controller("visitCtrl", ["$scope", "$ionicHistory", "httpRequester", "$timeout", "loginSession", "$ionicModal", function($scope, $ionicHistory, httpRequester, $timeout, loginSession, $ionicModal){
	$scope.init = function(){
		$scope.period ={};
		$scope.chart={};
		$scope.tot={x:[], y:[]};
		$scope.selDate = new Date();
		$scope.selMonth = new Date();
		$scope.selYear = new Date();
		$ionicModal.fromTemplateUrl("datepicker.html", {
			scope:$scope
		}).then(function(modal){
			$scope.dateModal = modal;
		});
		
		$ionicModal.fromTemplateUrl("monthpicker.html", {
			scope:$scope
		}).then(function(modal){
			$scope.monthModal = modal;
		});

		$ionicModal.fromTemplateUrl("yearpicker.html", {
			scope:$scope
		}).then(function(modal){
			$scope.yearModal = modal;
		});

		//$scope.drawWeekChart(new Date());
		$scope.weekly();

	};
	$scope.showPicker = function(){
		if($scope.period.dist == 0){
			$scope.dateModal.show();
		}else if($scope.period.dist == 1){
			$scope.monthModal.show();
		}else if($scope.period.dist == 2){
			$scope.yearModal.show();
		}
	};
	$scope.selectDate = function(date){
		$scope.dateModal.hide();
		$scope.selDate = date;
		$scope.drawWeekChart(date);
	};
	$scope.selectMonth = function(date){
		$scope.monthModal.hide();
		$scope.selMonth = date;
		$scope.drawMonthChart(date);
	};
	$scope.selectYear = function(date){
		$scope.yearModal.hide();
		$scope.selYear = date;
		$scope.drawYearChart(date);
	};
	$scope.weekly= function(){
		$scope.period.dist = 0;
		$scope.chart.axisXTerm = 3;
		$scope.chart.totTitle="주간 통계";
		$scope.chart.title="일별 상세보기";
		$scope.drawWeekChart(new Date());
	};
	$scope.monthly= function(){
		$scope.chart.axisXTerm = 1;
		$scope.period.dist = 1;
		$scope.chart.totTitle="월간 통계";
		$scope.chart.title="주별 상세보기";
		$scope.drawMonthChart(new Date());
	};
	$scope.yearly= function(){
		$scope.chart.axisXTerm = 1;
		$scope.period.dist = 2;
		$scope.chart.totTitle="연간 통계";
		$scope.chart.title="월별 상세보기";
		$scope.drawYearChart(new Date());
	};
	$scope.sum = function(data){
		var count=0;
		for(var i=0;i<data.length;i++){
			if(data[i]==""){
				continue;
			}
			count+= parseInt(data[i]);
		}
		return count;
	};
	$scope.printTitle = function(title){
		if($scope.period.dist == 0){
			var d = new Date(title);
			return d.format("MM/dd(e)");
		}else if($scope.period.dist == 1){
			return title;
		}else if($scope.period.dist == 2){
			return title;
		}
	};

	$scope.drawWeekChart = function(date){
		var sDate; 
		var eDate = date;
		sDate = new Date(eDate.getFullYear(), eDate.getMonth(), eDate.getDate()-6);
		$scope.selTerm = sDate.format("MM월dd일") +" ~ " +eDate.format("MM월dd일");

		sDate = sDate.format("yyyy-MM-dd");
		eDate = eDate.format("yyyy-MM-dd");

		httpRequester.request({
			url:"/stats/visitWeek/"+loginSession.getSession().id,
			method:"GET",
			data:{
				sDate:sDate,
				eDate:eDate
			},
			success:function(data){
				data.result.reverse();
				$scope.chart.list= data.result;
				$scope.tot={x:[], y:[]};
				var len = $scope.chart.list.length;
				$scope.chart.totCount = 0;
				var tmpSum = 0;
				for(var i=0;i<$scope.chart.list.length;i++){
					$scope.chart.list[i].x.splice(0, 9);
					$scope.chart.list[i].y.splice(0, 9);
					$scope.chart.list[i].title = $scope.chart.list[i].date;
					$scope.tot.x.push($scope.chart.list[len-1-i].date.substring(5, 10).replace("-", "/"));
					tmpSum = $scope.sum(data.result[len-1-i].y);
					$scope.tot.y.push(tmpSum);
					$scope.chart.totCount +=  tmpSum;
				}


			}

		});
	};
	$scope.drawMonthChart = function(date){
		$scope.selTerm = date.format("yyyy년 MM월");
		var yearMonth = date.format("yyyy-MM");


		httpRequester.request({
			url:"/stats/visitMonth/"+loginSession.getSession().id,
			method:"GET",
			data:{
				yearMonth : yearMonth
			},
			success:function(data){
				var weekCount = data.result.weekCount;
				var year = data.result.date.substring(0, 4);
				var month = parseInt(data.result.date.substring(5, 7))-1;
				var cal = new Date(year, month, 1);
				$scope.chart.totCount = 0;
				$scope.chart.list=[];
				$scope.tot={
					x:[],
					y:[]
				};
				for(var i=0;i<weekCount;i++){
					$scope.chart.list[i]={x:[], y:[]};
				}
				var beforeWeekOfMon=-1;
				var nowWeekOfMon = 0;
				var tmpSum=0;
				while(cal.getMonth() == month){
					var nowFormat = cal.format("yyyy-MM-dd");
					var statsInfo = data.result.data[nowFormat];
					beforeWeekOfMon = nowWeekOfMon;
					nowWeekOfMon = Math.ceil((cal.getDate()-1-cal.getDay())/7);
					$scope.chart.list[nowWeekOfMon].x.push(cal.format("MM/dd"));
					$scope.chart.list[nowWeekOfMon].y.push((statsInfo!=undefined ? statsInfo.y : 0));

					if($scope.tot.x[nowWeekOfMon] == undefined){
						$scope.chart.list[nowWeekOfMon].title=(nowWeekOfMon+1)+"주";
					}
					if(nowWeekOfMon != beforeWeekOfMon){
						$scope.tot.x.push((beforeWeekOfMon+1)+"주");
						tmpSum = $scope.sum($scope.chart.list[beforeWeekOfMon].y);
						$scope.tot.y.push(tmpSum);
						$scope.chart.totCount += tmpSum;
						if($scope.chart.list[beforeWeekOfMon].x.length <= 7){
							for(var i=$scope.chart.list[beforeWeekOfMon].x.length ;i<7;i++){
								$scope.chart.list[beforeWeekOfMon].x.push("");
								$scope.chart.list[beforeWeekOfMon].y.push("");
							}
						}
					}

					cal.setDate(cal.getDate()+1);
				}
				$scope.tot.x.push((nowWeekOfMon+1)+"주");
				tmpSum = $scope.sum($scope.chart.list[nowWeekOfMon].y)
				$scope.tot.y.push(tmpSum);
				$scope.chart.totCount += tmpSum;
				if($scope.chart.list[nowWeekOfMon].x.length <= 7){
					for(var i=$scope.chart.list[nowWeekOfMon].x.length ;i<7;i++){
						$scope.chart.list[nowWeekOfMon].x.push("");
						$scope.chart.list[nowWeekOfMon].y.push("");
					}
				}

			}

		});
	};
	$scope.drawYearChart = function(date){
		$scope.selTerm = date.format("yyyy년");
		var year= date.getFullYear();;


		httpRequester.request({
			url:"/stats/visitYear/"+loginSession.getSession().id,
			method:"GET",
			data:{
				year: year
			},
			success:function(data){
				var year = data.result.date;
				var cal = new Date(year, 0, 1);
				$scope.chart.totCount = 0;
				$scope.chart.list=[];
				$scope.tot={
					x:[],
					y:[]
				};
				var nowWeekOfMon = 0;
				var tmpSum=0;
				for(var i=0;i<12;i++){
					$scope.chart.list[i]={x:[], y:[]};
					var firstOfMonth = new Date(cal.getFullYear(), cal.getMonth(), 1);
					var lastOfMonth = new Date(cal.getFullYear(), cal.getMonth()+1, 0);
					var weekCount = Math.ceil((firstOfMonth.getDay() + lastOfMonth.getDate())/7);

					$scope.chart.list[i].x=[];
					$scope.chart.list[i].y=[];
					var monthFormat = cal.format("MM");
					for(var j=0;j<weekCount;j++){
						var statsItem = data.result.data[monthFormat+" "+j];
						if(statsItem){
							$scope.chart.list[i].x.push((j+1)+"주");
							$scope.chart.list[i].y.push(statsItem.y);
						}else{
							$scope.chart.list[i].x.push((j+1)+"주");
							$scope.chart.list[i].y.push(0);
						}
					}
					$scope.chart.list[i].title = (i+1)+"월";

					$scope.tot.x.push((i+1)+"월");
					$scope.tot.y.push($scope.sum($scope.chart.list[i].y));
					tmpSum += $scope.tot.y[$scope.tot.y.length-1];


					cal.setMonth(cal.getMonth()+1);
				}
				$scope.chart.totCount = tmpSum;

			}

		});
	};
	$scope.init();
	
}]);
mainApp.controller("totalCustomerCtrl", ["$scope", "$ionicHistory", "httpRequester", "$timeout", "loginSession", function($scope, $ionicHistory, httpRequester, $timeout, loginSession){
	$scope.init = function(){
		$scope.chart = {};
		$scope.genderChartEle = undefined;
		$scope.ageChartEle = undefined;

		httpRequester.request({
			url:"/stats/totalCustomer/"+loginSession.getSession().id,
			method:"GET",
			success:function(data){
				$scope.chart.gender = data.result.gender;
				$scope.chart.age = data.result.age;

				var genderData = [];
				var val = $scope.chart.gender["M"]==undefined ? 0 : $scope.chart.gender["M"];
				genderData.push({
					value:val*1,
					color:"#46BFBD",
					highlight:"#5AD3D1",
					label:"남"
				});
				val = $scope.chart.gender["F"]==undefined ? 0 : $scope.chart.gender["F"];
				genderData.push({
					value:val*1,
					color:"#F7464A",
					highlight:"#FF5A5E",
					label:"여"
				});
				var genderCtx = document.querySelector("#gender-chart").getContext("2d");
				if($scope.genderChartEle != undefined){
					$scope.genderChartEle.destroy();
				}
				$scope.genderChartEle = new Chart(genderCtx).Doughnut(genderData, {
					animation:false,
					percentageInnerCutout:65
				});
				var manRatio = Math.floor((genderData[0].value/(genderData[0].value+genderData[1].value)) * 100);
				$scope.chart.gender.txt = " "+manRatio +" : "+(100-manRatio)+" ";




				var ageCtx = document.querySelector("#age-chart").getContext("2d");

				var option={};
				var chartElem=undefined;
				option.animation=false;
				option.scaleGridLineColor="#ddd";
				option.scaleLineColor="#ddd";
				option.tooltipTemplate="<%=value%>명";
				option.scaleShowVerticalLines=false;
				option.scaleShowHorizontalLines=false;
				option.showTooltips = true;
				
				//color:"#46BFBD",		m
				//color:"#F7464A",		f
				var data = {
					"F" :[],
					"M" :[]
				};
				var idx =0;
				var labels=[];
				for(var i=0;i<9;i++){
					if(i > 5){
						if($scope.chart.age["M "+(i+1)] != undefined){
							data["M"][data["M"].length-1]+=($scope.chart.age["M "+(i+1)] * 1); 
						}
						
						if($scope.chart.age["F "+(i+1)] != undefined){
							data["F"][data["F"].length-1]+=($scope.chart.age["F "+(i+1)] * 1); 
						}
					}else{
						labels.push((i+1)+"0대");

						if($scope.chart.age["M "+(i+1)] == undefined){
							data["M"].push(0);
						}else{
							data["M"].push($scope.chart.age["M "+(i+1)]);
						}


						if($scope.chart.age["F "+(i+1)] == undefined){
							data["F"].push(0);
						}else{
							data["F"].push($scope.chart.age["F "+(i+1)]);
						}
					}

				}
				labels[labels.length-1]+=" 이상";
				if($scope.chart.age["M "+"-100"] != undefined || $scope.chart.age["F "+"-100"] != undefined){
					data["M"].push($scope.chart.age["M "+"-100"]==undefined ? 0 : $scope.chart.age["M "+"-100"]);
					data["F"].push($scope.chart.age["F "+"-100"]==undefined ? 0 : $scope.chart.age["F "+"-100"]);
					labels[labels.length]="Unknown";
				}

				if($scope.ageChartEle != undefined){
					$scope.ageChartEle.destroy();

				}
				$scope.ageChartEle = new Chart(ageCtx).Bar({
					labels:labels,
					datasets:[
						{   
							fillColor:"#46BFBD",
							strokeColor:"#46BFBD",
							pointColor:"#46BFBD",
							pointStrokeColor:"#fff",
							//pointHighlightStroke:"#f26162",
							pointHighlightStroke:"#46BFBD",
							pointHighlightFill:"#fff",
							data:data["M"]
						},
						{   
							fillColor:"#F7464A",
							strokeColor:"#F7464A",
							pointColor:"#F7464A",
							pointStrokeColor:"#fff",
							//pointHighlightStroke:"#f26162",
							pointHighlightStroke:"#F7464A",
							pointHighlightFill:"#fff",
							data:data["F"]
						}   
					]   
				}, option);



			}

		});



	};


	$scope.init();
}]);

mainApp.controller("busyDayCtrl", ["$scope", "$ionicHistory", "httpRequester", "$timeout", "loginSession", "$ionicModal", "$ionicPopup", function($scope, $ionicHistory, httpRequester, $timeout, loginSession, $ionicModal, $ionicPopup){
	$scope.init = function(){
		//initialize date is
		//edate : today - 1
		//sdate : edate - 7
		$scope.chart={};
		$scope.chart.eDate=new Date();
		$scope.chart.sDate=new Date($scope.chart.eDate.getFullYear(), $scope.chart.eDate.getMonth(), $scope.chart.eDate.getDate()-6);

		$scope.chart.sDateTxt=$scope.chart.sDate.format("MM월dd일");
		$scope.chart.eDateTxt=$scope.chart.eDate.format("MM월dd일");
		$scope.busyChartEle = undefined;
		$ionicModal.fromTemplateUrl("sdatepicker.html", {
			scope:$scope
		}).then(function(modal){
			$scope.sDateModal = modal;
		});
		$ionicModal.fromTemplateUrl("edatepicker.html", {
			scope:$scope
		}).then(function(modal){
			$scope.eDateModal = modal;
		});
		$scope.drawChart();
	};
	$scope.showSDatePicker = function(){
		$scope.sDateModal.show();
	};
	$scope.selectSDate = function(date){
		$scope.chart.sDate = date;
		$scope.sDateModal.hide();
		$scope.chart.sDateTxt=$scope.chart.sDate.format("MM월dd일");
	};
	$scope.showEDatePicker = function(){
		$scope.eDateModal.show();
	};
	$scope.selectEDate = function(date){
		$scope.chart.eDate = date;
		$scope.eDateModal.hide();
		$scope.chart.eDateTxt=$scope.chart.eDate.format("MM월dd일");
	};
	$scope.drawChart = function(){
		if($scope.chart.eDate.format("yyyyMMdd") < $scope.chart.sDate.format("yyyyMMdd")){
			$ionicPopup.alert({
				title:"날짜 선택",
				template:"<center>끝날짜는 시작날짜보다 먼저 나올 수 없습니다.</center>"
			});
			return;
		}
		httpRequester.request({
			url:"/stats/busyDay/"+loginSession.getSession().id,
			method:"GET",
			data :{
				sDate : $scope.chart.sDate.format("yyyy-MM-dd"),
				eDate : $scope.chart.eDate.format("yyyy-MM-dd")
			},
			success:function(data){
				var y =[];
				var labels = ['일', '월', '화', '수', '목', '금', '토'];
				for(var i=0;i<labels.length;i++){
					y.push(data.result[i]==undefined ? 0:data.result[i]);
				}
				

				var busyCtx = document.querySelector("#busyday-chart").getContext("2d");;
				var option={};
				option.animation=false;
				option.scaleGridLineColor="#ddd";
				option.scaleLineColor="#ddd";
				option.tooltipTemplate="<%=value%>명";
				option.scaleShowVerticalLines=false;
				option.scaleShowHorizontalLines=false;
				option.showTooltips = true;
				if($scope.busyChartEle != undefined){
					$scope.busyChartEle.destroy();
				}
				$scope.busyChartEle = new Chart(busyCtx).Bar({
					labels:labels,
					datasets:[
						{   
							fillColor:"rgba(49, 164, 188, 1)",
							strokeColor:"rgba(49, 164, 188, 1)",
							pointColor:"rgba(49, 164, 188, 1)",
							pointStrokeColor:"#fff",
							//pointHighlightStroke:"#f26162",
							pointHighlightStroke:"rgba(49, 164, 188, 1)",
							pointHighlightFill:"#fff",
							data:y
						}   
					]   
				}, option);



			}

		});
	};
	$scope.init();
}]);
mainApp.controller("procStatsCtrl", ["$scope", "$ionicHistory", "httpRequester", "$timeout", "loginSession", "$ionicModal", "$ionicPopup", function($scope, $ionicHistory, httpRequester, $timeout, loginSession, $ionicModal, $ionicPopup){
	$scope.init = function(){
		//initialize date is
		//edate : today - 1
		//sdate : edate - 7
		$scope.chart={};
		$scope.chart.eDate=new Date();
		$scope.chart.sDate=new Date($scope.chart.eDate.getFullYear(), $scope.chart.eDate.getMonth(), $scope.chart.eDate.getDate()-6);

		$scope.procChartEle = undefined;
		$scope.ageProcChartEle = undefined;

		$scope.chart.sDateTxt=$scope.chart.sDate.format("MM월dd일");
		$scope.chart.eDateTxt=$scope.chart.eDate.format("MM월dd일");
		$ionicModal.fromTemplateUrl("sdatepicker.html", {
			scope:$scope
		}).then(function(modal){
			$scope.sDateModal = modal;
		});
		$ionicModal.fromTemplateUrl("edatepicker.html", {
			scope:$scope
		}).then(function(modal){
			$scope.eDateModal = modal;
		});
		$scope.drawChart();
	};
	$scope.showSDatePicker = function(){
		$scope.sDateModal.show();
	};
	$scope.selectSDate = function(date){
		$scope.chart.sDate = date;
		$scope.sDateModal.hide();
		$scope.chart.sDateTxt=$scope.chart.sDate.format("MM월dd일");
	};
	$scope.showEDatePicker = function(){
		$scope.eDateModal.show();
	};
	$scope.selectEDate = function(date){
		$scope.chart.eDate = date;
		$scope.eDateModal.hide();
		$scope.chart.eDateTxt=$scope.chart.eDate.format("MM월dd일");
	};
	$scope.drawChart = function(){
		if($scope.chart.eDate.format("yyyyMMdd") < $scope.chart.sDate.format("yyyyMMdd")){
			$ionicPopup.alert({
				title:"날짜 선택",
				template:"<center>끝날짜는 시작날짜보다 먼저 나올 수 없습니다.</center>"
			});
			return;
		}
		httpRequester.request({
			url:"/stats/procStats/"+loginSession.getSession().id,
			method:"GET",
			data :{
				sDate : $scope.chart.sDate.format("yyyy-MM-dd"),
				eDate : $scope.chart.eDate.format("yyyy-MM-dd")
			},
			success:function(data){
				var y =[];
				var labels = [];
				var procData = [];
				var colors = ["#18aa8e", "#f08041", "#fae25e", "#abd825"];
				$scope.chart.totProc = 0;
				var i=0;
				var totalHash = {};
				var labels = [];
				var genderData={
					"M":[],
					"F":[]
				};
				for(var key in data.result){
					var subKey = key.split(" ")[0];
					var gender = key.split(" ")[1];
					if(totalHash[subKey]!=undefined){
						totalHash[subKey]+=(data.result[key]*1);
					}else{
						totalHash[subKey]=(data.result[key]*1);
						labels.push(subKey);
					}
					$scope.chart.totProc+=(data.result[key]*1);
				}
				for(var key in totalHash){
					procData.push({
						value:totalHash[key],
						color:colors[i],
						label:key
					});
					i++;
				}

				for(var i=0;i<labels.length;i++){
					genderData["M"].push(data.result[labels[i]+" M"]);
					genderData["F"].push(data.result[labels[i]+" F"]);
				}
				
				var procTotalCtx = document.querySelector("#proc-chart").getContext("2d");
				if($scope.procChartEle != undefined){
					$scope.procChartEle.destroy();
				}
				$scope.procChartEle = new Chart(procTotalCtx).Doughnut(procData, {
					animation:false,
					percentageInnerCutout:65
				});

				var procGenderCtx = document.querySelector("#proc-chart2").getContext("2d");
				var option={};
				option.animation=false;
				option.scaleGridLineColor="#ddd";
				option.scaleLineColor="#ddd";
				option.tooltipTemplate="<%=value%>명";
				option.scaleShowVerticalLines=false;
				option.scaleShowHorizontalLines=false;
				option.showTooltips = true;


				if($scope.ageProcChartEle != undefined){
					$scope.ageProcChartEle.destroy();
				}
				$scope.ageProcChartEle = new Chart(procGenderCtx).Bar({
					labels:labels,
					datasets:[
						{   
							fillColor:"#46BFBD",
							strokeColor:"#46BFBD",
							pointColor:"#46BFBD",
							pointStrokeColor:"#fff",
							//pointHighlightStroke:"#f26162",
							pointHighlightStroke:"#46BFBD",
							pointHighlightFill:"#fff",
							data:genderData["M"]
						},
						{   
							fillColor:"#F7464A",
							strokeColor:"#F7464A",
							pointColor:"#F7464A",
							pointStrokeColor:"#fff",
							//pointHighlightStroke:"#f26162",
							pointHighlightStroke:"#F7464A",
							pointHighlightFill:"#fff",
							data:genderData["F"]
						}   
					]   
				}, option);
			}

		});
	};
	$scope.init();
}]);
mainApp.controller("returnRatioCtrl", ["$scope", "$ionicHistory", "httpRequester", "$timeout", "loginSession", "$ionicModal", "$ionicPopup", function($scope, $ionicHistory, httpRequester, $timeout, loginSession, $ionicModal, $ionicPopup){
	$scope.init = function(){
	};

	$scope.init();
}]);

