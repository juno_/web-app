mainApp.controller("preferenceCtrl", function($scope, loginSession, $state){

	$scope.init = function(){
		$scope.setAlarm(0);
	};
	
	$scope.signOut = function(){
		loginSession.signOut();

		window.location.href=ROOT+"/index.html";
	};
	$scope.setAlarm = function(index){
		$scope.activeIdx = index;
	};
	$scope.account = function(){
		$state.go("account");
	};
	$scope.stats= function(){
		$state.go("stats");
	};

	$scope.init();
});
