mainApp.controller("accountCtrl", function($scope, loginSession, $state, httpRequester, $ionicHistory){

	$scope.init = function(){
		$scope.designer = loginSession.getSession();
	};
	$scope.changePassword = function(){
		$state.go("changePassword");
	};
	$scope.signOut = function(){
		loginSession.signOut();
		$state.go("signIn", {}, {reload:true});
		$ionicHistory.clearHistory();
		//window.location.href = ROOT+"/index.html";
		//window.location.reload(true);
	};
	

	$scope.init();
});
