mainApp.controller("changePasswordCtrl",["$scope", "loginSession", "$state", "httpRequester", "$ionicPopup", "$ionicHistory",
function($scope, loginSession, $state, httpRequester, $ionicPopup, $ionicHistory){

	$scope.init = function(){
		$scope.pw={};
	};

	$scope.confirmPw= function(){
		if($scope.pw.showPw){
			$("#confirm-pw")[0].type="text";
		}else{
			$("#confirm-pw")[0].type="password";
		}
	};
	$scope.changePassword = function(){

		if(!$scope.pw.oldPw || !$scope.pw.newPw){
			console.log($scope.pw.oldPw);
			console.log($scope.pw.newPw);
			var text= "";
			if(!$scope.pw.oldPw){
				text="현재 비밀번호를 입력하세요.";
			}else if(!$scope.pw.newPw){
				text="변경할 비밀번호를 입력하세요.";
			}

			$ionicPopup.alert({
				title:"비밀번호 변경",
				template:text,
			});
			return;
		}

		httpRequester.request({
			url:"/designer/changePassword/"+loginSession.getSession().id,
			method:"PUT",
			data:{
				old:$scope.pw.oldPw,
				new:$scope.pw.newPw
			},
			success:function(data){
				console.log(data);
				
				$ionicPopup.alert({
					title:"비밀번호 변경",
					template:"비밀번호 변경을 완료했습니다."
				}).then(function(res){
					$ionicHistory.goBack();
				});
			},
			error:function(data){
				if(data.data=="unauthorized"){
					$ionicPopup.alert({
						title:"비밀번호 변경",
						template:"현재 비밀번호가 일치하지 않습니다."
					}).then(function(res){
						$scope.pw={};
					});
				}else{
					$ionicPopup.alert({
						title:"비밀번호 변경",
						template:"비밀번호 변경에 실패했습니다."
					}).then(function(res){
						$scope.pw={};
					});
					
				}
				console.log(data);
			}

		});
		
	}

	$scope.init();
}]);
