mainApp.controller("signInCtrl", ["$scope", "customerStorage", "loginSession", "httpRequester", "memoStorage", "$q", "$state", "nativeCall", "optionStorage", "$ionicPopup",
function($scope, customerStorage, loginSession, httpRequester, memoStorage, $q, $state, nativeCall, optionStorage, $ionicPopup){
	$scope.init = function(){
		$scope.account={};
		$scope.account.id="";
		$scope.account.pw="";
		$scope.setHideTabs(true);
		/*
		$scope.$on("$ionicView.beforeEnter", function(){
			$ionicHistory.clearHistory();
		});
		*/
	};

	$scope.signIn = function(){

		if($scope.account.id=="" || $scope.account.pw==""){
			$ionicPopup.alert({
				title : "로그인",
				template : "아이디와 비밀번호를 모두 입력하세요."
			});
			return;
		
		}
		httpRequester.request({
			url:"/signIn",
			method:"POST", 
			data:{id:$scope.account.id, pw:$scope.account.pw}, 
			success:function(data){
				console.log(data);

				var designerInfo = {}; 
				designerInfo.name=data.designer.name;
				designerInfo.id=data.designer.id;
				designerInfo.branch=data.designer.branch;
				loginSession.setSession(designerInfo);


				$q.all([customerStorage.init(), memoStorage.init(), optionStorage.init()]).then(function (data){
					console.log(data);
					nativeCall.initDesigner(loginSession.getSession().id);
					window.location.href=ROOT+"/index.html";

				}, function(data){
					$ionicPopup.alert({
						title : "로그인 실패",
						template : "로그인하지 못했습니다."
					});
					
				});
				/*
				httpRequester.request({
					url:"/memo/c12",
					data:{count:10, designerId:"test"},
					method:"GET",
					success:function(data){
						console.log(data);
					},
					error:function(data){
						console.log(data);
					}
				});
				*/
			},
			error:function(data){
				$ionicPopup.alert({
					title : "로그인",
					template : "계정 정보가 없습니다."
				});
	
		      	}
		});

	};
	$scope.init();


}]);
