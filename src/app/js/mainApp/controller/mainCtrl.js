mainApp.controller("mainCtrl", function($scope, $http, $interval, $state, customerStorage, memoStorage, $rootScope, $filter, nativeCall, $ionicScrollDelegate, $timeout, $ionicScrollDelegate, $ionicHistory, httpRequester, loginSession){
	var VISIT_CUSTOMER = 1;
	var BYE_DESIGNER=2;
	var CHANGE_CUSTOMER_INFO=3;

	$scope.init=function(){
		$scope.badge={
			customerList : customerStorage.load(),
			notiCount : memoStorage.load()
		};
	};
	$scope.setHideTabs= function(is){
		$scope.hideTab=is;
	};




	$scope.fetchPush = function(push){
		console.log(push);
		if(loginSession.getSession().id == undefined){
			return;
		}
		if(push != undefined){
			httpRequester.request({
				url:"/designer/fetchPush/"+loginSession.getSession().id+"/"+push.id,
				resend:false,
				loading:false,
				success:function(data){
					console.log(data);
					if(data.data != undefined){
						$scope.pushProcess(data.data);
						//$scope.$apply();
					}
				},
				error:function(data){
					console.log(data);
				}
			});
		}else{
			httpRequester.request({
				url:"/designer/fetchPush/"+loginSession.getSession().id,
				resend:false,
				loading:false,
				success:function(data){
					console.log(data);
					for(var i=0;i<data.data.length;i++){
						$scope.pushProcess(data.data[i]);
					}
					//$scope.$apply();
				},
				error:function(data){
					console.log(data);
				}
			});
		}

	};

	$scope.pushProcess = function(pushObj){
		console.log(pushObj);
		var payload = JSON.parse(pushObj.data);
		var pushId = pushObj.pushId;

		if(payload.type == VISIT_CUSTOMER){
			var existIdx= customerStorage.find(payload.data.customer);	
			if(existIdx == -1){
				customerStorage.save({fixedMemo:"",name:payload.data.customer.name, id:payload.data.customer.id});
			}
			memoStorage.setVisit(payload.data, pushObj.pushId);
		}else if(payload.type == CHANGE_CUSTOMER_INFO){
			customerStorage.changeInfo(payload.data.customer);	
			memoStorage.updateCustomerInfo(payload.data.customer);
		}
	};

	$scope.init();

});
