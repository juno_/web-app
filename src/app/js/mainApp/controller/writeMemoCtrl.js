mainApp.controller("writeMemoCtrl", ["$scope", "$stateParams", "memoStorage", "httpRequester", "loginSession", "resender", "$state", "nativeCall","optionStorage", "$ionicHistory", "$ionicPopup",
function($scope, $stateParams, memoStorage, httpRequester, loginSession, resender, $state, nativeCall, optionStorage, $ionicHistory, $ionicPopup){
	

	$scope.init=function(){
		$scope.customer=$stateParams.customerId;
		$scope.customer={};
		$scope.customer.id = $stateParams.customerId;
		$scope.customer.name = $stateParams.customerName;


		//if memo idx exsit : modify 


		$scope.proc = optionStorage.proc;


		$scope.memo={};
		$scope.memo.customer={};
		$scope.memo.customer.id= $scope.customer.id;
		$scope.memo.customer.name= $scope.customer.name;
		$scope.memo.proc={};


		$scope.memoIdx = $stateParams.memoIdx;
		if($scope.memoIdx){
			$scope.modInit();
		}else{
			var loadMemo;
			loadMemo = memoStorage.loadCustomer($scope.memo.customer);
			$scope.memo.isVisit = loadMemo.isVisit;
		}


	};
	$scope.modInit = function(){
		$scope.memoList = memoStorage.loadCustomer($scope.customer).data;
		$scope.modMemo = $scope.memoList[$scope.memoIdx];
		//console.log($scope.modMemo);
		$scope.memo.price = $scope.modMemo.price == 0 ? "" : $scope.modMemo.price;
		$scope.memo.content= $scope.modMemo.content;

		var procOp;
		var i=0;
		if($scope.modMemo.proc == ""){
			procOp = [];
		}else{
			procOp = $scope.modMemo.proc.split("|");
		}
		for(i =0 ;i<procOp.length;i++){
			$scope.memo.proc[procOp[i]]=parseInt(procOp[i]);
		}
		//procOp

	};

	$scope.saveMemo = function(){

		console.log($scope.memo.proc);

		if(!$scope.memo.content){
			$ionicPopup.alert({
				title:"메모작성",
				template:"내용을 입력해주세요."
			});
			return;
		}
		if(!$scope.memo.price){
			$scope.memo.price =0;
		}



		if($scope.modMemo){
			var procList="";
			for(var key in $scope.memo.proc){
				if($scope.memo.proc[key] != false){
					procList+=$scope.memo.proc[key];
					procList+="|";
				}
			}
			procList=procList.substring(0, procList.length-1);
			//$scope.m == 0 ? "" : $scope.modMemo.priceemo.proc = procList;
			$scope.memo.proc = procList;

			var q = memoStorage.update($scope.memo, $scope.memoIdx);
			q.then (function(){
				$ionicHistory.goBack();
			}, function(){
				$ionicHistory.goBack();
			});

		}else{
			var date = new Date();
			var loadMemo;
			loadMemo = memoStorage.loadCustomer($scope.memo.customer);
			if(loadMemo.isVisit){
				$scope.memo.date = loadMemo.isVisit;
				$scope.memo.isHiMemo = true;
			}else{
				$scope.memo.date = date.format("yyyy-MM-dd HH:mm:ss");
				$scope.memo.isHiMemo = false;
			}
			$scope.memo.timestamp= date.getTime()+"";
			$scope.memo.state="saving";
			var procList="";
			for(var key in $scope.memo.proc){
				if($scope.memo.proc[key] != false){
					procList+=$scope.memo.proc[key];
					procList+="|";
				}
			}
			procList=procList.substring(0, procList.length-1);

			$scope.memo.proc = procList;
			$scope.memo.pushId = loadMemo.pushId;

			resender.resend(0);
			var q = memoStorage.save($scope.memo);
			q.then(function(){
				nativeCall.removeNoti();
				$ionicHistory.goBack();
			},function(){
				nativeCall.removeNoti();
				$ionicHistory.goBack();
			});
		}
	};
	$scope.init();
}]);
