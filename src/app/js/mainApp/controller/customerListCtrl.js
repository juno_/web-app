mainApp.controller("customerListCtrl", ["$scope", "customerStorage", "$timeout", "$rootScope", "$filter", "$state", "$ionicModal", "$ionicPopover", "memoStorage", "httpRequester", "loginSession", "nativeCall", "$ionicScrollDelegate", "$ionicTabsDelegate", 
function($scope, customerStorage, $timeout, $rootScope, $filter, $state, $ionicModal, $ionicPopover, memoStorage, httpRequester, loginSession, nativeCall, $ionicScrollDelegate, $ionicTabsDelegate){
	var index=0;

	$scope.fixedMemo={
		maxFMLen : 100
	};

	$scope.init=function(){
		$scope.customerList=[];
		$scope.customerListView = [];
		$scope.fmPopupCustomer = {};
		$scope.scrollToTop = false;

		$scope.customerList = customerStorage.load();
		$ionicModal.fromTemplateUrl("fixedMemo.html", {
			scope:$scope
		}).then(function(modal){
			$scope.fixedMemoModal = modal;
		});
		$scope.$on("modal.hidden", function(){
			$scope.fixedMemo.modFM = false;
			$scope.fixedMemo.newFM ="";
			$ionicScrollDelegate.$getByHandle("p-scroll").scrollTop();
		});

		////////////////////////////////////////////////
		/*
		$scope.$on("$scrollToTop", function(){
			$scope.scrollToTop = true;
		})
		$scope.$on("$ionicView.beforeEnter", function(){
			if($scope.scrollToTop == true){
				$scope.scrollToTop = false;
				$ionicScrollDelegate.$getByHandle("customer-scroll").scrollTop();
				
			}
		});
		*/
		////////////////////////////////////////////////

		$ionicPopover.fromTemplateUrl("optionPopover.html", {
			scope:$scope
		}).then(function(popover){
			$scope.optionPopover = popover;
		});
		
	};
	$scope.modFixedMemo = function(customer){
		if($scope.fixedMemo.modFM ){
			$scope.fixedMemo.modFM = false;
		}else{
			$scope.fixedMemo.modFM = true;
			$scope.fixedMemo.newFM = customer.fixedMemo;
		}
	};
	$scope.fixedMemo.chkLen = function($event){
		
		if($scope.fixedMemo.newFM.length > $scope.fixedMemo.maxFMLen-1 && $event.keyCode != 8){
			$scope.fixedMemo.newFM = $scope.fixedMemo.newFM.substring(0, $scope.fixedMemo.maxFMLen);
			$event.preventDefault();
			$event.stopPropagation();
			return false;
		}
	};
	$scope.fixedMemo.save = function(){
		//customerStorage.modFixedMemo($scope.selCustomerIdx, $scope.fixedMemo.newFM);
		customerStorage.modFixedMemo($scope.selCustomer, $scope.fixedMemo.newFM);
		$ionicScrollDelegate.$getByHandle("p-scroll").scrollTop();
		$scope.fixedMemo.modFM = false;
	};
	$scope.writeMemo=function(customer){
		$scope.fixedMemoModal.hide();
		//$state.go("writeMemo_cv", {customerId:customer.id, customerName:customer.name});
		$ionicTabsDelegate.select(1);
		$timeout(function(){
			$state.go("customerMemo", {customerId:customer.id, customerName:customer.name});
		}, 0);

	};
	$scope.searchCustomer = function(keyword){
		//console.log($scope.searchKey);
		if(keyword == undefined){
			$scope.customerListView = $filter("searchFor")($scope.customerList, $scope.searchKey);
		}else{
			$scope.searchKey = keyword;
			$scope.customerListView = $filter("searchFor")($scope.customerList, keyword);
		}
	};
	$scope.showFMPopup = function(customer, index){
		$scope.selCustomer= customer;
		$scope.selCustomerIdx = index;
		$scope.fixedMemoModal.show();
	};
	$scope.showOption = function($event, customer){
		$scope.selCustomer= customer;
		$scope.optionPopover.show($event);
	};
	$scope.delCustomer = function(){
		var customerMemo = memoStorage.loadCustomer($scope.selCustomer);
		var param = {
			customerId:$scope.selCustomer.id,
		}
		if(customerMemo.pushId){
			param.pushId = customerMemo.pushId;
		}
                httpRequester.request({
                        url:"/customer/hideCustomer/"+loginSession.getSession().id,
                        method:"PUT", 
                        resend:true,
			data : param,
                        success:function(data){
				memoStorage.removeRoom($scope.selCustomer.id);
				customerStorage.remove($scope.selCustomer.id);

                                console.log(data);
                                nativeCall.showToast($scope.selCustomer.name+" 고객님과 메모가 삭제되었습니다.");
				$scope.optionPopover.hide();
                        },  
                        error:function(data){
                                console.log(data);
                                nativeCall.showToast("삭제에 실패했습니다.");
				$scope.optionPopover.hide();
                        }   
                }); 
	};
	$scope.init();
}]);


