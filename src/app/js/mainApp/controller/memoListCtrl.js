mainApp.value("scrollGoTop", {state:false});
mainApp.controller("memoListCtrl", function($scope, $http, $interval, memoStorage, $state, $timeout, $ionicScrollDelegate, loginSession, customerStorage, nativeCall, $ionicPopover, httpRequester, $rootScope, $ionicScrollDelegate, scrollGoTop){
	var index=0;
	var maxLen = 0;
	var viewCount = 15;

	$scope.init=function(){
		var isUpdate = false;
		$scope.memoRoomList=[];
		$scope.memoRoomVisitList = [];
		$scope.memoRoomListView = [];
		$scope.touchStartPos = {};
		$scope.touchEndPos = {};

		$scope.isLongPress = false;
		$scope.scrollToTop = false;


		index=0;


		//$scope.memoRoomListView = $scope.memoRoomVisitList.concat($scope.memoRoomListView);
		//$scope.addMemoList();

		$scope.memoRoomList = memoStorage.load();

		///////////////////////////////////////////
		$scope.$on("$scrollToTop", function(){
			$scope.scrollToTop = true;
		});
		$scope.$on("$ionicView.beforeEnter", function(){
			if($scope.scrollToTop == true || scrollGoTop.state == true){
				//$scope.$apply();
				$scope.scrollToTop = false;
				$ionicScrollDelegate.scrollTop();
				scrollGoTop.state = false;
			}
		});
		///////////////////////////////////////////


		$ionicPopover.fromTemplateUrl("memoOptionPopover.html", {
                        scope:$scope
                }).then(function(popover){
                        $scope.optionPopover = popover;
                });


	}

	$scope.showMemo = function(customer){
		$state.go("customerMemo",{"customerId":customer.id,"customerName":customer.name});
	};
        $scope.showOption = function($event, customer){
                $scope.selCustomer= customer;
                $scope.optionPopover.show($event);
        };  
        $scope.delCustomer = function(){
    
		var customerMemo = memoStorage.loadCustomer($scope.selCustomer);
		var param = {
			customerId:$scope.selCustomer.id,
		}
		if(customerMemo.pushId){
			param.pushId = customerMemo.pushId;
		}
                httpRequester.request({
                        url:"/customer/hideCustomer/"+loginSession.getSession().id,
                        method:"PUT", 
                        resend:true,
                        data:param,  
                        success:function(data){
				memoStorage.removeRoom($scope.selCustomer.id);
				customerStorage.remove($scope.selCustomer.id);

                                console.log(data);
                                nativeCall.showToast($scope.selCustomer.name+" 고객님과 메모가 삭제되었습니다.");
                                $scope.optionPopover.hide();

                        },  
                        error:function(data){
                                console.log(data);
                                nativeCall.showToast("삭제에 실패했습니다.");
                                $scope.optionPopover.hide();
                        }   
                }); 
        };  





	$scope.init();
});

