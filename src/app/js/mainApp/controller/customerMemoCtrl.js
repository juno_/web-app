mainApp.controller("customerMemoCtrl", function($scope, $timeout, $stateParams, memoStorage, httpRequester, loginSession, resender, $state, nativeCall, $ionicPopover, optionStorage, customerStorage, $ionicHistory){
	var moreCount = 10;
	
	$scope.memoOption = {};

	$scope.init=function(){
		console.log("customerMemoCtrl");
		$scope.customer={id:$stateParams.customerId, name:$stateParams.customerName};
		$scope.isMoreData = true;
		//$scope.customerMemoList = memoList;

		$scope.customerMemoList = memoStorage.loadCustomer({id:$scope.customer.id, name:$scope.customer.name}).data;
		console.log($scope.customerMemoList);

		$scope.$on("$ionicView.beforeEnter", function(){
			//$scope.setHideTabs(true);
			$scope.isMoreData = ($scope.customerMemoList.length == 0 ? false:true );
		});

		$ionicPopover.fromTemplateUrl("memoOptionPopover.html", {
			scope:$scope
		}).then(function(popover){
			$scope.memoPopover = popover;
		});
	};
	$scope.loadMore = function(){
		httpRequester.request({
			url:"/memo/"+$scope.customer.id,
			method:"GET",
			loading:false,
			data:{
				designerId : loginSession.getSession().id,		
				timestamp : $scope.customerMemoList[$scope.customerMemoList.length-1].timestamp,
				count:moreCount
			},
			success:function(data){
				if(data.result.length == 0){
					$scope.isMoreData = false;
					return;
				}
				var i = 0;
				var len =  data.result.length;
				while(i<len){
					$scope.customerMemoList.push(data.result[i]);
					i++;
					
				}
				//$scope.customerMemoList = $scope.customerMemoList.concat(data.result);
				$scope.$broadcast("scroll.infiniteScrollComplete");

			}
		});

	};
	$scope.getProcName=function(num){
		if(!num){
			return undefined;
		}
		var numArr = num.split("|");
		var procList = "";
		for(var i =0 ;i<numArr.length;i++){
			procList+=(optionStorage.proc[numArr[i]]).name;
			if(i +1 <numArr.length){
				procList+=" + ";
			}
		}
		return procList;

	}
	$scope.writeMemo = function(param){
		$state.go("writeMemo", param);
	};



	$scope.memoOption.showMemoOption = function($event, idx){
		console.log($event);
		
		$scope.memoOption.selIdx= idx;
		$scope.memoOption.selMemo = $scope.customerMemoList[idx];
		$scope.memoPopover.show($event);
	};
	$scope.memoOption.modMemo = function(){
		$scope.memoPopover.hide();
		$state.go("writeMemo", {customerId : $scope.memoOption.selMemo.customer.id, customerName:$scope.memoOption.selMemo.customer.name, memoIdx : $scope.memoOption.selIdx });

	};
	$scope.memoOption.delMemo = function(){
		var q;
		q=memoStorage.delete($scope.memoOption.selMemo.customer, $scope.memoOption.selIdx);
		/*
		q.then(function(){
			if($scope.customerMemoList.length == 0){
				customerStorage.remove($scope.memoOption.selMemo.customer.id);
				$ionicHistory.goBack();
			}
		},function(){
		});
		*/
		$scope.memoPopover.hide();
	};
	$scope.init();
});
