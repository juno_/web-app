mainApp.config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "loginSessionProvider", "nativeCallProvider", "$ionicConfigProvider",
function($stateProvider, $urlRouterProvider, $httpProvider, loginSessionProvider, nativeCallProvider, $ionicConfigProvider){
	
	var indexUrl= "";
	var indexCtrl = "";
	console.log(loginSessionProvider.chkAlreadySignIn());
	if(loginSessionProvider.chkAlreadySignIn()){
		indexUrl = ROOT+ "/js/template/customerList.html";
		indexCtrl = "customerListCtrl";
	}else{
		indexUrl =ROOT+"/js/template/signIn.html";
		indexCtrl = "signInCtrl";
	}
	$urlRouterProvider.otherwise("/main");


	$stateProvider
	.state("main", {
		url:"/main",
		views:{
			"customerView":{
				templateUrl:indexUrl,
				controller: indexCtrl
			}
		}
	})
	.state("signIn", {
		url:"/signIn",
		views:{
			"customerView":{
				templateUrl : ROOT+"/js/template/signIn.html",
				controller: "signInCtrl"
			}
		}
	})
	.state("customerList", {
		url:"/customerList",
		views:{
			"customerView":{
				templateUrl : ROOT+"/js/template/customerList.html",
				//templateUrl :"customerList.html",
				controller: "customerListCtrl",
			}
		}
	})
	.state("memoList", {
		url:"/memoList",
		views:{
			"memoView":{
				templateUrl : ROOT+"/js/template/memoList.html",
				//templateUrl : "memoList.html",
				controller: "memoListCtrl"
				/*
				resolve : {
					memoList : function($stateParams, memoStorage){
						var test = memoStorage.getLastMemo();
						console.log(test);
						return test;

					}
				}
				*/

			}
		}
	})
	.state("customerMemo", {
		url:"/customerMemo/:customerId/:customerName",
		views:{
			"memoView":{
				templateUrl : ROOT+"/js/template/customerMemo.html",
				controller: "customerMemoCtrl"
			}
		}
	})
	.state("pref", {
		url:"/pref",
		views:{
			"prefView":{
				templateUrl : ROOT+"/js/template/pref.html",
				controller: "preferenceCtrl"
			}
		}
	})
	.state("account", {
		url:"/account",
		views:{
			"prefView":{
				templateUrl : ROOT+"/js/template/account.html",
				controller: "accountCtrl"
			}
		}
	})
	.state("changePassword", {
		url:"/changePassword",
		views:{
			"prefView":{
				templateUrl : ROOT+"/js/template/changePassword.html",
				controller: "changePasswordCtrl"
			}
		}
	})
	.state("stats", {
		url:"/stats",
		views:{
			"prefView":{
				templateUrl : ROOT+"/js/template/stats.html",
				controller: "statsCtrl"
			}
		}
	})
	.state("visit", {
		url:"/visit",
		views:{
			"prefView":{
				templateUrl : ROOT+"/js/template/stats/visit.html",
				controller: "visitCtrl"
			}
		}
	})
	.state("totalCustomer", {
		url:"/totalCustomer",
		views:{
			"prefView":{
				templateUrl : ROOT+"/js/template/stats/totalCustomer.html",
				controller: "totalCustomerCtrl"
			}
		}
	})
	.state("busyDay", {
		url:"/busyDay",
		views:{
			"prefView":{
				templateUrl : ROOT+"/js/template/stats/busyDay.html",
				controller: "busyDayCtrl"
			}
		}
	})
	.state("procStats", {
		url:"/procStats",
		views:{
			"prefView":{
				templateUrl : ROOT+"/js/template/stats/procStats.html",
				controller: "procStatsCtrl"
			}
		}
	})
	.state("returnRatio", {
		url:"/returnRatio",
		views:{
			"prefView":{
				templateUrl : ROOT+"/js/template/stats/returnRatio.html",
				controller: "returnRatioCtrl"
			}
		}
	})
	.state("customerStats", {
		url:"/customerStats",
		views:{
			"customerView":{
				templateUrl : ROOT+"/js/template/customerStats.html",
				controller: "customerStatsCtrl"
			}
		}
	})
	.state("writeMemo_cv", {
		url:"/writeMemo/:customerId/:customerName",
		views:{
			"customerView":{
				templateUrl : ROOT+"/js/template/writeMemo.html",
				controller: "writeMemoCtrl"
			}
		}
	})
	.state("writeMemo", {
		url:"/writeMemo/:customerId/:customerName/:memoIdx",
		views:{
			"memoView":{
				templateUrl : ROOT+"/js/template/writeMemo.html",
				controller: "writeMemoCtrl"
			}
		}
	});

	nativeCallProvider.setPhoneDist();


	/*
	if(ionic.Platform.isAndroid()){
		$ionicConfigProvider.scrolling.jsScrolling(false);
	}
	*/



        /*
        $httpProvider.defaults.transformRequest=function(data){
                if(data === undefined){
                        return data;
                }
                return $.param(data);
        };
        */
        //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
}]);
mainApp.run(function(resender, nativeCall, $ionicPlatform, $ionicHistory, $timeout){
	console.log("run");
	//FastClick.attach(document.body);
	
	ionic.Platform.isFullScreen=true;

	resender.resend(0);
	$ionicPlatform.registerBackButtonAction(function(event){
		var stateName = $ionicHistory.currentView().stateName;

		if(stateName == "customerList" ||
                        stateName  == "memoList" ||  
                        stateName  == "pref" ||
                        stateName  == "signIn"  ||  
                        stateName  == "main" 
                ){  
                        nativeCall.exitApp();

                }else{
                        $ionicHistory.goBack();
                }   

	}, 100);

	$timeout(function(){
		nativeCall.getSetReady();
	}, 0);

	/*
	$timeout(function(){
		if("createEvent" in document){
			var evt = document.createEvent("HTMLEvents");
			evt.initEvent("backbutton", false, true);
			document.dispatchEvent(evt);
		}else{
			document.fireEvent("backbutton");
		}
	}, 5000);
	*/


	
});


