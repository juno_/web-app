window.mainApp = angular.module("mainApp", ["commonApp", "ng", "ui.router","ionic", "swipe" ]);

/*
function pushNoti(push){
	if(typeof push === "string"){
		push = JSON.parse(push);
	}
	angular.element(document.getElementById("mainApp")).scope().pushProcess(push);

}
*/
function fetchAllPush(){
	angular.element(document.getElementById("mainApp")).scope().fetchPush();

}
function fetchPush(push){
	if(typeof push === "string"){
		push = JSON.parse(push);
	}
	angular.element(document.getElementById("mainApp")).scope().fetchPush(push);
}
function backPressed(){
	if("createEvent" in document){
		var evt = document.createEvent("HTMLEvents");
		evt.initEvent("backbutton", false, true);
		document.dispatchEvent(evt);
	}else{
		document.fireEvent("backbutton");
	}   
	//angular.element(document.getElementById("mainApp")).scope().backPressed();
}

