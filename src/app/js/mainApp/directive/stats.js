mainApp.directive("visitStats", ["httpRequester", "loginSession",  function(httpRequester, loginSession){
	function getData(){
		httpRequester.request({
			url:"/stats/visit/"+loginSession.getSession().id,
			method:"GET",
			success:function(data){
				data.result.push({x:"10", y:"2014-03"});
				data.result.push({x:"20", y:"2014-04"});
				data.result.push({x:"30", y:"2014-05"});
				data.result.push({x:"40", y:"2014-06"});
				data.result.push({x:"50", y:"2014-07"});
				data.result.push({x:"60", y:"2014-08"});
				data.result.push({x:"70", y:"2014-09"});
				data.result.push({x:"80", y:"2014-10"});
				data.result.push({x:"90", y:"2014-11"});
				data.result.push({x:"120", y:"2014-12"});
				var ctx = document.getElementById("visitStats").getContext("2d");
				var y= [];
				var x= [];
				var option={};
				for(var i=0;i<data.result.length;i++){
					y.push(data.result[i].y);
					x.push(data.result[i].x);
				}

				option.animation=false;
				option.scaleGridLineColor="#ddd";
				option.scaleLineColor="#ddd";
				option.tooltipTemplate="<%=value%>명";

				var chart = new Chart(ctx).Line({
					labels:y,
					datasets:[
						{
							fillColor:"rgba(49, 164, 188, 0)",
							strokeColor:"rgba(49, 164, 188, 1)",
							pointColor:"rgba(49, 164, 188, 1)",
							pointStrokeColor:"#fff",
							//pointHighlightStroke:"#f26162",
							pointHighlightStroke:"rgba(49, 164, 188, 1)",
							pointHighlightFill:"#fff",
							data:x
						}
					]
				}, option);



			}

		});
	}

	return{
		restrict:"E",
		link : function($scope, $element, $attrs){
			getData();
		},
		template : function($element, $attrs, $scope){
		
			var tmpl="<div class='chart-wrap' >";
			tmpl+="<canvas class='stats-can' id='visitStats' ></canvas>";
			tmpl+="<div class='chart-title' >title</div>";
			tmpl+="<input type='month' />";
			tmpl+="</div>";

			return tmpl;
		},
	};
}]);
