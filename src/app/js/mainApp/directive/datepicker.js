mainApp.directive("jDatePicker", function(){
        return{
                restrict : "E",
                scope : { 
			selCallback:"&",
			selDate:"=",
                },  
                link : function($scope, $element, $attrs){
			$scope.cal={};
			$scope.cal.cur = $scope.selDate;
			$scope.days = ["일","월", "화","수","목", "금", "토"];

			$scope.$watch('selDate', function(newValue, oldValue){
				if(newValue){
					$scope.cal.cur =newValue; 
					$scope.update();
				}
			});

			$scope.update = function(){
				var today = new Date();
				$scope.cal.year = $scope.cal.cur.getFullYear();
				$scope.cal.month= $scope.cal.cur.getMonth();


				$scope.cal.dateList = [];
				var firstOfMonth = new Date($scope.cal.year, $scope.cal.month, 1);
				var lastOfMonth = new Date($scope.cal.year, $scope.cal.month+1, 0);
				var firstDateDay = firstOfMonth.getDay();
				var startDay = 0;
				//push before month dates
				for(var i = firstDateDay;i>0;i--){
					var beforeDate = new Date(firstOfMonth);
					beforeDate.setDate(firstOfMonth.getDate()-i);
					var obj = {};
					obj.year = beforeDate.getFullYear();
					obj.month = beforeDate.getMonth();
					obj.date= beforeDate.getDate();
					obj.day= startDay % 7;
					startDay++;
					$scope.cal.dateList.push({date:beforeDate.getDate(), dist:"prev", dateObj:obj, cls:"button-light" });
				} 
				//push dates of now month
				var start = firstOfMonth.getDate();
				var end  = lastOfMonth.getDate();
				for(var i = start ; i<=end;i++){
					var obj = {};
					obj.year = firstOfMonth.getFullYear();
					obj.month = firstOfMonth.getMonth();
					obj.date= i;
					obj.day= startDay % 7;
					startDay++;
					if(today.getFullYear() == $scope.cal.year 
						&& today.getMonth() == $scope.cal.month
						&& i == today.getDate()){
						$scope.cal.dateList.push({date:i, dist:"today", dateObj : obj, cls:"button-royal" });
					}else{
						if(obj.day == 0){
							$scope.cal.dateList.push({date:i, dist:"now", dateObj : obj, cls:"button-assertive" });
						}else{
							$scope.cal.dateList.push({date:i, dist:"now", dateObj : obj, cls:"button-dark" });
						}
					}
				}
				
				//push next month dates
				var lastDateDay = lastOfMonth.getDay();
				var nextMonth = new Date(lastOfMonth);
				nextMonth.setDate(lastOfMonth.getDate()+1);
				for(var i=0;i<6-lastDateDay ;i++){
					var obj = {};
					obj.year = nextMonth.getFullYear();
					obj.month = nextMonth.getMonth();
					obj.date = i+1; 
					obj.day= startDay % 7;
					startDay++;
					$scope.cal.dateList.push({date:i+1, dist:"next", dateObj : obj, cls:"button-light" });
				}




			};
			$scope.selToday = function(){
				$scope.selCallback({dateObj:new Date()});
			}
			$scope.tapDate = function(dateObj){
				var selD = new Date(dateObj.year, dateObj.month, dateObj.date);
				$scope.selCallback({dateObj:selD});
			};
			$scope.getTitle = function(){
				return ($scope.cal.month+1)+"월 "+$scope.cal.year;
			};
			$scope.nextMon = function(){
				$scope.cal.cur.setMonth($scope.cal.cur.getMonth()+1);
				$scope.update();
			};
			$scope.moveToToday = function(){
				$scope.cal.cur = new Date();
				$scope.update();
			}
			$scope.prevMon = function(){
				$scope.cal.cur.setMonth($scope.cal.cur.getMonth()-1);
				$scope.update();
			};
			$scope.update();
                },  
                template : function($element, $attrs, $scope){
                        var tmp ="";
                        tmp+="<div class='j-dp' >";
				tmp+="<div class='dp-header' >";
					tmp+="<div class='prev-mon button button-clear button-positive icon ion-ios-arrow-left' on-tap='prevMon()' ></div>";
					tmp+="<div class='now-mon button button-clear button-positive' >{{getTitle()}}</div>";
					tmp+="<div class='next-mon button button-clear button-positive icon ion-ios-arrow-right' on-tap='nextMon()' ></div>";
				tmp+="</div>";
				tmp+="<div class='dp-body' >";
					tmp+="<div class='date-col' ng-repeat='day in days' >";
					tmp+="{{day}}";
					tmp+="</div>";
					tmp+="<div class='date-col' ng-repeat='date in cal.dateList' >";
					tmp+="<span class='button button-clear' ng-class='date.cls'  on-tap='tapDate(date.dateObj)' >{{date.date}}</span>";
					tmp+="</div>";

				tmp+="</div>";

				tmp+="<div class='dp-func row' >";
					tmp+="<div class='col' >";

						tmp+="<div class='button button-positive button-clear' on-tap='moveToToday()' >이번달</div>";
					tmp+="</div>";
					tmp+="<div class='col' >";
						tmp+="<div class='button button-positive button-clear' on-tap='selToday()' >오늘</div>";
					tmp+="</div>";
				tmp+="</div>";
                        tmp+="</div>";
                        return tmp;
                }   
        }   
});

mainApp.directive("jMonthPicker", function(){
        return{
                restrict : "E",
                scope : { 
			selCallback:"&",
			selMonth:"=",
                },  
                link : function($scope, $element, $attrs){

			
			$scope.cal={};
			$scope.cal.cur = $scope.selMonth;



			$scope.$watch('selMonth', function(newValue, oldValue){
				if(newValue){
					$scope.cal.cur =newValue; 
					$scope.update();
				}
			});

			$scope.update = function(){
				var now = new Date();
				$scope.cal.monthList = [
					{title:"1월", month:0, cls : "button-dark" },
					{title:"2월", month:1, cls : "button-dark" },
					{title:"3월", month:2, cls : "button-dark"  },
					{title:"4월", month:3, cls : "button-dark"  },
					{title:"5월", month:4, cls : "button-dark"  },
					{title:"6월", month:5, cls : "button-dark"  },
					{title:"7월", month:6, cls : "button-dark"  },
					{title:"8월", month:7, cls : "button-dark"  },
					{title:"9월", month:8, cls : "button-dark"  },
					{title:"10월", month:9, cls : "button-dark"  },
					{title:"11월", month:10, cls : "button-dark"  },
					{title:"12월", month:11, cls : "button-dark"  }];
				$scope.cal.year = $scope.cal.cur.getFullYear();
				$scope.cal.month= $scope.cal.cur.getMonth();
				if($scope.cal.year == now.getFullYear()){
					for(var i=0;i<$scope.cal.monthList.length;i++){
						if($scope.cal.monthList[i].month == now.getMonth()){
							$scope.cal.monthList[i].cls = "button-royal";
							break;
						}
					}
				}


			};
			$scope.selNowMonth= function(){
				$scope.selCallback({dateObj:new Date()});
			}
			$scope.tapMonth = function(month){
				var selD = new Date($scope.cal.cur.getFullYear(), month, 1);
				$scope.selCallback({dateObj:selD});
			};
			$scope.nextYear = function(){
				$scope.cal.cur.setFullYear($scope.cal.cur.getFullYear()+1);
				$scope.update();
			};
			$scope.moveToNowYear= function(){
				$scope.cal.cur = new Date();
				$scope.update();
			}
			$scope.prevYear = function(){
				$scope.cal.cur.setFullYear($scope.cal.cur.getFullYear()-1);
				$scope.update();
			};
			$scope.update();
                },  
                template : function($element, $attrs, $scope){
                        var tmp ="";
                        tmp+="<div class='j-dp' >";
				tmp+="<div class='dp-header' >";
					tmp+="<div class='prev-mon button button-clear button-positive icon ion-ios-arrow-left' on-tap='prevYear()' ></div>";
					tmp+="<div class='now-mon button button-clear button-positive' >{{cal.year}}</div>";
					tmp+="<div class='next-mon button button-clear button-positive icon ion-ios-arrow-right' on-tap='nextYear()' ></div>";
				tmp+="</div>";
				tmp+="<div class='dp-body' >";
					tmp+="<div class='month-col' ng-repeat='m in cal.monthList' >";
					tmp+="<span class='button button-clear' ng-class='m.cls' on-tap='tapMonth(m.month)' >{{m.title}}</span>";
					tmp+="</div>";

				tmp+="</div>";

				tmp+="<div class='dp-func row' >";
					tmp+="<div class='col' >";

						tmp+="<div class='button button-positive button-clear' on-tap='moveToNowYear()' >올해</div>";
					tmp+="</div>";
					tmp+="<div class='col' >";
						tmp+="<div class='button button-positive button-clear' on-tap='selNowMonth()' >이번달</div>";
					tmp+="</div>";
				tmp+="</div>";
                        tmp+="</div>";

                        return tmp;
                }   
        }   
});

mainApp.directive("jYearPicker", function(){
        return{
                restrict : "E",
                scope : { 
			selCallback:"&",
			selYear:"=",
                },  
                link : function($scope, $element, $attrs){

			
			$scope.cal={};
			$scope.cal.cur = $scope.selYear;



			$scope.$watch('selYear', function(newValue, oldValue){
				if(newValue){
					$scope.cal.cur =newValue; 
					//$scope.update();
				}
			});

			$scope.update = function(){
				var now = new Date();
				$scope.cal.yearList = [];
				var year = $scope.cal.cur.getFullYear(); 
				
				for(var i =0 ;i<12;i++){
					var yearItem = {};
					yearItem.year = year + i;
					yearItem.title= (year + i)+"년";
					yearItem.cls = now.getFullYear() == yearItem.year ? "button-royal" : "button-dark";
					$scope.cal.yearList.push(yearItem);
				}
				$scope.cal.decade = year + " ~ "+(year + i-1);


			};
			$scope.selNowYear = function(){
				$scope.selCallback({dateObj:new Date()});
			}
			$scope.tapYear = function(year){
				var selD = new Date(year, 0, 1);
				$scope.selCallback({dateObj:selD});
			};
			$scope.prevDecade = function(){
				var firstYear = $scope.cal.yearList[0].year;
				firstYear -= 12;
				$scope.cal.cur = new Date(firstYear, 0, 1);
				$scope.update();
			};
			$scope.nextDecade = function(){
				var lastYear = $scope.cal.yearList[$scope.cal.yearList.length - 1].year;
				lastYear += 1;
				$scope.cal.cur = new Date(lastYear, 0, 1);
				$scope.update();
			};
			$scope.moveToNowYear= function(){
				var selD = new Date();
				$scope.selCallback({dateObj:selD});
				$scope.cal.cur = selD;
				$scope.update();
			}
			$scope.update();
                },  
                template : function($element, $attrs, $scope){
                        var tmp ="";
                        tmp+="<div class='j-dp' >";
				tmp+="<div class='dp-header' >";
					tmp+="<div class='prev-mon button button-clear button-positive icon ion-ios-arrow-left' on-tap='prevDecade()' ></div>";
					tmp+="<div class='now-mon button button-clear button-positive' >{{cal.decade}}</div>";
					tmp+="<div class='next-mon button button-clear button-positive icon ion-ios-arrow-right' on-tap='nextDecade()' ></div>";
				tmp+="</div>";
				tmp+="<div class='dp-body' >";
					tmp+="<div class='month-col' ng-repeat='year in cal.yearList' >";
					tmp+="<span class='button button-clear' ng-class='year.cls' on-tap='tapYear(year.year)' >{{year.title}}</span>";
					tmp+="</div>";

				tmp+="</div>";

				tmp+="<div class='dp-func row' >";
					tmp+="<div class='col' >";

						tmp+="<div class='button button-positive button-clear' on-tap='moveToNowYear()' >올해</div>";
					tmp+="</div>";
				tmp+="</div>";
                        tmp+="</div>";
                        return tmp;
                }   
        }   
});

