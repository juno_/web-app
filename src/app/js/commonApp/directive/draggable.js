commonApp.directive("draggable", function($window, $document){

	return {
		restrict: "A",
		link : function($scope, $element, $attrs){
			var startX = 0,
			startY = 0,
			x = 0,
			y = 0,
			pressX=0,
			pressY=0,
			pressEvent = "touchstart mousedown",
			moveEvent = "touchmove mousemove",
			releaseEvent = "touchend mouseup";






			$element.on(pressEvent, function(event){

				event.preventDefault();
				event.stopPropagation();
				var eventX = event.pageX || event.originalEvent.touches[0].pageX ,
				    eventY = event.pageY || event.originalEvent.touches[0].pageY ;

				pressX = eventX;
				pressY = eventY;
				startX = eventX - x;
				startY = eventY - y;

				$document.on(moveEvent, mousemove);
				$document.on(releaseEvent, mouseup);

			});

			function mousemove(event){

				var eventX = event.pageX || event.originalEvent.touches[0].pageX ,
				    eventY = event.pageY || event.originalEvent.touches[0].pageY ;

				y=eventY - startY;
				x=eventX - startX;

				$element.css({ "top":y+"px", "left":x+"px" });

			};
			function mouseup(event){

				event.preventDefault();
				event.stopPropagation();
				var releaseX = event.originalEvent.pageX || event.originalEvent.changedTouches[0].pageX ,
				    releaseY = event.originalEvent.pageY || event.originalEvent.changedTouches[0].pageY ;

				if(Math.abs(releaseX - pressX) <= 10 && Math.abs(releaseY - pressY) <= 10){
					$scope.$eval($attrs.click);
					$scope.$apply();
				}

				$document.unbind(moveEvent, mousemove);
				$document.unbind(releaseEvent, mouseup);
			};
		}
	};


});
