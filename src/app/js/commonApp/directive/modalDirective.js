commonApp.directive("modal", function($window){
	return {
		restrict:"E",
		replace:true,
		scope:true,
		transclude:true,
		link : function($scope, $element, $attrs){
			$scope.contentUrl = $attrs.src;
			//$scope.modalStyle={"min-height":($scope.height-100)+"px" };
			$scope.modalStyle= {"height" : $scope.height*90/100+"px"};
			$scope.$watch($attrs.ngShow, function(val){
				if(val == true){
					$window.document.body.style.position="fixed";
					if($attrs.onCreate){
						$scope.$eval($attrs.onCreate);
					}
				}else{
					$window.document.body.style.position="relative";
				}
			});
		},
		template : function($element, $attrs){
			var modalTmpl = "";
			if($attrs.ctrl != undefined){
				modalTmpl += '<div class="modal-backdrop" ng-controller="'+$attrs.ctrl+'" >';
			}else{
				modalTmpl += '<div class="modal-backdrop" >';
			}
				modalTmpl += '<div class="modal-container" >';
					modalTmpl += '<div class="modal" ng-style="modalStyle" >';
						modalTmpl += '<div class="modal-title"  ><span>'+$attrs.title+'</span><a ng-click="$parent.'+$attrs.ngShow+'=false"  href="javascript:void(0);" >X</a></div>';
						modalTmpl += '<div class="modal-content"  ng-include="contentUrl" >';
						modalTmpl += '</div>';
						modalTmpl += '<div class="modal-btn-container" >';
						if($attrs.confirm){
							modalTmpl += '<input type="button" class="primary-btn" value="확인" ng-click="$parent.'+$attrs.ngShow+'=false;'+$attrs.confirm+'"  />';
						}
							modalTmpl += '<input type="button" class="normal-btn" value="닫기" ng-click="$parent.'+$attrs.ngShow+'=false"  />';
						modalTmpl += '</div>';
					modalTmpl += '</div>';
				modalTmpl += '</div>';
			modalTmpl += '</div>';

			return modalTmpl;
		}
	};
});
